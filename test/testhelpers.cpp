#include "testhelpers.h"

Game TestHelpers::combatSample(BattleField& injectedField) {
  auto game = Game();
  auto& field = injectedField;

  field.placeAt(Mob(2), {25, 25});
  field.placeAt(Mob(3), {25, 28});
  field.placeAt(Mob(2), {26, 24});
  field.placeAt(Mob(3), {23, 25});
  field.placeAt(Item(), {30, 30});

  Point2D location = {20, 37};
  auto mob = Mob(1, Faction(Faction::Type::player), Race(Race::Type::undead), true);
  mob.setMaxHp(100);
  mob.setHp(100);
  mob.setSpeed(125);
  field.placeAt(mob, location);

  game.getCombat()->loadField(field, location);
  return game;
}

Game TestHelpers::aStarCombatTestField(const BattleField& base) {
  auto game = Game();
  auto field = BattleField(base);

  auto mob = Mob(1, Faction(Faction::Type::player), Race(Race::Type::undead), true);
  mob.setMaxHp(100);
  mob.setHp(100);
  mob.setSpeed(125);
  auto playerLocation = Point2D(10, 10);
  field.placeAt(mob, playerLocation);

  field.placeAt(Blocker(), {5, 10});
  field.placeAt(Blocker(), {5, 11});
  field.placeAt(Blocker(), {5, 9});

  field.placeAt(Blocker(), {15, 10});
  field.placeAt(Blocker(), {15, 11});
  field.placeAt(Blocker(), {15, 9});

  field.placeAt(Blocker(), {10, 5});
  field.placeAt(Blocker(), {11, 5});
  field.placeAt(Blocker(), {9, 5});

  field.placeAt(Blocker(), {10, 15});
  field.placeAt(Blocker(), {11, 15});
  field.placeAt(Blocker(), {9, 15});

  field.placeAt(Mob(2), {3, 10});
  field.placeAt(Mob(2), {3, 11});
  field.placeAt(Mob(2), {3, 9});

  field.placeAt(Mob(2), {17, 10});
  field.placeAt(Mob(2), {17, 11});
  field.placeAt(Mob(2), {17, 9});

  field.placeAt(Mob(2), {10, 3});
  field.placeAt(Mob(2), {11, 3});
  field.placeAt(Mob(2), {9, 3});

  field.placeAt(Mob(2), {10, 17});
  field.placeAt(Mob(2), {11, 17});
  field.placeAt(Mob(2), {9, 17});

  game.getCombat()->loadField(field, playerLocation);
  return game;
}

Game TestHelpers::interFactionCombat(const BattleField& base) {
  auto game = Game();
  auto field = BattleField(base);

  field.placeAt(Blocker(), {30, 28});
  field.placeAt(Blocker(), {30, 29});
  field.placeAt(Blocker(), {30, 30});
  field.placeAt(Blocker(), {30, 31});
  field.placeAt(Blocker(), {30, 32});
  field.placeAt(Blocker(), {30, 33});

  Point2D playerLocation = {15, 30};
  field.placeAt(Mob(1, Faction(Faction::Type::player), Race(Race::Type::undead), true), playerLocation);

  field.placeAt(Mob(2, Faction(Faction::Type::undead)), {25, 26});
  field.placeAt(Mob(2, Faction(Faction::Type::undead)), {24, 29});
  field.placeAt(Mob(2, Faction(Faction::Type::undead)), {27, 27});
  field.placeAt(Mob(2, Faction(Faction::Type::undead)), {22, 32});

  field.placeAt(Mob(3, Faction(Faction::Type::player)), {17, 26});
  field.placeAt(Mob(3, Faction(Faction::Type::player)), {16, 29});
  field.placeAt(Mob(3, Faction(Faction::Type::player)), {18, 27});
  field.placeAt(Mob(3, Faction(Faction::Type::player)), {13, 32});

  field.placeAt(Mob(5, Faction(Faction::Type::wildlife)), {33, 26});
  field.placeAt(Mob(5, Faction(Faction::Type::wildlife)), {34, 29});
  field.placeAt(Mob(5, Faction(Faction::Type::wildlife)), {33, 28});
  field.placeAt(Mob(5, Faction(Faction::Type::wildlife)), {35, 32});
  field.placeAt(Mob(5, Faction(Faction::Type::wildlife)), {31, 27});
  field.placeAt(Mob(5, Faction(Faction::Type::wildlife)), {32, 33});

  game.getCombat()->loadField(field, playerLocation);
  return game;
}

Game TestHelpers::singlePlayerUnit(const BattleField& base) {
  auto game = Game();
  auto field = BattleField(base);

  constexpr Point2D playerLocation = {15, 30};
  field.placeAt(Mob(1, Faction(Faction::Type::player), Race(Race::Type::undead), true), playerLocation);

  game.getCombat()->loadField(field, playerLocation);
  return game;
}
