#include "../src/persistence/combatserializer.h"
#include "extern/catch.hpp"
#include "testhelpers.h"
#include <iostream>

using namespace sdlwrapp;

constexpr std::uint32_t notVeryRandomSeed = 123;

/* Disabled: Needs fixing after renderer refactor
* Saving/Loading isn't done yet anyway
TEST_CASE("Lossless saving and loading of combat", "[persistence]") {
  Point2D windowSize = {800, 600};
  Point2D fieldSize = {50, 50};

  {
    auto window = SDLWindow::createWindow("Test", 0, 0, windowSize.x, windowSize.y);
    auto combat = Combat(&window, windowSize, fieldSize);

    CombatSerializer::saveCombat("test/", "test.sav", combat);
  }

  auto window = SDLWindow::createWindow("Test", 0, 0, windowSize.x, windowSize.y);
  auto json = CombatSerializer::loadFile("test/test.sav");
  Combat combatLoaded = CombatSerializer::fromJSON(&window, windowSize, json);

  REQUIRE(combatLoaded.getField().getSize() == fieldSize);
}
*/

TEST_CASE("PRNG Meyer's singleton functionality", "[misc]") {
  Random& r = Random::instance();
  r.setEngine(std::mt19937(notVeryRandomSeed));

  {
    constexpr auto mean = 10.0f;
    constexpr auto deviation = 1.0f;

    std::vector<float> generatedNumbers{};
    for (int i = 0; i <= 10; i++) {
      generatedNumbers.push_back(r.gen_normal_distributed(mean, deviation));
    }
    for (const auto& num : generatedNumbers) {
      REQUIRE((num >= 7.0f && num <= 13.0f));
    }
  }

  {
    constexpr std::uint32_t min = 5;
    constexpr std::uint32_t max = 123;

    std::vector<std::uint32_t> generatedNumbers{};
    for (int i = 0; i <= 10; i++) {
      generatedNumbers.push_back(r.gen_uniform_distributed(min, max));
    }
    for (const auto& num : generatedNumbers) {
      REQUIRE((num >= min && num <= max));
    }
  }
}

TEST_CASE("Equipped gear adds and removes modifiers", "Mob") {
  auto field = BattleField({64, 64});
  auto game = TestHelpers::singlePlayerUnit(field);

  auto weapon = std::make_shared<Mainhand>();
  weapon->modifiers =
    {{Modifier::Type::minMeleeDmg, 3},
    {Modifier::Type::maxMeleeDmg, 6},
    {Modifier::Type::phyResist, 100},
    {Modifier::Type::statSpeed, 1000},
    {Modifier::Type::maxMeleeDmg, 30},
    {Modifier::Type::statStrength, 12},
    {Modifier::Type::statMaxHealth, 20},
    {Modifier::Type::statCurHealth, 5},
    {Modifier::Type::minMeleeDmg, 3}};


  auto player = game.getCombat()->getObjectView()->getPlayer().value();
  player->equipItem(weapon, Equipment::EquipmentSlot::mainHand);

  // Did equipping a sword with modifiers add every modifier?
  REQUIRE(player->getResistances().phy.getRelative() > 0.1);
  REQUIRE(player->getStats().strength.getTotal() == 22);
  REQUIRE(player->getStats().speed.getTotal() > 1000);
  REQUIRE(player->getHealth().max > 20);
  REQUIRE(player->getHealth().current == 15);
  REQUIRE(player->getAttackDamage().max >= 6);
  REQUIRE(player->getAttackDamage().min >= 3);

  player->unequipItem(Equipment::EquipmentSlot::mainHand);
  REQUIRE(player->getResistances().phy.getRelative() == 0.0);
  REQUIRE(player->getStats().strength.getTotal() == 10);
  REQUIRE(player->getStats().speed.getTotal() < 1000);
  REQUIRE(player->getHealth().max == 10);
  REQUIRE(player->getHealth().current == 5);
  REQUIRE(player->getAttackDamage().max == 3);
  REQUIRE(player->getAttackDamage().min == 1);
}
