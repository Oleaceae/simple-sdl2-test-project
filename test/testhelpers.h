#ifndef GAME_TESTHELPERS_H
#define GAME_TESTHELPERS_H

#include "../src/game.h"

class TestHelpers {
public:
  static Game combatSample(BattleField& injectedField);
  static Game aStarCombatTestField(const BattleField& base);
  static Game interFactionCombat(const BattleField& base);
  static Game singlePlayerUnit(const BattleField& base);

private:
};

#endif
