#include <iostream>

#include "../src/game.h"
#include "../src/generation/levelgenerator.h"
#include "extern/catch.hpp"
#include "testhelpers.h"

using namespace sdlwrapp;

TEST_CASE("Cellular automata generated level", "[.manual]") {
  auto field = LevelGenerator::cellularAutomata(BattleField({128, 128}), 40, 2, 1, 5, 5);

  auto game = TestHelpers::combatSample(field);

  REQUIRE_NOTHROW(game.start());
}

TEST_CASE("Drunkard's walk generated level", "[.manual]") {
  // Random::instance().setEngine(std::mt19937(0));
  auto field = LevelGenerator::drunkardTunneling(BattleField({128, 128}), 25, 300, 14, 2, 12, 1);
  auto game = TestHelpers::combatSample(field);

  REQUIRE_NOTHROW(game.start());
}

TEST_CASE("Pathfinding AStar", "[.manual]") {
  auto field = BattleField({64, 64});
  auto game = TestHelpers::aStarCombatTestField(field);

  REQUIRE_NOTHROW(game.start());
}

TEST_CASE("Inter faction combat", "[.manual]") {
  auto field = BattleField({64, 64});
  auto game = TestHelpers::interFactionCombat(field);

  REQUIRE_NOTHROW(game.start());
}

TEST_CASE("Modifiers, equipment, menus", "[.manual]") {
  auto game = TestHelpers::singlePlayerUnit(BattleField({64, 64}));

  const auto resistancesTester = std::make_shared<Mainhand>();
  resistancesTester->modifiers = {{Modifier::Type::minMeleeDmg, 3},    {Modifier::Type::maxMeleeDmg, 6},
                                  {Modifier::Type::dthResist, 50},     {Modifier::Type::phyResist, 10},
                                  {Modifier::Type::ertResist, 5000},   {Modifier::Type::lifResist, -20},
                                  {Modifier::Type::staResist, -10000}, {Modifier::Type::wndResist, 130},
                                  {Modifier::Type::firResist, 90},     {Modifier::Type::watResistBase, 140}};

  const auto attributesTester = std::make_shared<Armor>();
  attributesTester->modifiers = {{Modifier::Type::statSpeed, 113},
                                 {Modifier::Type::statWillpower, -3},
                                 {Modifier::Type::statDexterity, 5},
                                 {Modifier::Type::statMaxHealth, 34},
                                 {Modifier::Type::statStrength, 1}};

  game.getCombat()->getControlledMob()->equipItem(resistancesTester, Equipment::EquipmentSlot::mainHand);
  game.getCombat()->getControlledMob()->equipItem(attributesTester, Equipment::EquipmentSlot::armor);

  REQUIRE_NOTHROW(game.start());
}
