# WINDOWS

### Manual installation of packages:
Get the latest version of SDL2: https://github.com/libsdl-org/SDL/releases
Get the latest version of SDL_image: https://github.com/libsdl-org/SDL_image/releases
Get the latest version of SDL_ttf: https://github.com/libsdl-org/SDL_ttf/releases
Get the latest version of SDL_mixer: https://github.com/libsdl-org/SDL_mixer/releases

### Alternatively: Use vcpkg
Ensure the environment variable VCPKG_ROOT is set as required in [CMakePresets.json](CMakePresets.json)

## Visual Studio
Add the path to each dependency to your CMakeSettings.json in the project root directory:

```
"variables": [
        {
          "name": "SDL2_DIR",
          "value": "your-path/SDL2-2.28.2",
          "type": "PATH"
        },
        {
          "name": "SDL2_ttf_DIR",
          "value": "your-path/SDL2_ttf-2.20.2",
          "type": "PATH"
        },
        {
          "name": "SDL2_image_DIR",
          "value": "your-path/SDL2_image-2.6.3",
          "type": "PATH"
        },
        {
          "name": "SDL2_mixer_DIR",
          "value": "your-path/SDL2_mixer-2.6.3",
          "type": "PATH"
        },
```

## Visual Studio Code with CMake extensions
Add the path to each dependency to your settings.json:

```
    "cmake.configureEnvironment": {
        "SDL2_DIR": "C:/Coding/Dependencies/SDL2-2.28.2",
        "SDL2_ttf_DIR": "C:/Coding/Dependencies/SDL2_ttf-2.20.2",
        "SDL2_image_DIR": "C:/Coding/Dependencies/SDL2_image-2.6.3",
        "SDL2_mixer_DIR": "C:/Coding/Dependencies/SDL2_mixer-2.6.3",
    }
```

# UNIX

sudo apt-get install libsdl2-dev
sudo apt-get install libsdl2-ttf-dev
sudo apt-get install libsdl2-image-dev
sudo apt-get install libsdl2-mixer-dev

# Dependencies for deployment

Ensure you put all .dll/.so files into dependencies/ as they are copied into the build directory by CMake