#ifndef GAME_COMBATSERIALIZER_H
#define GAME_COMBATSERIALIZER_H

#include "../combat.h"
#include "../extern/json/json.h"
#include "../model/droptable.h"
#include <filesystem>
#include <fstream>

class CombatSerializer {
public:
  /* Disabled after renderer refactor - fix this next TODO

  [[nodiscard]] static Combat fromJSON(sdlwrapp::SDLWindow* window, Point2D windowSize, const nlohmann::json& json,
                                       const DropTable::ItemAtlas& itemAtlas = {},
                                       const DropTable::DropTableAtlas& dropTableAtlas = {});
  */
  [[nodiscard]] static nlohmann::json fromCombat(const Combat& combat);
  [[nodiscard]] static nlohmann::json loadFile(const std::string& filepath);
  static void saveCombat(const std::string& filepath, const std::string& filename, const Combat& combat);

private:
};

#endif
