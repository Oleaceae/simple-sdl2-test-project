#include "combatserializer.h"

using namespace nlohmann;
using namespace sdlwrapp;

/*
Combat CombatSerializer::fromJSON(SDLWindow* window, Point2D windowSize, const json& json,
                                  const DropTable::ItemAtlas& itemAtlas,
                                  const DropTable::DropTableAtlas& dropTableAtlas) {
  Point2D fieldSize = {json["combatWidth"].template get<int>(), json["combatHeight"].template get<int>()};
  Combat out = Combat(window, windowSize, fieldSize, &itemAtlas, &dropTableAtlas);
  return out;
}
*/

nlohmann::json CombatSerializer::fromCombat(const Combat& combat) {
  json out = json();
  const auto& field = combat.getField();

  out["combatHeight"] = field.getHeight();
  out["combatWidth"] = field.getWidth();

  return out;
}

nlohmann::json CombatSerializer::loadFile(const std::string& filepath) {
  auto file = std::ifstream(filepath);
  return json::parse(file);
}

void CombatSerializer::saveCombat(const std::string& filepath, const std::string& filename, const Combat& combat) {
  std::filesystem::create_directory(filepath);
  auto file = std::ofstream(filepath + filename);
  file << fromCombat(combat).dump();
}
