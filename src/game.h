#ifndef GAME_H
#define GAME_H

#include "sdl2wrapp/sdl.h"
#include "sdl2wrapp/sdlimage.h"
#include "sdl2wrapp/sdlmixer.h"

#include "combat.h"

#include "math/point2d.h"
#include "model/gamemode.h"

class Game {
public:
  Game();

  void start();
  void loadCombat(Combat& newCombat) noexcept;
  Combat* getCombat() noexcept;

private:
  static DropTable::DropTableAtlas loadDropTableAtlas();
  static DropTable::ItemAtlas loadItemAtlas();

  Point2D windowSize{1600, 900};
  std::set<sdlwrapp::WindowFlag> windowFlags{sdlwrapp::WindowFlag::resizable};
  int windowPosX = SDL_WINDOWPOS_UNDEFINED;
  int windowPosY = SDL_WINDOWPOS_UNDEFINED;
  const std::string windowTitle{};
  DropTable::DropTableAtlas dropTableAtlas{};
  DropTable::ItemAtlas itemAtlas{};
  GameMode mode = GameMode::combat;

  sdlwrapp::SDL sdl{};
  sdlwrapp::SDLImage sdlimage{};
  sdlwrapp::SDLMixer sdlmixer{};
  sdlwrapp::SDLWindow window{};
  sdlwrapp::SDLRenderer renderer{};
  Combat combat;
};

#endif
