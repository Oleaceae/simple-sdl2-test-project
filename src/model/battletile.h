#ifndef BATTLETILE_H
#define BATTLETILE_H

#include <expected>
#include <list>
#include <memory>

#include "../math/point2d.h"
#include "blocker.h"
#include "errortype.h"
#include "mob.h"

class BattleField;

template <typename T>
concept ItemType = std::is_base_of<Item, T>::value;

class BattleTile {
public:
  BattleTile() = default;
  explicit BattleTile(BattleField* field);

  void add(const Mob& mob) noexcept;
  void add(const std::shared_ptr<Mob>& mob) noexcept;
  void remove(const std::shared_ptr<Mob>& mob) noexcept;
  void add(const Blocker& blocker) noexcept;
  void add(const std::shared_ptr<Blocker>& blocker) noexcept;
  void remove(const std::shared_ptr<Blocker>& blocker) noexcept;
  template <ItemType T> void add(const T& item) noexcept;
  void clearBlockers() noexcept;
  void add(const std::shared_ptr<Item>& item) noexcept;
  void remove(const std::shared_ptr<Item>& item) noexcept;

  // Ignore the object view when removing objects, used by map generation algorithms for performance reasons.
  // A call to the object view's reload function is required after mapgen is done, as the field and ov will be out of sync
  void removeWithoutObjectView(const std::shared_ptr<Blocker>& blocker) noexcept;
  void clearBlockersWithoutObjectView() noexcept;

  [[nodiscard]] const std::list<std::shared_ptr<Mob>>& getMobs() noexcept;
  [[nodiscard]] bool hasMob() const noexcept;
  [[nodiscard]] bool hasBlocker() const noexcept;
  [[nodiscard]] bool hasItem() const noexcept;
  [[nodiscard]] bool blocksMovement() const noexcept;

  [[nodiscard]] std::expected<std::shared_ptr<Mob>, ErrorType> getFirstMob() noexcept;
  [[nodiscard]] std::expected<std::shared_ptr<Mob>, ErrorType> getMobByID(std::size_t id) noexcept;

  [[nodiscard]] std::expected<std::shared_ptr<Item>, ErrorType> takeFirstItem() noexcept;
  [[nodiscard]] std::expected<std::shared_ptr<const Item>, ErrorType> getFirstItem() const noexcept;

  [[nodiscard]] std::expected<const std::shared_ptr<Blocker>, ErrorType> getFirstBlocker() const noexcept;

  [[nodiscard]] std::expected<const std::uint64_t, ErrorType> getFirstMobIconID() const noexcept;
  [[nodiscard]] std::expected<const std::uint64_t, ErrorType> getFirstBlockerIconID() const noexcept;
  [[nodiscard]] std::expected<const std::uint64_t, ErrorType> getFirstItemIconID() const noexcept;

  void setField(BattleField* newField) noexcept;

  Point2D location{};

  void initializeObjects() noexcept;

private:
  std::list<std::shared_ptr<Mob>> mobs{};
  std::list<std::shared_ptr<Blocker>> blockers{};
  std::list<std::shared_ptr<Item>> items{};
  BattleField* field{nullptr};
};

#endif
