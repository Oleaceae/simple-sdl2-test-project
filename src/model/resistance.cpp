#include "resistance.h"
#include "mob.h"

#include <cmath>

void Resistance::calculateRelative() noexcept {
  this->rel = this->base;

  // f(x, y) = x / (y + |x|)
  this->rel += static_cast<float>(abs) / static_cast<float>((diminishingFactor + std::abs(abs)));
}

int Resistance::getAbsolute() const noexcept {
  return this->abs;
}

float Resistance::getRelative() const noexcept {
  return this->rel;
}

Resistance& Resistance::operator+=(const Resistance& res) {
  this->abs += res.abs;
  this->base += res.base;
  this->calculateRelative();
  return *this;
}

std::string Phy::toString() const noexcept {
  return {};
}

std::string Sta::toString() const noexcept {
  return {};
}

std::string Fir::toString() const noexcept {
  return {};
}

std::string Wat::toString() const noexcept {
  return {};
}

std::string Ert::toString() const noexcept {
  return {};
}

std::string Wnd::toString() const noexcept {
  return {};
}

std::string Lif::toString() const noexcept {
  return {};
}

std::string Dth::toString() const noexcept {
  return {};
}

void Resistances::add(const Resistances& other) noexcept {
  this->phy = other.phy;
  this->sta = other.sta;
  this->fir = other.fir;
  this->wat = other.wat;
  this->ert = other.ert;
  this->wnd = other.wnd;
  this->lif = other.lif;
  this->dth = other.dth;
}

void Resistances::add(const Phy& other) noexcept {
  this->phy += other;
}

void Resistances::add(const Sta& other) noexcept {
  this->sta += other;
}

void Resistances::add(const Fir& other) noexcept {
  this->fir += other;
}

void Resistances::add(const Wat& other) noexcept {
  this->wat += other;
}

void Resistances::add(const Ert& other) noexcept {
  this->ert += other;
}

void Resistances::add(const Wnd& other) noexcept {
  this->wnd += other;
}

void Resistances::add(const Lif& other) noexcept {
  this->lif += other;
}

void Resistances::add(const Dth& other) noexcept {
  this->dth += other;
}

void Resistances::updateRelativeValues() noexcept {
  this->phy.calculateRelative();
  this->sta.calculateRelative();
  this->fir.calculateRelative();
  this->wat.calculateRelative();
  this->ert.calculateRelative();
  this->wnd.calculateRelative();
  this->lif.calculateRelative();
  this->dth.calculateRelative();
}

std::array<float, 8> Resistances::toArray() const {
  return {
    this->phy.getRelative(),
    this->sta.getRelative(),
    this->fir.getRelative(),
    this->wat.getRelative(),
    this->ert.getRelative(),
    this->wnd.getRelative(),
    this->lif.getRelative(),
    this->dth.getRelative()
  };
}
