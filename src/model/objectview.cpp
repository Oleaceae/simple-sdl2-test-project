#include "objectview.h"

#include "battlefield.h"

void ObjectView::add(const std::shared_ptr<Blocker>& object) noexcept {
  this->blockers.emplace_back(object);
}

void ObjectView::add(const std::shared_ptr<Item>& object) noexcept {
  this->items.emplace_back(object);
}

void ObjectView::add(const std::shared_ptr<Mob>& object) noexcept {
  this->mobs.emplace_back(object);
}

void ObjectView::remove(const std::shared_ptr<Blocker>& object) noexcept {
  removeSingle(object, this->blockers);
}

void ObjectView::remove(const std::shared_ptr<Item>& object) noexcept {
  removeSingle(object, this->items);
}

void ObjectView::remove(const std::shared_ptr<Mob>& object) noexcept {
  removeSingle(object, this->mobs);
}

const std::list<View<const Blocker>>& ObjectView::getBlockers() const noexcept {
  return this->blockers;
}

const std::list<View<const Item>>& ObjectView::getItems() const noexcept {
  return this->items;
}

const std::list<View<const Mob>>& ObjectView::getMobs() const noexcept {
  return this->mobs;
}

void ObjectView::setPlayerMob(const std::shared_ptr<Mob>& newPlayerMob) noexcept {
  this->playerMob = newPlayerMob;
}

std::expected<const Statistics*, ErrorType> ObjectView::getPlayerStats() const noexcept {
  if (this->playerMob != nullptr) {
    return &this->playerMob->getStats();
  } else {
    return std::unexpected<ErrorType>{ErrorType::noneFound};
  }
}

std::expected<const std::shared_ptr<Mob>, ErrorType> ObjectView::getPlayer() const noexcept {
  if (this->playerMob != nullptr) {
    return this->playerMob;
  }
  return std::unexpected<ErrorType>{ErrorType::noneFound};
}

void ObjectView::reloadBlockers(const BattleField& field) {
  this->blockers.clear();
  field.getTiles()->forEach([&](const BattleTile& tile) {
    auto blocker = tile.getFirstBlocker();
    if (blocker.has_value()) {
      this->add(blocker.value());
    }
  });
}

template <typename T> void ObjectView::removeSingle(const std::shared_ptr<T>& object, std::list<View<const T>>& list) {
  for (auto it = list.begin(); it != list.end(); it++) {
    if (object == it->getObject()) {
      list.erase(it);
      break;
    }
  }
}
