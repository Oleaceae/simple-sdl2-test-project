#include "mob.h"

#include <algorithm>
#include <utility>

#include "../events/messagequeue.h"
#include "../math/random.h"
#include "battletile.h"
#include "battletile.t.h"

void CurMax::increaseMax(const int value) noexcept {
  this->max += value;
  if (this->current > max) {
    this->current = max;
  }
}

void CurMax::refill(const int value) noexcept {
  this->current += value;
  if (this->current > max) {
    this->current = max;
  }
}

Mob::Mob() {
  activeAI = availableAIs.at("Idle");
}

Mob::Mob(const std::uint64_t iconID, Faction faction, const Race race, const bool pcControlled)
    : iconID(iconID), race(race), faction(std::move(faction)), playerControlled(pcControlled) {
  activeAI = availableAIs.at("Idle");
}

bool Mob::operator==(const Mob& other) const {
  return this->id == other.id;
}

bool Mob::attack(Mob& enemy) const noexcept {
  const auto damageDealt =
      static_cast<int32_t>(Random::instance().gen_uniform_distributed(this->attackDamage.min, this->attackDamage.max));

  enemy.health.current = enemy.health.current - damageDealt;
  MessageQueue::instance().add(std::to_string(this->id) + " attacked " + std::to_string(enemy.id) + " for " +
                               std::to_string(damageDealt) + " damage.");
  if (enemy.isDead()) {
    return true;
  }
  return false;
}

bool Mob::isDead() const noexcept {
  return this->health.current <= 0;
}

void Mob::pickupItem() noexcept {
  auto tile = this->location;
  if (!tile->hasItem()) {
    return;
  }
  const auto& item = tile->takeFirstItem().value();
  this->inventory.add(item);
  // TODO get gcc 13 for std format..
  MessageQueue::instance().add(""
                               "You picked up a " +
                               item->description);
}

void Mob::onDeath() noexcept {
  // nothing yet
}

void Mob::dropLoot(const DropTable::ItemAtlas& itemAtlas, const DropTable::DropTableAtlas& dropTableAtlas) noexcept {
  auto loot = dropTableAtlas.at(this->dropTableID).rollTable(itemAtlas, dropTableAtlas);
  for (const auto& item : loot.loot) {
    this->addLootToTile(item);
  }
}

const Statistics& Mob::getStats() const noexcept {
  return this->statistics;
}

const Health& Mob::getHealth() const noexcept {
  return this->health;
}

const Mana& Mob::getMana() const noexcept {
  return this->mana;
}

const Equipment& Mob::getEquipment() const noexcept {
  return this->equipment;
}

const Resistances& Mob::getResistances() const noexcept {
  return this->resistances;
}

const Damage& Mob::getAttackDamage() const noexcept {
  return this->attackDamage;
}

void Mob::equipItem(const std::shared_ptr<Item>& item, const Equipment::EquipmentSlot slot) noexcept {
  if (!item->isEquipable()) {
    return;
  }

  const auto unequippedItem = this->equipment.equip(item, slot);

  if (unequippedItem.has_value()) {
    std::ranges::for_each(unequippedItem.value()->getModifiers(),
                          [this](const Modifier& mod) { this->removeModifier(mod); });
  }

  std::ranges::for_each(item->getModifiers(), [this](const Modifier& mod) { this->addModifier(mod); });
}

void Mob::unequipItem(const Equipment::EquipmentSlot slot) noexcept {
  const auto unequippedItem = this->equipment.unequip(slot);

  if (unequippedItem.has_value()) {
    this->inventory.add(unequippedItem.value());
    std::ranges::for_each(unequippedItem.value()->getModifiers(),
                          [this](const Modifier& mod) { this->removeModifier(mod); });
  }
}

void Mob::addModifier(const Modifier& mod) noexcept {
  switch (mod.type) {
  case Modifier::Type::none:
    return;
  case Modifier::Type::phyResist:
    this->resistances.add(Phy(mod.value));
    break;
  case Modifier::Type::staResist:
    this->resistances.add(Sta(mod.value));
    break;
  case Modifier::Type::firResist:
    this->resistances.add(Fir(mod.value));
    break;
  case Modifier::Type::watResist:
    this->resistances.add(Wat(mod.value));
    break;
  case Modifier::Type::ertResist:
    this->resistances.add(Ert(mod.value));
    break;
  case Modifier::Type::wndResist:
    this->resistances.add(Wnd(mod.value));
    break;
  case Modifier::Type::lifResist:
    this->resistances.add(Lif(mod.value));
    break;
  case Modifier::Type::dthResist:
    this->resistances.add(Dth(mod.value));
    break;
  case Modifier::Type::minMeleeDmg:
    this->attackDamage.min += mod.value;
    break;
  case Modifier::Type::maxMeleeDmg:
    this->attackDamage.max += mod.value;
    break;
  case Modifier::Type::statMaxHealth:
    this->health.increaseMax(mod.value);
    break;
  case Modifier::Type::statCurHealth:
    this->health.refill(mod.value);
    break;
  case Modifier::Type::statMaxMana:
    this->mana.increaseMax(mod.value);
    break;
  case Modifier::Type::statCurMana:
    this->mana.refill(mod.value);
    break;
  case Modifier::Type::statStrength:
    this->statistics.strength.addBonus(mod.value);
    break;
  case Modifier::Type::statConstitution:
    this->statistics.constitution.addBonus(mod.value);
    break;
  case Modifier::Type::statMind:
    this->statistics.mind.addBonus(mod.value);
    break;
  case Modifier::Type::statWillpower:
    this->statistics.willpower.addBonus(mod.value);
    break;
  case Modifier::Type::statDexterity:
    this->statistics.dexterity.addBonus(mod.value);
    break;
  case Modifier::Type::statSpeed:
    this->statistics.speed.addBonus(mod.value);
    break;
  case Modifier::Type::phyResistBase:
    this->resistances.add(Phy(static_cast<float>(mod.value) / 100.0f));
    break;
  case Modifier::Type::staResistBase:
    this->resistances.add(Sta(static_cast<float>(mod.value) / 100.0f));
    break;
  case Modifier::Type::firResistBase:
    this->resistances.add(Fir(static_cast<float>(mod.value) / 100.0f));
    break;
  case Modifier::Type::watResistBase:
    this->resistances.add(Wat(static_cast<float>(mod.value) / 100.0f));
    break;
  case Modifier::Type::ertResistBase:
    this->resistances.add(Ert(static_cast<float>(mod.value) / 100.0f));
    break;
  case Modifier::Type::wndResistBase:
    this->resistances.add(Wnd(static_cast<float>(mod.value) / 100.0f));
    break;
  case Modifier::Type::lifResistBase:
    this->resistances.add(Lif(static_cast<float>(mod.value) / 100.0f));
    break;
  case Modifier::Type::dthResistBase:
    this->resistances.add(Dth(static_cast<float>(mod.value) / 100.0f));
    break;
  case Modifier::Type::armorPen:
    this->attackDamage.armorPen += static_cast<float>(mod.value) / 100.0f;
    break;
  }
}

void Mob::removeModifier(const Modifier& mod) noexcept {
  addModifier(-mod);
}

void Mob::addLootToTile(const std::shared_ptr<Item>& loot) const noexcept {
  this->location->add(loot);
}

bool Mob::isPlayerControlled() const noexcept {
  return this->playerControlled;
}

void Mob::setPlayerControlled(const bool flag) noexcept {
  this->playerControlled = flag;
}

void Mob::setHp(const std::int32_t hp) noexcept {
  if (hp > health.max) {
    this->health.current = health.max;
  } else {
    this->health.current = hp;
  }
}

void Mob::setSpeed(const std::int32_t spd) noexcept {
  this->statistics.speed.base = spd;
}

void Mob::setMaxHp(const std::int32_t maxHp) noexcept {
  this->health.max = maxHp;
}

std::uint16_t Mob::tryTurn(const std::int32_t speedThreshold) noexcept {
  this->statistics.elapsedTimeBeforeTurn += this->statistics.speed.getTotal();
  std::uint16_t turnsToTake{};
  while (this->statistics.elapsedTimeBeforeTurn >= speedThreshold) {
    ++turnsToTake;
    this->statistics.elapsedTimeBeforeTurn -= speedThreshold;
  }
  return turnsToTake;
}
