#include "droptable.h"
#include <unordered_map>

DropTable::DropTable(const std::vector<std::tuple<chance, itemID>>& items,
                     const std::vector<std::tuple<chance, dropTableID>>& dropTables) noexcept {
  this->drops = items;
  this->dropTables = dropTables;
}

Loot DropTable::rollTable(const ItemAtlas& itemAtlas, const DropTableAtlas& dropTableAtlas) const noexcept {
  Loot loot{};

  for (const auto& dropTable : this->dropTables) {
    auto chance = std::get<0>(dropTable);
    if (DropTable::roll(chance)) {
      Loot dtLoot = dropTableAtlas.at(std::get<1>(dropTable)).rollTable(itemAtlas, dropTableAtlas);
      for (const auto& item : dtLoot.loot) {
        loot.loot.push_back(item);
      }
    }
  }

  for (const auto& item : this->drops) {
    auto chance = std::get<0>(item);
    if (DropTable::roll(chance)) {
      loot.loot.push_back(itemAtlas.at(std::get<1>(item)));
    }
  }
  return loot;
}

bool DropTable::roll(DropTable::chance chance) noexcept {
  auto rolled = Random::instance().gen_uniform_distributed(0, DropTable::hundredPercentChance);
  if (rolled <= chance) {
    return true;
  }
  return false;
}
