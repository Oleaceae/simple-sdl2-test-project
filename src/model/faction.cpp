#include "faction.h"

#include <algorithm>

std::unordered_map<Faction::Type, std::vector<Faction::Type>> Faction::factionRelations = {
    {Type::player, {Type::undead}},
    {Type::undead, {Type::player}},
    {Type::wildlife, {}},
};

Faction::Faction(const Type type) : type(type) {
  if (factionRelations.contains(type)) {
    this->friends = factionRelations.at(this->type);
  }
}

std::string Faction::toString() const noexcept {
  switch (this->type) {
  case Type::player:
    return "Player";
  case Type::wildlife:
    return "Wildlife";
  case Type::undead:
    return "Undead";
  default:
    return "Unknown faction";
  }
}

[[nodiscard]] bool Faction::isFriend(const Faction& other) const noexcept {
  if (other.type == this->type) {
    return true;
  }
  return std::ranges::any_of(this->friends, [&](const Type type) { return type == other.type; });
}

bool Faction::operator==(const Faction& other) const {
  return this->type == other.type;
}

bool Faction::operator!=(const Faction& other) const {
  return !(*this == other);
}
