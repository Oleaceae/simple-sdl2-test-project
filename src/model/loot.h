#ifndef GAME_LOOT_H
#define GAME_LOOT_H

#include <vector>
#include <memory>

#include "equipment/item.h"

class Loot {
public:
  std::vector<std::shared_ptr<Item>> loot{};

private:
};

#endif
