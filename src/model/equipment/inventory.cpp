#include "inventory.h"

#include "../../events/messagequeue.h"

void Inventory::add(const std::shared_ptr<Item>& item) noexcept {
  this->items.push_back(item);
}
