#ifndef INVENTORY_H
#define INVENTORY_H

#include <memory>
#include <vector>

#include "item.h"

class Inventory {
public:
  void add(const std::shared_ptr<Item>& item) noexcept;

private:
  std::vector<std::shared_ptr<Item>> items{};
};

#endif
