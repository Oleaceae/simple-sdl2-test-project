#ifndef EQUIPMENT_H
#define EQUIPMENT_H

#include "item.h"

#include <memory>
#include <optional>

class Item;

struct Resistances;

class Equipment {
public:
  enum class EquipmentSlot { mainHand, offHand, armor, accessory_1, accessory_2 };

  std::optional<std::shared_ptr<Mainhand>> mainHand{{std::nullopt}};
  std::optional<std::shared_ptr<Offhand>> offHand{{std::nullopt}};
  std::optional<std::shared_ptr<Armor>> armor{{std::nullopt}};
  std::optional<std::shared_ptr<Accessory>> accessory_1{{std::nullopt}};
  std::optional<std::shared_ptr<Accessory>> accessory_2{{std::nullopt}};

  // Returns the unequipped item if one existed
  [[nodiscard]] std::optional<std::shared_ptr<Item>> equip(const std::shared_ptr<Item>& item, EquipmentSlot slot);
  [[nodiscard]] std::optional<std::shared_ptr<Item>> unequip(EquipmentSlot slot) noexcept;

  [[nodiscard]] static bool isEquipableToSlot(const std::shared_ptr<Item>& item, EquipmentSlot slot) noexcept;
  [[nodiscard]] static std::string slotToString(EquipmentSlot slot) noexcept;
};

class IllegalEquipAction final : std::exception {
public:
  IllegalEquipAction(const std::shared_ptr<Item>&, Equipment::EquipmentSlot slot);
  [[nodiscard]] const char* what() const noexcept override;

private:
  std::string errorMessage{};
};

#endif
