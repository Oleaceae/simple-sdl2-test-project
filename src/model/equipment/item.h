#ifndef ITEM_H
#define ITEM_H

#include <cstdint>
#include <list>
#include <vector>
#include <set>

#include "../resistance.h"

class BattleTile;

class Modifier {
public:
  enum class Type {
    none,
    minMeleeDmg,
    maxMeleeDmg,
    statMaxHealth,
    statCurHealth,
    statMaxMana,
    statCurMana,
    statStrength,
    statConstitution,
    statMind,
    statWillpower,
    statDexterity,
    statSpeed,
    phyResist,
    staResist,
    firResist,
    watResist,
    ertResist,
    wndResist,
    lifResist,
    dthResist,
    phyResistBase,
    staResistBase,
    firResistBase,
    watResistBase,
    ertResistBase,
    wndResistBase,
    lifResistBase,
    dthResistBase,
    armorPen
  };

  Type type{};
  int value{};

  Modifier operator-() const noexcept;
  bool operator<(const Modifier& other) const noexcept;
};

class Item {
public:
  enum class Type { item, mainhand, offhand, armor, accessory };

  virtual ~Item() = default;

  bool operator==(const Item& other) const {
    return this->itemID == other.itemID;
  }

  std::string name{"Missing item name."};
  std::string description{"Missing item description."};
  std::size_t itemID{};
  std::size_t iconID{1};
  BattleTile* location{nullptr};

  [[nodiscard]] virtual bool isEquipable() const noexcept;
  [[nodiscard]] virtual Type getType() const noexcept;
  [[nodiscard]] virtual std::list<Modifier> getModifiers() const noexcept;

private:
};

class EquipableItem : public Item {
public:
  std::set<Modifier> modifiers{};

  [[nodiscard]] bool isEquipable() const noexcept override;
  [[nodiscard]] std::list<Modifier> getModifiers() const noexcept override;

private:
};

class Mainhand final : public EquipableItem {
public:
  [[nodiscard]] Type getType() const noexcept override;
};

class Offhand final : public EquipableItem {
  [[nodiscard]] Type getType() const noexcept override;
};

class Armor final : public EquipableItem {
public:
  [[nodiscard]] Type getType() const noexcept override;
};

class Accessory final : public EquipableItem {
public:
  [[nodiscard]] Type getType() const noexcept override;
};


#endif
