#include "equipment.h"

#include <format>

std::optional<std::shared_ptr<Item>> Equipment::equip(const std::shared_ptr<Item>& item, const EquipmentSlot slot) {
  std::optional<std::shared_ptr<Item>> ret = std::nullopt;

  if (!isEquipableToSlot(item, slot)) {
    throw IllegalEquipAction(item, slot);
  }

  switch (slot) {
  case EquipmentSlot::mainHand:
    ret = this->mainHand;
    this->mainHand = std::static_pointer_cast<Mainhand>(item);
    return ret;
  case EquipmentSlot::offHand:
    ret = this->offHand;
    this->offHand = std::static_pointer_cast<Offhand>(item);
    return ret;
  case EquipmentSlot::armor:
    ret = this->armor;
    this->armor = std::static_pointer_cast<Armor>(item);
    return ret;
  case EquipmentSlot::accessory_1:
    ret = this->accessory_1;
    this->accessory_1 = std::static_pointer_cast<Accessory>(item);
    return ret;
  case EquipmentSlot::accessory_2:
    ret = this->accessory_2;
    this->accessory_2 = std::static_pointer_cast<Accessory>(item);
    return ret;
  }
  return ret;
}

std::optional<std::shared_ptr<Item>> Equipment::unequip(EquipmentSlot slot) noexcept {
  std::optional<std::shared_ptr<Item>> ret = std::nullopt;

  switch (slot) {
  case EquipmentSlot::mainHand:
    ret = this->mainHand;
    this->mainHand = std::nullopt;
    return ret;
  case EquipmentSlot::offHand:
    ret = this->offHand;
    this->offHand = std::nullopt;
    return ret;
  case EquipmentSlot::armor:
    ret = this->armor;
    this->armor = std::nullopt;
    return ret;
  case EquipmentSlot::accessory_1:
    ret = this->accessory_1;
    this->accessory_1 = std::nullopt;
    return ret;
  case EquipmentSlot::accessory_2:
    ret = this->accessory_2;
    this->accessory_2 = std::nullopt;
    return ret;
  }
  return ret;
}

bool Equipment::isEquipableToSlot(const std::shared_ptr<Item>& item, EquipmentSlot slot) noexcept {
  switch (item->getType()) {
  case Item::Type::mainhand:
    return slot == EquipmentSlot::mainHand;
  case Item::Type::offhand:
    return slot == EquipmentSlot::offHand;
  case Item::Type::armor:
    return slot == EquipmentSlot::armor;
  case Item::Type::accessory:
    return slot == EquipmentSlot::accessory_1 || slot == EquipmentSlot::accessory_2;
  default:
    return false;
  }
}

std::string Equipment::slotToString(const EquipmentSlot slot) noexcept {
  switch (slot) {
  case EquipmentSlot::mainHand:
    return "mainhand";
  case EquipmentSlot::offHand:
    return "offhand";
  case EquipmentSlot::armor:
    return "armor";
  case EquipmentSlot::accessory_1:
    return "accessory1";
  case EquipmentSlot::accessory_2:
    return "accessory2";
  default:
    return "Unknown slot";
  }
}

IllegalEquipAction::IllegalEquipAction(const std::shared_ptr<Item>& item, const Equipment::EquipmentSlot slot) {
  errorMessage = std::format("Tried equipping item {} into slot {}.", item->name, Equipment::slotToString(slot));
}

const char* IllegalEquipAction::what() const noexcept {
  return errorMessage.c_str();
}
