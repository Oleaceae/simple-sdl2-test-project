#include "item.h"

Modifier Modifier::operator-() const noexcept {
  return {this->type, -this->value};
}

bool Modifier::operator<(const Modifier& other) const noexcept {
  return this->type < other.type;
}

bool Item::isEquipable() const noexcept {
  return false;
}

Item::Type Item::getType() const noexcept {
  return Type::item;
}

std::list<Modifier> Item::getModifiers() const noexcept {
  return {};
}

bool EquipableItem::isEquipable() const noexcept {
  return true;
}

std::list<Modifier> EquipableItem::getModifiers() const noexcept {
  return {this->modifiers.begin(), this->modifiers.end()};
}

Item::Type Mainhand::getType() const noexcept {
  return Type::mainhand;
}

Item::Type Offhand::getType() const noexcept {
  return Type::offhand;
}

Item::Type Armor::getType() const noexcept {
  return Type::armor;
}

Item::Type Accessory::getType() const noexcept {
  return Type::accessory;
}
