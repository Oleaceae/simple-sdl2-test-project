#ifndef GAME_OBJECTVIEW_H
#define GAME_OBJECTVIEW_H

#include <expected>
#include <list>
#include <memory>

#include "../math/point2d.h"
#include "blocker.h"
#include "equipment/item.h"
#include "errortype.h"
#include "mob.h"
#include "view.h"

class BattleField;

class ObjectView {
public:
  void add(const std::shared_ptr<Blocker>& object) noexcept;
  void add(const std::shared_ptr<Item>& object) noexcept;
  void add(const std::shared_ptr<Mob>& object) noexcept;

  void remove(const std::shared_ptr<Blocker>& object) noexcept;
  void remove(const std::shared_ptr<Item>& object) noexcept;
  void remove(const std::shared_ptr<Mob>& object) noexcept;

  [[nodiscard]] const std::list<View<const Blocker>>& getBlockers() const noexcept;
  [[nodiscard]] const std::list<View<const Item>>& getItems() const noexcept;
  [[nodiscard]] const std::list<View<const Mob>>& getMobs() const noexcept;

  void setPlayerMob(const std::shared_ptr<Mob>& playerMob) noexcept;
  [[nodiscard]] std::expected<const Statistics*, ErrorType> getPlayerStats() const noexcept;
  [[nodiscard]] std::expected<const std::shared_ptr<Mob>, ErrorType> getPlayer() const noexcept;

  void reloadBlockers(const BattleField& field);

private:
  std::list<View<const Blocker>> blockers{};
  std::list<View<const Item>> items{};
  std::list<View<const Mob>> mobs{};
  std::shared_ptr<Mob> playerMob{};

  template <typename T> static void removeSingle(const std::shared_ptr<T>& object, std::list<View<const T>>& list);
};

#endif
