#ifndef GAME_ERRORTYPE_H
#define GAME_ERRORTYPE_H

// Used by std::unexpected
enum class ErrorType { noneFound };

#endif
