#ifndef GAME_DROPTABLE_H
#define GAME_DROPTABLE_H

#include "../math/random.h"
#include "loot.h"
#include <tuple>
#include <unordered_map>

class DropTable {
public:
  typedef std::uint32_t itemID;
  typedef std::uint32_t chance; // x in 10.000
  typedef std::uint32_t dropTableID;
  typedef std::unordered_map<DropTable::itemID, std::shared_ptr<Item>> ItemAtlas;
  typedef std::unordered_map<DropTable::dropTableID, DropTable> DropTableAtlas;

  explicit DropTable(const std::vector<std::tuple<chance, itemID>>&,
                     const std::vector<std::tuple<chance, dropTableID>>& = {}) noexcept;
  [[nodiscard]] Loot rollTable(const ItemAtlas& itemAtlas, const DropTableAtlas& dropTableAtlas) const noexcept;

private:
  [[nodiscard]] static bool roll(chance) noexcept;

  std::vector<std::tuple<chance, itemID>> drops{};
  std::vector<std::tuple<chance, dropTableID>> dropTables{};
  static constexpr std::uint32_t hundredPercentChance{10000};
};

#endif
