#ifndef MATRIX_H
#define MATRIX_H

#include <functional>
#include <vector>

template <typename T> class Matrix2d {
public:
  Matrix2d() {
    matrix = std::vector<std::vector<T>>(0);
  }

  Matrix2d(std::size_t height, std::size_t width) {
    matrix = std::vector<std::vector<T>>(height);
    for (auto& row : matrix) {
      row = std::vector<T>(width);
    }
  }

  T& operator()(std::size_t row, std::size_t column) {
    return matrix.at(row).at(column);
  }

  const T& operator()(std::size_t row, std::size_t column) const {
    return matrix.at(row).at(column);
  }

  std::vector<T>& operator()(std::size_t x) const {
    return matrix.at(x);
  }

  std::vector<std::vector<T>>& getMatrix() const {
    return this->matrix;
  };

  void forEach(std::function<void(T&)> func) {
    for (auto& row : matrix) {
      for (auto& cell : row) {
        func(cell);
      }
    }
  }

  void forEach(std::function<void(const T&)> func) const {
    for (auto& row : matrix) {
      for (auto& cell : row) {
        func(cell);
      }
    }
  }

private:
  std::vector<std::vector<T>> matrix;
};

#endif
