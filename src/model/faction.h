#ifndef GAME_FACTION_H
#define GAME_FACTION_H

#include <string>
#include <unordered_map>
#include <vector>

class Faction {
public:
  enum class Type {
    wildlife,
    undead,
    player,
  };

  Faction() = default;
  explicit Faction(Type type);

  [[nodiscard]] std::string toString() const noexcept;
  [[nodiscard]] bool isFriend(const Faction& other) const noexcept;

  Type type{};

  bool operator==(const Faction& other) const;
  bool operator!=(const Faction& other) const;
private:
  std::vector<Type> friends{};

  static std::unordered_map<Type, std::vector<Type>> factionRelations;
};

#endif
