#ifndef GAME_STATISTICS_H
#define GAME_STATISTICS_H

#include <cstdint>

struct Attribute {
  Attribute() = default;
  explicit Attribute(const std::int32_t base) : base(base){};
  std::int32_t base{10};
  std::int32_t bonus{0};

  void addBonus(std:: int32_t add) noexcept;
  void addBase(std:: int32_t add) noexcept;
  [[nodiscard]] std::int32_t getTotal() const noexcept;
};

struct Statistics {
  Attribute strength{10};
  Attribute dexterity{10};
  Attribute constitution{10};
  Attribute willpower{10};
  Attribute mind{10};

  Attribute speed{100};
  std::uint32_t elapsedTimeBeforeTurn{0};
};

#endif
