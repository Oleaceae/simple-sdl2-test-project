#ifndef BATTLEFIELD_H
#define BATTLEFIELD_H

#include <stdexcept>
#include <tuple>

#include "../math/point2d.h"
#include "battletile.h"
#include "matrix2d.h"
#include "mob.h"
#include "objectview.h"
#include "view.h"

class Combat;

class BattleField {
public:
  BattleField() = delete;
  explicit BattleField(Point2D fieldSize, Combat* combat = nullptr);
  BattleField(const BattleField& other);

  BattleField& operator=(const BattleField& other);

  void initializeBattleTiles();

  [[nodiscard]] Matrix2d<BattleTile>* getTiles();
  [[nodiscard]] const Matrix2d<BattleTile>* getTiles() const;

  BattleTile& operator()(Point2D point);

  std::tuple<bool, std::shared_ptr<Mob>> moveMob(const std::shared_ptr<Mob>& mob,
                                                 Point2D direction); // Tuple bool is true if mob was moved
  bool movePlayerMob(std::shared_ptr<Mob> mob, Point2D direction, Combat& controllingCombat);
  [[nodiscard]] BattleTile& getTile(const Point2D& location);
  [[nodiscard]] const BattleTile& getTile(const Point2D& location) const;
  bool placeAt(Blocker blocker, const Point2D& location) noexcept;
  bool placeAt(Mob obj, const Point2D& location) noexcept;
  bool placeAt(Item item, const Point2D& location) noexcept;
  void setCombat(Combat* newCombat) noexcept;

  [[nodiscard]] std::size_t getWidth() const;
  [[nodiscard]] std::size_t getHeight() const;

  [[nodiscard]] Point2D getSize() const;

  ObjectView objectView{};

private:
  std::size_t width{};
  std::size_t height{};
  std::size_t idCounter{};

  Matrix2d<BattleTile> tiles{};
  Combat* combat{nullptr};
};

#endif
