#include "battletile.h"
#include "battlefield.h"
#include <algorithm>

BattleTile::BattleTile(BattleField* field) : field(field) {
}

void BattleTile::add(const Mob& mob) noexcept {
  auto sharedMob = std::make_shared<Mob>(mob);
  this->add(sharedMob);
}

void BattleTile::remove(const std::shared_ptr<Mob>& mob) noexcept {
  field->objectView.remove(mob);
  mobs.remove(mob);
}

void BattleTile::add(const Blocker& blocker) noexcept {
  auto sharedBlocker = std::make_shared<Blocker>(blocker);
  this->add(sharedBlocker);
}

void BattleTile::remove(const std::shared_ptr<Blocker>& blocker) noexcept {
  field->objectView.remove(blocker);
  blockers.remove(blocker);
}

void BattleTile::remove(const std::shared_ptr<Item>& item) noexcept {
  field->objectView.remove(item);
  items.remove(item);
}

void BattleTile::removeWithoutObjectView(const std::shared_ptr<Blocker>& blocker) noexcept {
  blockers.remove(blocker);
}

void BattleTile::clearBlockersWithoutObjectView() noexcept {
  this->blockers.clear();
}

void BattleTile::add(const std::shared_ptr<Mob>& mob) noexcept {
  mob->location = this;
  mobs.push_front(mob);
  field->objectView.add(mob);
}

void BattleTile::add(const std::shared_ptr<Blocker>& blocker) noexcept {
  blocker->location = this;
  blockers.push_front(blocker);
  field->objectView.add(blocker);
}

void BattleTile::add(const std::shared_ptr<Item>& item) noexcept {
  item->location = this;
  items.push_front(item);
  field->objectView.add(item);
}

const std::list<std::shared_ptr<Mob>>& BattleTile::getMobs() noexcept {
  return this->mobs;
}

bool BattleTile::hasMob() const noexcept {
  return !this->mobs.empty();
}

bool BattleTile::hasBlocker() const noexcept {
  return !this->blockers.empty();
}

bool BattleTile::hasItem() const noexcept {
  return !this->items.empty();
}

std::expected<std::shared_ptr<Mob>, ErrorType> BattleTile::getFirstMob() noexcept {
  if (this->hasMob()) {
    return this->mobs.front();
  }
  return std::unexpected<ErrorType>{ErrorType::noneFound};
}

std::expected<std::shared_ptr<Mob>, ErrorType> BattleTile::getMobByID(std::size_t id) noexcept {
  auto result = std::find_if(this->mobs.begin(), this->mobs.end(),
                             [=](const std::shared_ptr<Mob>& mob) { return mob->id == id; });

  if (result != this->mobs.end()) {
    return *result;
  }
  return std::unexpected<ErrorType>{ErrorType::noneFound};
}

std::expected<const std::uint64_t, ErrorType> BattleTile::getFirstMobIconID() const noexcept {
  if (this->hasMob()) {
    return this->mobs.front()->iconID;
  }
  return std::unexpected<ErrorType>{ErrorType::noneFound};
}

std::expected<const std::uint64_t, ErrorType> BattleTile::getFirstBlockerIconID() const noexcept {
  if (this->hasBlocker()) {
    return this->blockers.front()->iconID;
  }
  return std::unexpected<ErrorType>{ErrorType::noneFound};
}

std::expected<const std::uint64_t, ErrorType> BattleTile::getFirstItemIconID() const noexcept {
  if (this->hasItem()) {
    return this->items.front()->iconID;
  }
  return std::unexpected<ErrorType>{ErrorType::noneFound};
}

bool BattleTile::blocksMovement() const noexcept {
  if (this->hasMob()) {
    return true;
  }
  if (this->hasBlocker()) {
    return this->blockers.front()->blocksMovement();
  }
  return false;
}

std::expected<std::shared_ptr<Item>, ErrorType> BattleTile::takeFirstItem() noexcept {
  if (this->hasItem()) {
    std::shared_ptr<Item> item = this->items.front();
    this->remove(this->items.front());
    return item;
  }
  return std::unexpected{ErrorType::noneFound};
}

std::expected<std::shared_ptr<const Item>, ErrorType> BattleTile::getFirstItem() const noexcept {
  if (this->hasItem()) {
    return this->items.front();
  }
  return std::unexpected{ErrorType::noneFound};
}

std::expected<const std::shared_ptr<Blocker>, ErrorType> BattleTile::getFirstBlocker() const noexcept {
  if (this->hasBlocker()) {
    return this->blockers.front();
  }
  return std::unexpected{ErrorType::noneFound};
}

void BattleTile::setField(BattleField* newField) noexcept {
  this->field = newField;
}

// TODO, O(n). Reconsider steps of map generation. Leave filling of ObjectView until after generation?
void BattleTile::clearBlockers() noexcept {
  std::for_each(this->blockers.begin(), this->blockers.end(),
                [this](const std::shared_ptr<Blocker>& blocker) { this->field->objectView.remove(blocker); });
  this->blockers.clear();
}

void BattleTile::initializeObjects() noexcept {
  std::for_each(this->blockers.begin(), this->blockers.end(),
                [this](std::shared_ptr<Blocker>& blocker) { blocker->location = this; });
  std::for_each(this->items.begin(), this->items.end(), [this](std::shared_ptr<Item>& item) { item->location = this; });
  std::for_each(this->mobs.begin(), this->mobs.end(), [this](std::shared_ptr<Mob>& mob) { mob->location = this; });
}
