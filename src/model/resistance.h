#ifndef GAME_RESISTANCE_H
#define GAME_RESISTANCE_H

#include <string>
#include <array>
#include <list>

class Mob;

class Resistance {
public:
  virtual ~Resistance() = default;
  explicit Resistance(const int val) : abs(val){}
  explicit Resistance(const float val) : base(val){}

  void calculateRelative() noexcept;
  [[nodiscard]] int getAbsolute() const noexcept;
  [[nodiscard]] float getRelative() const noexcept;

  [[nodiscard]] virtual std::string toString() const noexcept = 0;

  Resistance& operator+=(const Resistance& res);

private:
  int abs{};
  float base{};
  float rel{};

  static constexpr unsigned int diminishingFactor = 50;
};

class Phy final : public Resistance {
public:
  explicit Phy(const int val) : Resistance(val){}
  explicit Phy(const float val) : Resistance(val){}
  [[nodiscard]] std::string toString() const noexcept override;
};

class Sta final : public Resistance {
public:
  explicit Sta(const int val) : Resistance(val){}
  explicit Sta(const float val) : Resistance(val){}
  [[nodiscard]] std::string toString() const noexcept override;
};

class Fir final : public Resistance {
public:
  explicit Fir(const int val) : Resistance(val){}
  explicit Fir(const float val) : Resistance(val){}
  [[nodiscard]] std::string toString() const noexcept override;
};

class Wat final : public Resistance {
public:
  explicit Wat(const int val) : Resistance(val){}
  explicit Wat(const float val) : Resistance(val){}
  [[nodiscard]] std::string toString() const noexcept override;
};

class Ert final : public Resistance {
public:
  explicit Ert(const int val) : Resistance(val){}
  explicit Ert(const float val) : Resistance(val){}
  [[nodiscard]] std::string toString() const noexcept override;
};

class Wnd final : public Resistance {
public:
  explicit Wnd(const int val) : Resistance(val){}
  explicit Wnd(const float val) : Resistance(val){}
  [[nodiscard]] std::string toString() const noexcept override;
};

class Lif final : public Resistance {
public:
  explicit Lif(const int val) : Resistance(val){}
  explicit Lif(const float val) : Resistance(val){}
  [[nodiscard]] std::string toString() const noexcept override;
};

class Dth final : public Resistance {
public:
  explicit Dth(const int val) : Resistance(val){}
  explicit Dth(const float val) : Resistance(val){}
  [[nodiscard]] std::string toString() const noexcept override;
};

struct Resistances {
  enum class AsEnum {
    phy = 0,
    sta,
    fir,
    wat,
    ert,
    wnd,
    lif,
    dth
  };

  Phy phy{0};
  Sta sta{0};
  Fir fir{0};
  Wat wat{0};
  Ert ert{0};
  Wnd wnd{0};
  Lif lif{0};
  Dth dth{0};

  void add(const Resistances& other) noexcept;
  void add(const Phy& other) noexcept;
  void add(const Sta& other) noexcept;
  void add(const Fir& other) noexcept;
  void add(const Wat& other) noexcept;
  void add(const Ert& other) noexcept;
  void add(const Wnd& other) noexcept;
  void add(const Lif& other) noexcept;
  void add(const Dth& other) noexcept;

  void updateRelativeValues() noexcept;
  [[nodiscard]] std::array<float, 8> toArray() const;
};

#endif
