#include "blocker.h"

bool Blocker::blocksVision() const noexcept {
  return !this->vision;
}

bool Blocker::blocksMovement() const noexcept {
  return !this->movement;
}

bool Blocker::blocksFlight() const noexcept {
  return !this->flight;
}
