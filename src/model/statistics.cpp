#include "statistics.h"

void Attribute::addBonus(std::int32_t add) noexcept {
  this->bonus += add;
}

void Attribute::addBase(std::int32_t add) noexcept {
  this->base += add;
}

std::int32_t Attribute::getTotal() const noexcept {
  return base + bonus;
}
