#ifndef GAME_RACE_H
#define GAME_RACE_H

#include <string>

class Race {
public:
  enum class Type {
    unknown,
    undead,
  };

  Race() = default;
  explicit Race(Race::Type type);

  const std::string toString() const noexcept;

  Type type{};

private:
};

#endif
