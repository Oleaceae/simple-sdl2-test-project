#ifndef BLOCKER_H
#define BLOCKER_H

#include <cstddef>

class BattleTile;

class Blocker {
public:
  std::size_t iconID{4};

  [[nodiscard]] bool blocksVision() const noexcept;
  [[nodiscard]] bool blocksMovement() const noexcept;
  [[nodiscard]] bool blocksFlight() const noexcept;

  bool operator==(const Blocker& other) const {
    return this->iconID == other.iconID;
  }

  BattleTile* location{nullptr};

private:
  bool vision{false};
  bool movement{false};
  bool flight{false};
};

#endif
