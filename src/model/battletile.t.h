#ifndef GAME_BATTLETILE_T_H
#define GAME_BATTLETILE_T_H

#include "battlefield.h"
#include "battletile.h"

template <ItemType T> void BattleTile::add(const T& item) noexcept {
  auto sharedItem = std::make_shared<T>(item);
  this->add(sharedItem);
}

#endif
