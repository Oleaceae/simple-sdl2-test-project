#include "battlefield.h"
#include "../combat.h"
#include "battletile.t.h"
#include "objectview.h"

BattleField::BattleField(Point2D fieldSize, Combat* combat) {
  this->height = fieldSize.y;
  this->width = fieldSize.x;
  this->tiles = Matrix2d<BattleTile>(height, width);
  this->initializeBattleTiles();
  this->combat = combat;
}

void BattleField::initializeBattleTiles() {
  Point2D size = this->getSize();
  // Set x and y coordinates of each tile
  for (int i = 0; i < size.x; i++) {
    for (int j = 0; j < size.y; j++) {
      BattleTile& tile = this->tiles(j, i);
      tile.initializeObjects();
      tile.location = {i, j};
      tile.setField(this);
    }
  }
}

Matrix2d<BattleTile>* BattleField::getTiles() {
  return &this->tiles;
}

const Matrix2d<BattleTile>* BattleField::getTiles() const {
  return &this->tiles;
}

BattleTile& BattleField::operator()(Point2D point) {
  return this->tiles(point.y, point.x);
}

std::tuple<bool, std::shared_ptr<Mob>> BattleField::moveMob(const std::shared_ptr<Mob>& mob, Point2D direction) {
  BattleTile* source = mob->location;
  Point2D destinationPoint = source->location + direction;

  if (!destinationPoint.withinPositiveBounds(this->width, this->height)) {
    return {false, mob};
  }

  BattleTile& destination = this->operator()(destinationPoint);

  auto enemy = destination.getFirstMob();
  if (enemy.has_value()) {
    if (mob->faction.isFriend(enemy.value()->faction)) {
      return {false, mob};
    }
    if (mob->attack(*enemy.value())) {
      enemy.value()->onDeath();
      enemy.value()->dropLoot(*combat->itemAtlas, *combat->dropTableAtlas);

      if (enemy.value() == combat->getControlledMob()) { // We just died
        combat->setControlledMob(nullptr);
        objectView.setPlayerMob(nullptr);
      }

      destination.remove(enemy.value());
    }
    return {true, mob};
  }

  if (destination.blocksMovement()) {
    return {false, mob};
  }

  mob->location = &destination;
  destination.add(mob);
  std::uint32_t mobID = mob->id;
  source->remove(mob);

  return {true, destination.getMobByID(mobID).value()};
}

bool BattleField::movePlayerMob(std::shared_ptr<Mob> mob, Point2D direction, Combat& controllingCombat) {
  std::tuple<bool, std::shared_ptr<Mob>> moveResult = this->moveMob(mob, direction);
  bool moved = std::get<0>(moveResult);
  if (moved) {
    controllingCombat.setControlledMob(std::get<1>(moveResult));
  }
  return moved;
}

std::size_t BattleField::getWidth() const {
  return this->width;
}

std::size_t BattleField::getHeight() const {
  return this->height;
}

BattleTile& BattleField::getTile(const Point2D& location) {
  return this->tiles(location.y, location.x);
}

const BattleTile& BattleField::getTile(const Point2D& location) const {
  return this->tiles(location.y, location.x);
}

bool BattleField::placeAt(Mob mob, const Point2D& location) noexcept {
  if (!location.withinPositiveBounds(this->width, this->height)) {
    return false;
  }
  BattleTile& tile = this->getTile(location);
  mob.id = this->idCounter++;

  tile.add(mob);

  return true;
}

bool BattleField::placeAt(Blocker blocker, const Point2D& location) noexcept {
  if (!location.withinPositiveBounds(this->width, this->height)) {
    return false;
  }
  BattleTile& tile = this->getTile(location);
  tile.add(blocker);

  return true;
}

bool BattleField::placeAt(Item item, const Point2D& location) noexcept {
  if (!location.withinPositiveBounds(this->width, this->height)) {
    return false;
  }
  BattleTile& tile = this->getTile(location);
  tile.add(item);

  return true;
}

Point2D BattleField::getSize() const {
  return Point2D(this->width, this->height);
}

void BattleField::setCombat(Combat* newCombat) noexcept {
  this->combat = newCombat;
}
BattleField::BattleField(const BattleField& o)
    : tiles(o.tiles), width(o.width), height(o.height), idCounter(o.idCounter), objectView(o.objectView),
      combat(nullptr) {
  this->initializeBattleTiles();
}

BattleField& BattleField::operator=(const BattleField& other) {
  this->tiles = other.tiles;
  this->width = other.width;
  this->height = other.height;
  this->idCounter = other.idCounter;
  this->objectView = other.objectView;
  this->combat = nullptr;
  this->initializeBattleTiles();

  return *this;
}
