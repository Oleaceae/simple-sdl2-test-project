#ifndef MOB_H
#define MOB_H

#include <cstdint>
#include <map>

#include "../ai/ai.h"
#include "droptable.h"
#include "equipment/equipment.h"
#include "equipment/inventory.h"
#include "faction.h"
#include "race.h"
#include "statistics.h"
#include "resistance.h"

class BattleTile;

struct Damage {
  std::uint32_t min{1};
  std::uint32_t max{3};
  float multiplier{1.};
  float armorPen{0.};
};

struct CurMax {
  std::int32_t current{10};
  std::int32_t max{10};
  void increaseMax(int value) noexcept;
  void refill(int value) noexcept;
};

typedef CurMax Health;
typedef CurMax Mana;

class Mob {
public:
  Mob();
  explicit Mob(std::uint64_t iconID, Faction faction = Faction(), Race race = Race(),
               bool pcControlled = false);

  bool operator==(const Mob& other) const;

  // attack returns true if the enemy has died, TODO consider using some decoupling technique like event queue
  bool attack(Mob& enemy) const noexcept;
  [[nodiscard]] bool isDead() const noexcept;
  void pickupItem() noexcept;
  void onDeath() noexcept;
  void dropLoot(const DropTable::ItemAtlas& itemAtlas, const DropTable::DropTableAtlas& dropTableAtlas) noexcept;
  std::uint16_t tryTurn(std::int32_t speedThreshold) noexcept;

  [[nodiscard]] const Statistics& getStats() const noexcept;
  [[nodiscard]] const Health& getHealth() const noexcept;
  [[nodiscard]] const Mana& getMana() const noexcept;
  [[nodiscard]] const Equipment& getEquipment() const noexcept;
  [[nodiscard]] const Resistances& getResistances() const noexcept;
  [[nodiscard]] const Damage& getAttackDamage() const noexcept;

  void equipItem(const std::shared_ptr<Item>&, Equipment::EquipmentSlot slot) noexcept;
  void unequipItem(Equipment::EquipmentSlot slot) noexcept;

  void addModifier(const Modifier& mod) noexcept;
  void removeModifier(const Modifier& mod) noexcept;

  std::uint32_t id{};
  BattleTile* location{nullptr};
  std::uint64_t iconID{1};

  Race race{Race::Type::unknown};
  Faction faction{Faction::Type::wildlife};
  std::string name{"???"};

  std::map<std::string, std::shared_ptr<AI>> availableAIs{
      {"Idle", std::make_shared<AI>(AIFactory::idleAI("Combat"))},
      {"Combat", std::make_shared<AI>(AIFactory::simpleAI("Idle"))}};
  std::shared_ptr<AI> activeAI{};

  [[nodiscard]] bool isPlayerControlled() const noexcept;

  void setPlayerControlled(bool flag) noexcept;

  void setHp(std::int32_t) noexcept;
  void setMaxHp(std::int32_t) noexcept;
  void setSpeed(std::int32_t) noexcept;

private:
  void addLootToTile(const std::shared_ptr<Item>& loot) const noexcept;

  Damage attackDamage{};
  Health health{};
  Mana mana{};

  Statistics statistics{};
  Resistances resistances{};
  Equipment equipment{};
  Inventory inventory{};

  DropTable::dropTableID dropTableID{};

  bool playerControlled{false};
};

#endif
