#ifndef GAME_VIEW_H
#define GAME_VIEW_H

#include <memory>

template <class T> class View {
public:
  explicit View(std::shared_ptr<T> object) noexcept;
  [[nodiscard]] const std::shared_ptr<T>& getObject() const noexcept;

private:
  std::shared_ptr<T> object{};
};

template <class T> View<T>::View(std::shared_ptr<T> object) noexcept : object(object) {
}

template <class T> const std::shared_ptr<T>& View<T>::getObject() const noexcept {
  return this->object;
}

#endif
