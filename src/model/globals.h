#ifndef GLOBALS_H
#define GLOBALS_H

// This file contains global configurations

#include <string>

namespace globals {
  const std::string default_font{"assets/fonts/acer_vga_8x8.ttf"};
}

#endif
