#include "race.h"

Race::Race(Type type) : type(type) {
}

const std::string Race::toString() const noexcept {
  switch (this->type) {
  case Type::unknown:
    return "Unknown";
  case Type::undead:
    return "Undead";
  }
  return "Unknown race";
}
