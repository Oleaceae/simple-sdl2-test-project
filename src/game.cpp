#include "game.h"

#include "sound/playsound.h"

using namespace sdlwrapp;

Game::Game()
    : dropTableAtlas(Game::loadDropTableAtlas()),
      itemAtlas(Game::loadItemAtlas()), window(SDLWindow::createWindow(this->windowTitle, this->windowPosX, this->windowPosY, this->windowSize.x,
                                     this->windowSize.y, this->windowFlags)), renderer(this->window),
      combat(Combat(&this->window, &this->renderer, this->windowSize, {64, 64},
                                            &this->itemAtlas, &this->dropTableAtlas)) {
  this->window.setWindowIcon(Sprite::createSpriteSurface("assets/icons/inventory.png", 32, 0, 4));
  this->sdlmixer.setChannelFinishedCallback(sound::channelDone);
  this->sdlmixer.setChannelCount(sound::channelCount);

  this->combat.getRenderer()->loadUI(windowSize);

  if (SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "best") == false) {
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
  }
}

void Game::start() {
  while (this->mode != GameMode::quit) {
    switch (this->mode) {
    case GameMode::combat:
      this->mode = this->combat.start();
      break;
    case GameMode::quit:
      break;
    }
  }
}

DropTable::DropTableAtlas Game::loadDropTableAtlas() {
  // TODO add loading from file
  auto atlas = DropTable::DropTableAtlas{};
  atlas.insert({0, DropTable({{10000, 0}})});
  return atlas;
}

DropTable::ItemAtlas Game::loadItemAtlas() {
  // TODO add loading from file
  auto atlas = DropTable::ItemAtlas{};
  auto arm = std::make_shared<Armor>();
  arm->description = "A dropped piece of armor";
  atlas.insert({0, arm});
  return atlas;
}

void Game::loadCombat(Combat& newCombat) noexcept {
  this->combat = std::move(newCombat);
}

Combat* Game::getCombat() noexcept {
  return &this->combat;
}
