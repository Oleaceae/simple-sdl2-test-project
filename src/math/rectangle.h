#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "point2d.h"

struct Rectangle {
  [[nodiscard]] bool contains(Point2D point) const {
    return point.x > origin.x && point.x < span.x && point.y > origin.y && point.y < span.y;
  }

  Point2D origin{};
  Point2D span{};
};

#endif