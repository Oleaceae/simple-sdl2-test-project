#ifndef POINT2D_H
#define POINT2D_H

#include <exception>

#include "../sdl2wrapp/sdl.h"

// Not a "proper" vector. Contains game logic, like bounds checking and staying
// within coordinate system.
struct Point2D {
  [[nodiscard]] bool withinPositiveBounds(const int width, const int height) const noexcept {
    return !((x >= width || x < 0) || (y >= height || y < 0));
  }

  [[nodiscard]] bool withinPositiveBounds(const Point2D& size) const noexcept {
    return withinPositiveBounds(size.x, size.y);
  }

  [[nodiscard]] Point2D moveNegCoordsToZero() const noexcept {
    return Point2D(this->x < 0 ? 0 : this->x, this->y < 0 ? 0 : this->y);
  }

  [[nodiscard]] bool inside(const sdlwrapp::SDLRect rect) const noexcept {
    const auto area = &rect.rect;
    return ((area->x <= this->x && this->x <= (area->x + area->w)) &&
            (area->y <= this->y && this->y <= (area->y + area->h)));
  }

  [[nodiscard]] unsigned int distance(const Point2D& target) const noexcept {
    return abs(target.x - this->x) + abs(target.y - this->y);
  }

  void clamp(const Point2D origin, const Point2D direction) noexcept {
    if (this->x < origin.x) {
      this->x = 0;
    } else if (this->x > direction.x) {
      this->x = direction.x;
    }

    if (this->y < origin.y) {
      this->y = 0;
    } else if (this->y > direction.y) {
      this->y = direction.y;
    }
  }

  [[nodiscard]] bool inRange(const Point2D target, const std::int32_t range) const noexcept {
    return target.inside({x - range, y - range, range * 2, range * 2});
  }

  Point2D operator+(const Point2D& addend) const noexcept {
    Point2D point{this->x + addend.x, this->y + addend.y};
    return point;
  }

  Point2D operator-(const Point2D& subtrahend) const noexcept {
    return this->operator+({-subtrahend.x, -subtrahend.y});
  }

  // TODO this is just to translate a tile to a coordinate on screen for
  // drawing. Reconsider implementation
  Point2D operator*(const int multiplier) const noexcept {
    return {this->x * multiplier, this->y * multiplier};
  }

  Point2D operator/(const int divisor) const {
    if (divisor != 0) {
      return {this->x / divisor, this->y / divisor};
    } else {
      throw std::exception();
    }
  }

  bool operator==(const Point2D other) const noexcept {
    return ((this->x == other.x) && (this->y == other.y));
  }

  bool operator<(const Point2D other) const noexcept {
    return ((this->x < other.x) && (this->y < other.y));
  }

   bool operator>(const Point2D other) const noexcept {
    return ((this->x > other.x) && (this->y > other.y));
  }

  int x{};
  int y{};
};

#endif
