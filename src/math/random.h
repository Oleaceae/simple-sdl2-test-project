#ifndef GAME_RANDOM_H
#define GAME_RANDOM_H

#include <chrono>
#include <random>

class Random {
public:
  static Random& instance() {
    static Random r;
    return r;
  }
  // [min, max]
  std::uint32_t gen_uniform_distributed(std::uint32_t min, std::uint32_t max) {
    return std::uniform_int_distribution<std::uint32_t>(min, max).operator()(this->engine);
  }

  float gen_normal_distributed(float mean, float deviation = 1.0) {
    auto dist = std::normal_distribution<float>(mean, deviation);
    return std::normal_distribution<float>(mean, deviation).operator()(this->engine);
  }

#ifdef TESTING
  void setEngine(std::mt19937 newEngine) {
    this->engine = newEngine;
  }
#endif

private:
  Random(){};
  ~Random(){};

  std::mt19937 engine{std::random_device()() + static_cast<std::uint32_t>(std::time(nullptr))};
};

#endif
