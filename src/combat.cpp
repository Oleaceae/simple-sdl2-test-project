#include "combat.h"

using namespace sdlwrapp;

CombatEventHandler::CombatEventHandler(Combat* combat) noexcept
    : combat(combat), keyAtlas(loadDefaultKeyAtlas()), mouseWheelAtlas(loadDefaultMousewheelAtlas()) {
}

void CombatEventHandler::processEvents() {
  while (SDL_PollEvent(&this->event)) {
    if (this->event.type == SDL_QUIT) {
      this->combat->quit = true;
      return;
    }

    if (this->event.type == SDL_KEYDOWN) {
      this->combat->gameTime = GameTime::paused;
      this->handleKeyDownEvents();
      continue;
    }

    if (this->event.type == SDL_MOUSEWHEEL) {
      this->handleMouseWheelEvents();
      continue;
    }

    if (this->event.type == SDL_MOUSEBUTTONDOWN) {
      if (this->event.button.button == SDL_BUTTON_LEFT) {
        this->handleMouseClickEvents();
        continue;
      }
    }

    if (this->event.type == SDL_WINDOWEVENT && this->event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
      this->handleWindowResized();
      continue;
    }
  }
}

void CombatEventHandler::handleKeyDownEvents() {
  if (this->combat->getRenderer()->getUI()->hasOpenMenu()) {
    if (this->combat->getRenderer()->getUI()->getOpenMenu()->handleKey(this->event.key.keysym.sym)) {
      return;
    }
  }

  const auto key = this->keyAtlas.find(this->event.key.keysym.sym);
  if (key == this->keyAtlas.end()) {
    return;
  }
  this->handleInput(key->second);
}

void CombatEventHandler::handleMouseWheelEvents() {
  if (this->combat->getRenderer()->getUI()->hasOpenMenu()) {
    int x, y;
    SDL_GetMouseState(&x, &y);
    if (this->combat->getRenderer()->getUI()->getOpenMenu()->handleMouseWheel({x, y}, this->event.wheel.y)) {
      return;
    }
  }

  if (this->event.wheel.y > 0) {
    auto key = this->mouseWheelAtlas.find(MouseWheel::up);
    if (key == this->mouseWheelAtlas.end()) {
      return;
    }
    return this->handleInput(key->second);
  }
  if (this->event.wheel.y < 0) {
    auto key = this->mouseWheelAtlas.find(MouseWheel::down);
    if (key == this->mouseWheelAtlas.end()) {
      return;
    }
    return this->handleInput(key->second);
  }
}

void CombatEventHandler::handleInput(const KeyPressConfig key) {
  switch (key) {
  case KeyPressConfig::escape:
    // TODO unimplemented
    break;
  case KeyPressConfig::up:
    this->combat->moveControlledMob({0, -1});
    break;
  case KeyPressConfig::down:
    this->combat->moveControlledMob({0, 1});
    break;
  case KeyPressConfig::left:
    this->combat->moveControlledMob({-1, 0});
    break;
  case KeyPressConfig::right:
    this->combat->moveControlledMob({1, 0});
    break;
  case KeyPressConfig::downLeft:
    this->combat->moveControlledMob({-1, 1});
    break;
  case KeyPressConfig::downRight:
    this->combat->moveControlledMob({1, 1});
    break;
  case KeyPressConfig::upLeft:
    this->combat->moveControlledMob({-1, -1});
    break;
  case KeyPressConfig::upRight:
    this->combat->moveControlledMob({1, -1});
    break;
  case KeyPressConfig::zoomIn:
    this->combat->getRenderer()->zoomIn();
    break;
  case KeyPressConfig::zoomOut:
    this->combat->getRenderer()->zoomOut();
    break;
  case KeyPressConfig::pickupItem:
    this->combat->getControlledMob()->pickupItem();
    break;
  case KeyPressConfig::skipTurn:
    this->combat->doTurn();
    break;
  case KeyPressConfig::character:
    this->combat->getRenderer()->getUI()->openMenu(TextUI::Menu::character, this->combat);
    break;
  case KeyPressConfig::equipment:
    this->combat->getRenderer()->getUI()->openMenu(TextUI::Menu::equipment, this->combat);
    break;
  case KeyPressConfig::inventory:
    this->combat->getRenderer()->getUI()->openMenu(TextUI::Menu::inventory, this->combat);
    break;
  case KeyPressConfig::team:
    this->combat->getRenderer()->getUI()->openMenu(TextUI::Menu::team, this->combat);
    break;
  case KeyPressConfig::skills:
    this->combat->getRenderer()->getUI()->openMenu(TextUI::Menu::skills, this->combat);
    break;
  case KeyPressConfig::logbook:
    this->combat->getRenderer()->getUI()->openMenu(TextUI::Menu::logbook, this->combat);
    break;
  case KeyPressConfig::fullscreenSwitch:
    this->combat->getRenderer()->getWindow()->switchWindowMode();
    break;
  }
}

void CombatEventHandler::handleMouseClickEvents() {
  if (this->event.button.button == SDL_BUTTON_LEFT) {
    Point2D click = {this->event.button.x, this->event.button.y};

    if (this->combat->getRenderer()->getUI()->hasOpenMenu()) {
      if (this->combat->getRenderer()->getUI()->getOpenMenu()->handleLeftClick(click)) {
        return;
      }
    }

    if (this->combat->getRenderer()->getUI()->handleLeftClick(click)) {
      return;
    }

    if (this->combat->handleLeftClick(click)) {
      return;
    }
  }

  // Handle interactive game field objects
}

void CombatEventHandler::handleWindowResized() {
  const auto renderer = this->combat->getRenderer();

  const std::shared_ptr<Widget> openedMenu =
      renderer->getUI()->hasOpenMenu() ? renderer->getUI()->getOpenMenu() : nullptr;
  const TextUI::Menu openedMenuType = renderer->getUI()->getOpenMenuType();

  this->combat->setRenderer(CombatRenderer(renderer->getWindow(), renderer->getRenderer(),
                                           {renderer->getWindow()->getWidth(), renderer->getWindow()->getHeight()},
                                           this->combat));

  const auto rendererNew = this->combat->getRenderer();
  this->combat->getRenderer()->loadUI({rendererNew->getWindow()->getWidth(), rendererNew->getWindow()->getHeight()});

  if (openedMenu != nullptr) {
    this->combat->getRenderer()->getUI()->reopenMenu(openedMenu, openedMenuType);
  }
}

std::unordered_map<SDL_Keycode, KeyPressConfig> CombatEventHandler::loadDefaultKeyAtlas() noexcept {
  return {{SDLK_ESCAPE, KeyPressConfig::escape},   {SDLK_UP, KeyPressConfig::up},
          {SDLK_DOWN, KeyPressConfig::down},       {SDLK_LEFT, KeyPressConfig::left},
          {SDLK_RIGHT, KeyPressConfig::right},     {SDLK_KP_1, KeyPressConfig::downLeft},
          {SDLK_KP_2, KeyPressConfig::down},       {SDLK_KP_3, KeyPressConfig::downRight},
          {SDLK_KP_4, KeyPressConfig::left},       {SDLK_KP_6, KeyPressConfig::right},
          {SDLK_KP_7, KeyPressConfig::upLeft},     {SDLK_KP_8, KeyPressConfig::up},
          {SDLK_KP_9, KeyPressConfig::upRight},    {SDLK_PERIOD, KeyPressConfig::pickupItem},
          {SDLK_RETURN, KeyPressConfig::skipTurn}, {SDLK_c, KeyPressConfig::character},
          {SDLK_e, KeyPressConfig::equipment},     {SDLK_i, KeyPressConfig::inventory},
          {SDLK_t, KeyPressConfig::team},          {SDLK_m, KeyPressConfig::skills},
          {SDLK_l, KeyPressConfig::logbook},       {SDLK_F12, KeyPressConfig::fullscreenSwitch}};
}

std::unordered_map<MouseWheel, KeyPressConfig> CombatEventHandler::loadDefaultMousewheelAtlas() noexcept {
  return {{MouseWheel::up, KeyPressConfig::zoomIn}, {MouseWheel::down, KeyPressConfig::zoomOut}};
}

std::expected<KeyPressConfig, ErrorType> CombatEventHandler::getMappedKey(const SDL_Keycode key) const noexcept {
  if (this->keyAtlas.contains(key)) {
    return this->keyAtlas.at(key);
  }
  return std::unexpected(ErrorType::noneFound);
}

GameMode Combat::start() {
  this->eventHandler = CombatEventHandler(this);

  while (!this->quit) {

    this->ticksNow = SDL_GetTicks();

    this->eventHandler.processEvents();

    if (this->gameTime == GameTime::realtime) {
      // Update everything
    }

    this->renderer.renderScene(*this);

    this->delta = SDL_GetTicks() - this->ticksNow;

    if (delta < desiredDelta) {
      SDL_Delay(desiredDelta - delta);
    }

    this->delta = SDL_GetTicks() - this->ticksNow;
    if (this->delta != 0) {
      this->renderer.setWindowTitle("FPS: " + std::to_string(1000 / this->delta));
    }
  }
  return GameMode::quit;
}

BattleField& Combat::getField() noexcept {
  return this->battleField;
}

const BattleField& Combat::getField() const noexcept {
  return this->battleField;
}

void Combat::loadField(const BattleField& field, const Point2D& setPCLocation) noexcept {
  this->battleField = field;
  this->battleField.setCombat(this);

  this->renderer =
      CombatRenderer(this->renderer.getWindow(), this->renderer.getRenderer(),
                     {this->renderer.getWindow()->getWidth(), this->renderer.getWindow()->getHeight()}, this);

  this->renderer.loadUI({this->renderer.getWindow()->getWidth(), this->renderer.getWindow()->getHeight()});

  auto mob = battleField(setPCLocation).getFirstMob();
  if (mob.has_value()) {
    this->setControlledMob(mob.value());
    this->battleField.objectView.setPlayerMob(mob.value());
  } else {
    MessageQueue::instance().add("Could not find mob to set as player.");
  }
}

Combat::Combat(SDLWindow* window, SDLRenderer* renderer, Point2D windowSize, Point2D fieldSize,
               const DropTable::ItemAtlas* itemAtlas, const DropTable::DropTableAtlas* dropTableAtlas) noexcept
    : itemAtlas(itemAtlas), dropTableAtlas(dropTableAtlas), battleField(BattleField(fieldSize, this)),
      eventHandler(this), renderer(window, renderer, windowSize, this) {
}

Combat& Combat::operator=(Combat&& other) noexcept {
  if (this != &other) {
    this->battleField = other.battleField;
    this->battleField.setCombat(this);
    this->eventHandler = other.eventHandler;
    this->renderer = std::move(other.renderer);
    this->renderer.setCombat(this);
    this->quit = other.quit;
    this->itemAtlas = other.itemAtlas;
    this->dropTableAtlas = other.dropTableAtlas;
  }
  return *this;
}

bool Combat::handleLeftClick(const Point2D& click) {
  // No interaction with the combat field yet
  return false;
}

void Combat::setControlledMob(const std::shared_ptr<Mob>& mob) {
  // TODO maybe wrap controlledMob into an expected type
  this->controlledMob = mob;
  if (this->controlledMob != nullptr) {
    this->controlledMob->setPlayerControlled(true);
  }
  this->battleField.objectView.setPlayerMob(mob);
}

std::expected<const Point2D, ErrorType> Combat::getControlledMobLocation() const {
  if (this->controlledMob != nullptr) {
    return this->controlledMob->location->location;
  } else {
    return std::unexpected<ErrorType>{ErrorType::noneFound};
  }
}

std::shared_ptr<Mob>& Combat::getControlledMob() {
  return this->controlledMob;
}

void Combat::moveControlledMob(Point2D direction) {
  if (this->controlledMob == nullptr) {
    return;
  }
  if (this->battleField.movePlayerMob(this->controlledMob, direction, *this)) {
    this->doTurn();
  }
}

CombatRenderer* Combat::getRenderer() noexcept {
  return &this->renderer;
}

void Combat::setRenderer(CombatRenderer&& renderer) {
  this->renderer = std::move(renderer);
}

ObjectView* Combat::getObjectView() {
  return &this->getField().objectView;
}

const CombatEventHandler& Combat::getEventHandler() const noexcept {
  return this->eventHandler;
}

void Combat::doTurn() {
  int32_t speed;
  if (this->getControlledMob() != nullptr) {
    speed = this->getControlledMob()->getStats().speed.getTotal();
  } else {
    speed = 100;
  }

  const std::list<View<const Mob>> mobsCopy = battleField.objectView.getMobs();
  for (auto& ex : mobsCopy) {
    auto mob = getField()(ex.getObject()->location->location).getFirstMob();
    if (mob.has_value()) {
      if (mob.value()->isPlayerControlled()) {
        continue; // TODO give the player mob an empty AI? Think of something.
      }
      for (int i = mob.value()->tryTurn(speed); i > 0; i--) {
        ex.getObject()->activeAI->process(getField(), mob.value());
      }
    } else {
      continue;
    }
  }
}
