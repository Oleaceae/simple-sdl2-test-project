#ifndef GAME_PLAYSOUND_H
#define GAME_PLAYSOUND_H

#include <string>

namespace sound {

void playsound(const std::string& filePath);

void channelDone(int channel);

constexpr int channelCount = 8;

} // namespace sound

#endif
