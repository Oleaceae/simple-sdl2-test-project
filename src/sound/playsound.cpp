#include "playsound.h"

#include <array>
#include <optional>

#include "../sdl2wrapp/sdlmixer.h"

using namespace sdlwrapp;

namespace sound {

std::array<std::optional<MixChunk>, channelCount> soundCache = {};

void playsound(const std::string& filePath) {
  auto chunk = MixChunk(filePath);
  if (chunk.isNull()) {
    return;
  }
  if (const auto channel = chunk.play(); channel >= 0) {
    soundCache[channel] = std::move(chunk);
  }
}

void channelDone(const int channel) {
  if (soundCache.at(channel).has_value()) {
    soundCache[channel] = std::nullopt;
  }
}

} // namespace sound
