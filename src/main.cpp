#include "game.h"

// Unit testing
#ifdef TESTING
#define CATCH_CONFIG_RUNNER
#include "../test/extern/catch.hpp"
#endif

using namespace sdlwrapp;

int main(const int argc, char* argv[]) {
#ifdef TESTING
  Catch::Session().run(argc, argv);
#else
  try {
    Game game = Game();
    game.start();
  } catch (SDLException& ex) {
    SDL_Log("SDL Exception occured: %s", ex.what());
  } catch (std::exception& ex) {
    SDL_Log("Exception occured: %s", ex.what());
  }
  return 0;
#endif
}
