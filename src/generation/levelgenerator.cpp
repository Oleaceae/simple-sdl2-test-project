#include "levelgenerator.h"

using namespace sdlwrapp;

BattleField LevelGenerator::drunkardRandom(const BattleField& base, std::uint32_t lifetime, std::uint32_t iterations,
                                           std::uint16_t borderSize) {
  auto field = BattleField(base);
  LevelGenerator::fillField(field);
  Point2D fieldSize = field.getSize();

  for (int i = 0; i < iterations; i++) {
    Point2D spawn = {
        static_cast<int>(Random::instance().gen_uniform_distributed(0 + borderSize, fieldSize.x - 1 - borderSize)),
        static_cast<int>(Random::instance().gen_uniform_distributed(0 + borderSize, fieldSize.y - 1 - borderSize))};

    Drunkard().walk(field, spawn, lifetime, borderSize);
  }
  field.objectView.reloadBlockers(field);
  return field;
}

BattleField LevelGenerator::drunkardTunneling(const BattleField& base, std::uint32_t lifetime, std::uint32_t iterations,
                                              std::uint16_t tunnelSize, std::uint8_t tunnelWidth,
                                              std::uint16_t iterationsUntilTunneling, std::uint16_t borderSize) {
  auto field = BattleField(base);
  LevelGenerator::fillField(field);
  Point2D fieldSize = field.getSize();

  Point2D spawn = {
      static_cast<int>(Random::instance().gen_uniform_distributed(0 + borderSize, fieldSize.x - 1 - borderSize)),
      static_cast<int>(Random::instance().gen_uniform_distributed(0 + borderSize, fieldSize.y - 1 - borderSize))};

  for (int i = 0; i < iterations; i++) {
    for (int j = 0; j < iterationsUntilTunneling; j++) {
      Drunkard().walk(field, spawn, lifetime, borderSize);
    }
    spawn = LevelGenerator::tunnel(field, spawn, tunnelSize, tunnelWidth, borderSize, fieldSize);
  }
  field.objectView.reloadBlockers(field);
  return field;
}

BattleField LevelGenerator::cellularAutomata(const BattleField& base, std::uint16_t initLifePercentage,
                                             std::uint32_t iterations, std::uint16_t borderSize,
                                             std::uint8_t birthThreshold, std::uint8_t survThreshold) {
  BattleField field = BattleField(base);
  Point2D fieldSize = field.getSize();
  SDLRect border = {borderSize, borderSize, fieldSize.x - borderSize * 2 - 1, fieldSize.y - borderSize * 2 - 1};

  if (initLifePercentage > 100) {
    initLifePercentage = 100;
  }

  auto tiles = field.getTiles();

  tiles->forEach([&](BattleTile& tile) {
    if (!tile.location.inside(border)) {
      tile.add(Blocker());
    }
    if (Random::instance().gen_uniform_distributed(0, 100) <= initLifePercentage) {
      tile.add(Blocker());
    }
  });

  for (int i = 0; i < iterations; i++) {
    field.getTiles()->forEach([&](BattleTile& tile) {
      if (!tile.location.inside(border)) {
        return;
      }
      std::uint8_t neighbors = getNeighboringBlockerCount(field, tile.location, border);
      if (!tile.hasBlocker() && neighbors >= birthThreshold) {
        tile.add(Blocker());
      } else if (tile.hasBlocker() && neighbors >= survThreshold) {
      } else {
        tile.clearBlockersWithoutObjectView();
      }
    });
  }
  field.objectView.reloadBlockers(field);
  return field;
}

void LevelGenerator::fillField(BattleField& field) noexcept {
  for (int i = 0; i < field.getWidth(); i++) {
    for (int j = 0; j < field.getHeight(); j++) {
      field({i, j}).add(Blocker());
    }
  }
}

Point2D LevelGenerator::tunnel(BattleField& field, Point2D start, const std::uint16_t tunnelSize,
                               const std::uint8_t tunnelWidth, const std::uint16_t borderSize,
                               const Point2D fieldSize) {
  Point2D direction = {static_cast<int>(Random::instance().gen_uniform_distributed(0, 2) - 1),
                       static_cast<int>(Random::instance().gen_uniform_distributed(0, 2) - 1)};
  SDLRect border = {borderSize, borderSize, fieldSize.x - borderSize * 2 - 1, fieldSize.y - borderSize * 2 - 1};

  for (int i = 0; i < tunnelSize; i++) {
    if ((start + direction).inside(border)) {
      auto& tile = field(start + direction);

      if (tunnelWidth > 1) {
        Point2D widenDirection;
        // TODO looks like this can just use the direction?
        if (direction.y > 0)
          widenDirection = {-1, 0};
        else if (direction.y < 0)
          widenDirection = {1, 0};
        else if (direction.x < 0)
          widenDirection = {0, -1};
        else if (direction.x > 1)
          widenDirection = {0, 1};
        LevelGenerator::widenTunnel(field, start + direction, widenDirection, tunnelWidth, border);
      }
      tile.clearBlockersWithoutObjectView();
    } else {
      break;
    }
    start = start + direction;
  }
  return start;
}

std::uint8_t LevelGenerator::getNeighboringBlockerCount(const BattleField& field, const Point2D& center,
                                                        const SDLRect& border) noexcept {
  std::uint8_t count = 0;
  for (int x = -1; x <= 1; x++) {
    for (int y = -1; y <= 1; y++) {
      if (!(center + Point2D(x, y)).inside(border)) {
        count++;
        continue;
      }
      if (field.getTile(center + Point2D(x, y)).hasBlocker()) {
        count++;
      }
    }
  }
  return count;
}

void LevelGenerator::widenTunnel(BattleField& field, const Point2D& start, const Point2D& direction,
                                 const std::uint8_t width, const SDLRect& border) {
  for (int j = 1; j < width; j++) {
    Point2D target = start + direction * j;
    if (!target.inside(border)) {
      break;
    }
    auto& widen = field(target);
    widen.clearBlockersWithoutObjectView();
  }
}
