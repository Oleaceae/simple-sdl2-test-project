#include "drunkard.h"

#include "../math/random.h"

using namespace sdlwrapp;

void Drunkard::walk(BattleField& field, Point2D spawn, std::uint32_t lifetime, std::int32_t borderSize) const {
  auto fieldSize = field.getSize();

  field(spawn).clearBlockersWithoutObjectView();

  SDLRect border = {borderSize, borderSize, fieldSize.x - borderSize * 2 - 1, fieldSize.y - borderSize * 2 - 1};

  for (int i = 0; i < lifetime; i++) {
    Point2D direction = {static_cast<int>(Random::instance().gen_uniform_distributed(0, 2) - 1),
                         static_cast<int>(Random::instance().gen_uniform_distributed(0, 2) - 1)};

    auto target = spawn + direction;
    if (target.inside(border)) {
      auto& tile = field(target);
      tile.clearBlockersWithoutObjectView();
      spawn = target;
    } else {
      continue;
    }
  }
}
