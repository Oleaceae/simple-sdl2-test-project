#ifndef GAME_LEVELGENERATOR_H
#define GAME_LEVELGENERATOR_H

#include "../model/battlefield.h"
#include "drunkard.h"

class LevelGenerator {
public:
  static BattleField drunkardRandom(const BattleField& base, std::uint32_t lifetime, std::uint32_t iterations,
                                    std::uint16_t borderSize);

  static BattleField drunkardTunneling(const BattleField& base, std::uint32_t lifetime, std::uint32_t iterations,
                                       std::uint16_t tunnelSize, std::uint8_t tunnelWidth,
                                       std::uint16_t iterationsUntilTunneling, std::uint16_t borderSize);

  static BattleField cellularAutomata(const BattleField& base, std::uint16_t initLifePercentage,
                                      std::uint32_t iterations, std::uint16_t borderSize, std::uint8_t birthThreshold,
                                      std::uint8_t survThreshold);

private:
  static void fillField(BattleField& field) noexcept;
  static Point2D tunnel(BattleField& field, Point2D start, std::uint16_t tunnelSize, std::uint8_t tunnelWidth,
                        std::uint16_t borderSize, Point2D fieldSize);
  static std::uint8_t getNeighboringBlockerCount(const BattleField&, const Point2D& center,
                                                 const sdlwrapp::SDLRect& border) noexcept;
  static void widenTunnel(BattleField& field, const Point2D& start, const Point2D& direction, const uint8_t width,
                          const sdlwrapp::SDLRect& border);
};

#endif
