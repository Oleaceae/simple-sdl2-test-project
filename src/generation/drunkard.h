#ifndef GAME_DRUNKARD_H
#define GAME_DRUNKARD_H

#include "../model/battlefield.h"

class Drunkard {
public:
  void walk(BattleField& field, Point2D spawn, std::uint32_t lifetime = 10, std::int32_t borderSize = 1) const;

private:
};

#endif
