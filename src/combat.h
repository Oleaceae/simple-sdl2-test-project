#ifndef COMBAT_H
#define COMBAT_H

#include <unordered_map>

#include "sdl2wrapp/sdl.h"

#include "model/battlefield.h"
#include "model/droptable.h"
#include "model/gamemode.h"

#include "events/eventqueue.h"

#include "render/combatrenderer.h"

#include "persistence/combatserializer.h"

class Combat;

enum class KeyPressConfig {
  escape,
  up,
  down,
  left,
  right,
  downLeft,
  downRight,
  upLeft,
  upRight,
  zoomOut,
  zoomIn,
  pickupItem,
  skipTurn,
  character,
  equipment,
  inventory,
  team,
  skills,
  logbook,
  fullscreenSwitch
};

enum class MouseWheel { up, down };

class CombatEventHandler {
private:
  SDL_Event event = SDL_Event();
  Combat* combat = nullptr;
  // Keyboard key press atlas maps SDL_Keycode to our keys
  std::unordered_map<SDL_Keycode, KeyPressConfig> keyAtlas = {};
  // Mouse wheel is different from keys for SDL
  std::unordered_map<MouseWheel, KeyPressConfig> mouseWheelAtlas = {};

public:
  CombatEventHandler() noexcept : keyAtlas(loadDefaultKeyAtlas()){};
  explicit CombatEventHandler(Combat* combat) noexcept;

  void processEvents();
  void handleKeyDownEvents();
  void handleMouseWheelEvents();
  void handleInput(KeyPressConfig key);

  static std::unordered_map<SDL_Keycode, KeyPressConfig> loadDefaultKeyAtlas() noexcept;
  static std::unordered_map<MouseWheel, KeyPressConfig> loadDefaultMousewheelAtlas() noexcept;

  std::expected<KeyPressConfig, ErrorType> getMappedKey(SDL_Keycode key) const noexcept;

  void handleMouseClickEvents();
  void handleWindowResized();
};

enum class GameTime { realtime, paused };

class Combat {
public:
  Combat() = delete;
  explicit Combat(sdlwrapp::SDLWindow* window, sdlwrapp::SDLRenderer* renderer, Point2D windowSize, Point2D fieldSize,
                  const DropTable::ItemAtlas* itemAtlas = {},
                  const DropTable::DropTableAtlas* dropTableAtlas = {}) noexcept;
  Combat(const Combat& combat) = delete;
  Combat(Combat&& combat) = default;

  Combat& operator=(Combat&& other) noexcept;

  GameMode start();
  BattleField& getField() noexcept;
  const BattleField& getField() const noexcept;
  void loadField(const BattleField& field, const Point2D& setPCLocation) noexcept;

  [[nodiscard]] bool handleLeftClick(const Point2D& click);
  void setControlledMob(const std::shared_ptr<Mob>& mob);
  std::expected<const Point2D, ErrorType> getControlledMobLocation() const;
  std::shared_ptr<Mob>& getControlledMob();
  CombatRenderer* getRenderer() noexcept;
  void setRenderer(CombatRenderer&& renderer);
  ObjectView* getObjectView();
  const CombatEventHandler& getEventHandler() const noexcept;

  void moveControlledMob(Point2D direction);

  bool quit = false;
  GameTime gameTime = GameTime::paused;

  void doTurn();
  const DropTable::ItemAtlas* itemAtlas{nullptr};
  const DropTable::DropTableAtlas* dropTableAtlas{nullptr};

private:
  BattleField battleField{{64, 64}, this};
  CombatEventHandler eventHandler{};
  EventQueue<Combat> eventQueue{};
  CombatRenderer renderer{};

  std::shared_ptr<Mob> controlledMob{};

  const std::uint32_t fps = 30;
  const std::uint32_t desiredDelta = 1000 / fps;
  std::uint64_t ticksNow = SDL_GetTicks();
  std::uint32_t delta = 0;
  int gameTickTimeMS = 500;
};

#endif
