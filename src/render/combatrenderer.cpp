#include "combatrenderer.h"
#include "../combat.h"
#include "texticon.h"
#include "ui/statsview.h"

using namespace sdlwrapp;

CombatRenderer::CombatRenderer(SDLWindow* window, SDLRenderer* renderer, Point2D windowSize, Combat* combat)
    : window(window), combat(combat) {
  this->fieldSize = combat->getField().getSize();
  this->renderer = renderer;
  this->tileSizeLimitFloor = calculateTileSizeLimitFloor(windowSize, this->fieldSize);
  this->camera.size = windowSize;
  this->camera.size.y -= textBoxSize + statsBarSize;
  this->fontRenderer =
      TTF(globals::default_font, this->tileSize < this->tileSizeLimitFloor ? this->tileSizeLimitFloor : this->tileSize);

  if (this->tileSize < tileSizeLimitFloor) {
    this->tileSize = tileSizeLimitFloor;
  } else if (this->tileSize > tileSizeLimitCeiling) {
    this->tileSize = tileSizeLimitCeiling;
  }

  this->camera.bounds = this->calculateCameraBounds(this->fieldSize);

  // TODO load testing data here - change this later
  // Textures statically loaded for testing purposes
  loadTextures();
}

CombatRenderer::CombatRenderer(CombatRenderer&& other) noexcept {
  this->window = other.window;
  this->renderer = other.renderer;
  this->camera = other.camera;
  this->tileSize = other.tileSize;
  this->textUI = other.textUI;
  this->tileSizeLimitFloor = other.tileSizeLimitFloor;
  this->tileSizeLimitCeiling = other.tileSizeLimitCeiling;
  this->fieldSize = other.fieldSize;

  for (auto& [id, texture] : other.textureAtlas) {
    this->textureAtlas.emplace(id, std::move(texture));
  }

  this->fontRenderer = std::move(other.fontRenderer);
  this->textBoxFont = std::move(other.textBoxFont);
  this->healthBarFont = std::move(other.healthBarFont);
}

CombatRenderer& CombatRenderer::operator=(CombatRenderer&& other) noexcept {
  if (this != &other) {
    this->window = other.window;
    this->renderer = other.renderer;
    this->camera = other.camera;
    this->tileSize = other.tileSize;
    this->textUI = other.textUI;
    this->tileSizeLimitFloor = other.tileSizeLimitFloor;
    this->tileSizeLimitCeiling = other.tileSizeLimitCeiling;
    this->fieldSize = other.fieldSize;

    for (auto& [id, texture] : other.textureAtlas) {
      this->textureAtlas.emplace(id, std::move(texture));
    }

    this->fontRenderer = std::move(other.fontRenderer);
    this->textBoxFont = std::move(other.textBoxFont);
    this->healthBarFont = std::move(other.healthBarFont);
  }
  return *this;
}

Point2D CombatRenderer::calculateCameraBounds(const Point2D& combatFieldSize) const noexcept {
  Point2D lowerRightBound = {combatFieldSize.x, combatFieldSize.y};
  return (lowerRightBound * this->tileSize - this->camera.size).moveNegCoordsToZero();
}

void CombatRenderer::setCameraPosition(const Point2D& center) noexcept {
  this->camera.position = center * this->tileSize - (this->camera.size / 2);
  if (this->camera.position.withinPositiveBounds(this->camera.bounds.x, this->camera.bounds.y)) {
    return;
  }
  this->camera.position.clamp(Point2D(0, 0), this->camera.bounds);
}

void CombatRenderer::setCameraBounds(Point2D newCameraBounds) noexcept {
  this->camera.bounds = newCameraBounds;
}

// TODO optimization possible - only update dirty tiles
void CombatRenderer::renderScene(const Combat& combatField) {
  auto playerPosition = combatField.getControlledMobLocation();
  if (playerPosition.has_value()) { // Leave the camera wherever it was when the field lost the player
    this->setCameraPosition(playerPosition.value());
  }
  this->renderField(combatField.getField());
  this->textUI.renderToTarget(*this->renderer);
  this->renderPresent();
}

void CombatRenderer::renderField(const BattleField& field) {
  std::int32_t renderX = 0;
  std::int32_t renderY = 0;

  // These need to be rounded, so we don't see any tiles being rendered below the UI when nearing the bottom of the
  // screen
  auto colStart = std::lround(static_cast<double>(this->camera.position.x) / this->fontRenderer.getSize());
  auto colEnd = (this->camera.position.x + this->camera.size.x) / this->fontRenderer.getSize();
  auto rowStart = std::lround(static_cast<double>(this->camera.position.y) / this->fontRenderer.getSize());
  auto rowEnd = (this->camera.position.y + this->camera.size.y) / this->fontRenderer.getSize();

  if (rowEnd >= fieldSize.y) {
    rowEnd = fieldSize.y - 1;
  }
  if (colEnd >= fieldSize.x) {
    colEnd = fieldSize.x - 1;
  }

  for (long row = rowStart; row <= rowEnd; row++) {
    for (long column = colStart; column <= colEnd; column++) {
      this->renderTile(field.getTile({static_cast<int>(column), static_cast<int>(row)}),
                       {renderX, renderY, static_cast<int32_t>(this->fontRenderer.getSize()),
                        static_cast<int32_t>(this->fontRenderer.getSize())});
      renderX += this->fontRenderer.getSize();
    }
    renderX = 0;
    renderY += this->fontRenderer.getSize();
  }
}

void CombatRenderer::renderTile(const BattleTile& tile, const SDLRect& rect) {
  // TODO for consideration: we check once if tile has x and then getFirstX also
  // checks. Refactor? Rewrite?
  if (tile.hasMob()) {
    auto firstMobIconID = tile.getFirstMobIconID();
    this->textureAtlas.at(firstMobIconID.value()).render(this->renderer, &rect);
  } else if (tile.hasBlocker()) {
    auto firstBlockerIconID = tile.getFirstBlockerIconID();
    this->textureAtlas.at(firstBlockerIconID.value()).render(this->renderer, &rect);
  } else if (tile.hasItem()) {
    auto firstItemIconID = tile.getFirstItemIconID();
    this->textureAtlas.at(firstItemIconID.value()).render(this->renderer, &rect);
  } else {
    this->textureAtlas.at(0).render(this->renderer, &rect);
  }
}

void CombatRenderer::renderPresent() const {
  this->renderer->present();
  this->renderer->clear();
}

void CombatRenderer::loadTextures() {
  // TODO fix this up. Quick and dirty for testing
  // NOTE when mobs are loaded from json, load text representation and optional
  // color field

  std::vector<TextIcon> textures = {{0, ".", {128, 128, 128}}, {1, "@", {200, 0, 0}}, {2, "X", {0, 200, 0}},
                                    {3, "O", {128, 128, 128}}, {4, "T", {0, 0, 255}}, {5, "U", {0, 50, 128}}};

  for (auto& texture : textures) {
    this->textureAtlas.insert(
        {texture.id, SDLTexture(*this->renderer, this->fontRenderer.renderTextBlended(texture.icon, texture.color))});
  }
}

void CombatRenderer::loadUI(const Point2D& windowSize) noexcept {
  this->textUI.loadCombatUI(this->combat, &this->textBoxFont, &this->healthBarFont, &this->menuButtonFont, windowSize,
                            CombatRenderer::textBoxSize, CombatRenderer::statsBarSize);
}

TextUI* CombatRenderer::getUI() noexcept {
  return &this->textUI;
}

void CombatRenderer::reloadFont() {
  this->fontRenderer = TTF(globals::default_font, this->tileSize);
}

std::uint32_t CombatRenderer::calculateTileSizeLimitFloor(const Point2D& windowSize,
                                                          const Point2D& combatFieldSize) noexcept {
  const float minTileSizeWidth = static_cast<float>(windowSize.x) / static_cast<float>(combatFieldSize.x);
  const float minTileSizeHeight = static_cast<float>(windowSize.y) / static_cast<float>(combatFieldSize.y);
  const std::uint32_t limit = minTileSizeWidth > minTileSizeHeight
                                  ? static_cast<std::uint32_t>(std::ceil(minTileSizeWidth))
                                  : static_cast<std::uint32_t>(std::ceil(minTileSizeHeight));

  if (limit > CombatRenderer::absoluteMaxTileSize) {
    return CombatRenderer::absoluteMaxTileSize;
  }
  if (limit < CombatRenderer::absoluteMinTileSize) {
    return CombatRenderer::absoluteMinTileSize;
  }
  return limit;
}

void CombatRenderer::zoomIn() noexcept {
  if (this->tileSize == this->tileSizeLimitCeiling) {
    return;
  }
  std::uint32_t newSize = this->tileSize + CombatRenderer::zoomFactor;
  if (newSize > this->tileSizeLimitCeiling) {
    this->tileSize = this->tileSizeLimitCeiling;
  } else {
    this->tileSize = newSize;
  }
  this->setCameraBounds(this->calculateCameraBounds(this->fieldSize));
  this->reloadFont();
}

void CombatRenderer::zoomOut() noexcept {
  if (this->tileSize == this->tileSizeLimitFloor) {
    return;
  }
  std::uint32_t newSize = this->tileSize - CombatRenderer::zoomFactor;
  if (newSize < this->tileSizeLimitFloor) {
    this->tileSize = this->tileSizeLimitFloor;
  } else {
    this->tileSize = newSize;
  }
  this->setCameraBounds(this->calculateCameraBounds(this->fieldSize));
  this->reloadFont();
}

uint32_t CombatRenderer::getTileSize() const noexcept {
  return this->tileSize;
}

SDLWindow* CombatRenderer::getWindow() {
  return this->window;
}

void CombatRenderer::setCombat(Combat* newCombat) noexcept {
  this->combat = newCombat;
}

void CombatRenderer::setWindowTitle(const std::string& title) {
  this->window->setWindowTitle(title);
}

void CombatRenderer::setFieldSize(const Point2D& newSize) {
  this->fieldSize = newSize;
}

SDLRenderer* CombatRenderer::getRenderer() noexcept {
  return this->renderer;
}

sdlwrapp::TTF& CombatRenderer::getTextBoxFont() noexcept {
  return this->textBoxFont;
}
