#ifndef COMBAT_RENDERER_H
#define COMBAT_RENDERER_H

#include "../math/point2d.h"
#include "../sdl2wrapp/sdl.h"
#include "../sdl2wrapp/sdlttf.h"
#include "camera.h"
#include "ui/textui.h"
#include "../model/globals.h"

#include <unordered_map>

class Combat;
class BattleField;
class BattleTile;

class CombatRenderer {

public:
  CombatRenderer() = default;
  CombatRenderer(sdlwrapp::SDLWindow* window, sdlwrapp::SDLRenderer* renderer, Point2D windowSize, Combat* combat);
  CombatRenderer(CombatRenderer&& other) noexcept ;
  CombatRenderer(const CombatRenderer& source) = delete;
  ~CombatRenderer() = default;

  CombatRenderer& operator=(CombatRenderer&& other) noexcept;

  Point2D calculateCameraBounds(const Point2D& combatFieldSize) const noexcept;
  void setCameraPosition(const Point2D& center) noexcept;
  void setCameraBounds(Point2D newCameraBounds) noexcept;

  void zoomIn() noexcept;
  void zoomOut() noexcept;

  void renderScene(const Combat& combatField);
  void renderField(const BattleField& field);
  void renderTile(const BattleTile& tile, const sdlwrapp::SDLRect& rect);

  void renderPresent() const;
  void loadTextures();
  void loadUI(const Point2D& windowSize) noexcept;
  TextUI* getUI() noexcept;
  void reloadFont();
  void setCombat(Combat* newCombat) noexcept;
  void setWindowTitle(const std::string& title);
  void setFieldSize(const Point2D& newSize);

  sdlwrapp::SDLRenderer* getRenderer() noexcept;
  sdlwrapp::TTF& getTextBoxFont() noexcept;

  std::uint32_t getTileSize() const noexcept;
  sdlwrapp::SDLWindow* getWindow();

private:
  static std::uint32_t calculateTileSizeLimitFloor(const Point2D& windowSize, const Point2D& combatFieldSize) noexcept;
  sdlwrapp::SDLWindow* window{nullptr};
  sdlwrapp::SDLRenderer* renderer{};
  sdlwrapp::TTF fontRenderer{globals::default_font};
  sdlwrapp::TTF textBoxFont{globals::default_font, 12};
  sdlwrapp::TTF menuButtonFont{globals::default_font, 22};
  sdlwrapp::TTF healthBarFont{globals::default_font, 9};
  TextUI textUI{};
  const std::set<sdlwrapp::RendererFlag> flags{sdlwrapp::RendererFlag::accelerated};
  std::unordered_map<std::uint64_t, sdlwrapp::SDLTexture> textureAtlas{};
  Point2D fieldSize{};
  std::uint32_t tileSizeLimitFloor{16};
  std::uint32_t tileSizeLimitCeiling{64};
  Camera camera{};
  std::uint32_t tileSize{20};
  Combat* combat{nullptr};

  static constexpr std::uint32_t zoomFactor{4};
  static constexpr std::uint32_t absoluteMinTileSize = 8;
  static constexpr std::uint32_t absoluteMaxTileSize = 64;
  static constexpr std::int32_t textBoxSize = 128;
  static constexpr std::int32_t statsBarSize = 16;
};

#endif
