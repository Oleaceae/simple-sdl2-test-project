#ifndef GAME_CAMERA_H
#define GAME_CAMERA_H

#include "../math/point2d.h"

struct Camera {
  Point2D position{};
  Point2D size{};
  Point2D bounds{};
};

#endif