#ifndef GAME_SPRITE_H
#define GAME_SPRITE_H

#include <cstdint>
#include <memory>
#include <utility>
#include <vector>

#include "../sdl2wrapp/sdl.h"

class Sprite {
public:
  Sprite(const sdlwrapp::SDLRenderer& renderer, const std::string& filePath, std::uint16_t size, std::uint16_t col,
         std::uint16_t row);
  Sprite() = default;
  explicit Sprite(const std::shared_ptr<sdlwrapp::SDLTexture>& texture) : texture(texture){};

  /*
   * Loading sprite by file path and icon state requires some form of metadata - either inside the file's EXIF data text
   *fields or in an outside file(terrible). Consider this later TODO Sprite(std::string filePath, std::string
   *iconState);
   */

  std::shared_ptr<sdlwrapp::SDLTexture> texture{};

  static sdlwrapp::SDLSurface createSpriteSurface(const std::string& filePath, std::uint16_t size, std::uint16_t col,
                                                  std::uint16_t row);
private:
  static std::shared_ptr<sdlwrapp::SDLTexture> createSpriteTexture(const sdlwrapp::SDLRenderer& renderer,
                                                               const std::string& filePath, std::uint16_t size,
                                                               std::uint16_t col, std::uint16_t row);
};

struct Sprites {
  Sprites(const sdlwrapp::SDLRenderer& renderer, const std::string& filePath, std::uint16_t size, std::uint16_t row,
          std::uint16_t states);

  std::vector<Sprite> sprites{};
};

#endif