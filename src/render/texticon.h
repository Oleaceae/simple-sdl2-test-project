#ifndef TEXTICON_H
#define TEXTICON_H

#include <string>

#include "../sdl2wrapp/sdl.h"

struct TextIcon {
  std::uint32_t id{};
  std::string icon{};
  sdlwrapp::SDLColor color{};
};

#endif