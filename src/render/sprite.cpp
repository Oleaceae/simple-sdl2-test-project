#include "sprite.h"

#include "../sdl2wrapp/sdlimage.h"

using namespace sdlwrapp;

Sprite::Sprite(const SDLRenderer& renderer, const std::string& filePath, const std::uint16_t size,
               const std::uint16_t col, const std::uint16_t row) {
  this->texture = createSpriteTexture(renderer, filePath, size, col, row);
}

Sprites::Sprites(const SDLRenderer& renderer, const std::string& filePath, const std::uint16_t size,
                 const std::uint16_t row, const std::uint16_t states) {
  for (int i = 0; i < states; i++) {
    this->sprites.emplace_back(renderer, filePath, size, i, row);
  }
}

std::shared_ptr<SDLTexture> Sprite::createSpriteTexture(const SDLRenderer& renderer, const std::string& filePath,
                                                        const std::uint16_t size, const std::uint16_t col,
                                                        const std::uint16_t row) {
  return std::make_shared<SDLTexture>(SDLTexture(renderer, createSpriteSurface(filePath, size, col, row)));
}

SDLSurface Sprite::createSpriteSurface(const std::string& filePath, const std::uint16_t size, const std::uint16_t col,
                                       const std::uint16_t row) {
  const SDLSurface sheet = SDLImage::load(filePath);
  SDLSurface sprite = SDLSurface::createTransparentRGBSurface(size, size);
  const SDLRect src = {col * size, row * size, size, size};
  const SDLRect dest = {0, 0, size, size};

  sheet.blit(src, sprite, dest);
  return sprite;
}
