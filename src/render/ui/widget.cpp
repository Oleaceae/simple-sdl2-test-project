#include "widget.h"

using namespace sdlwrapp;

void Widget::render(SDLRenderer& renderer) {
  this->renderChildren(renderer);
}

void Widget::renderChildren(SDLRenderer& renderer) {
  for (const auto& widget : this->children) {
    widget->render(renderer);
  }
}

bool Widget::handleLeftClick(const Point2D& click) {
  return false;
}

bool Widget::handleMidClick(const Point2D& click) {
  return false;
}

bool Widget::handleRightClick(const Point2D& click) {
  return false;
}

bool Widget::handleKey(const SDL_Keycode key) {
  return false;
}

bool Widget::handleMouseWheel(const Point2D& mousePos, std::int32_t wheelDir) {
  return false;
}

void Widget::add(const std::shared_ptr<Widget>& child) {
  const auto& [x, y, w, h] = child->area.rect;
  child->area.rect.x = x + this->area.rect.x;
  child->area.rect.y = y + this->area.rect.y;
  child->afterAreaChanged();
  child->positionAddedChildren({this->area.rect.x, this->area.rect.y});
  this->children.push_back(child);
}

std::vector<std::shared_ptr<Widget>>& Widget::getChildren() noexcept {
  return this->children;
}

void Widget::afterAreaChanged() {
}

void Widget::move(const Point2D& direction) noexcept {
  this->area.rect.x = this->area.rect.x + direction.x;
  this->area.rect.y = this->area.rect.y + direction.y;
  this->positionAddedChildren(direction);
}

void Widget::align(const Alignment alignment, const Point2D& within) noexcept {
  switch (alignment) {
  case Alignment::vertical:
    this->area.rect.y = within.y / 2 - this->area.rect.h / 2;
    break;
  default:
    break;
  }
}

void Widget::positionAddedChildren(const Point2D& offset) {
  for (const auto& child : this->getChildren()) {
    child->area.rect.x = child->area.rect.x + offset.x;
    child->area.rect.y = child->area.rect.y + offset.y;
    child->positionAddedChildren(offset);
  }
}
