#ifndef TEXTUI_H
#define TEXTUI_H

#include <memory>
#include <vector>

#include "../../math/point2d.h"
#include "../../sdl2wrapp/sdl.h"
#include "../../sdl2wrapp/sdlttf.h"
#include "button.h"
#include "widget.h"

class Container;
class Combat;


class TextUI {
public:
  enum class Menu {
    character,
    equipment,
    inventory,
    team,
    skills,
    logbook,
    none
  };

  [[nodiscard]] sdlwrapp::SDLSurface renderSurface() const;
  [[nodiscard]] sdlwrapp::SDLTexture renderTexture() const;
  void renderToTarget(sdlwrapp::SDLRenderer& renderer);

  bool handleLeftClick(const Point2D& click) const;

  template <WidgetType W> void add(W widget);
  void add(const std::shared_ptr<Widget>& child);

  void openMenu(Menu menuType, Combat* combat) noexcept;
  void reopenMenu(const std::shared_ptr<Widget>& menu, Menu type);
  [[nodiscard]] bool hasOpenMenu() const;
  [[nodiscard]] std::shared_ptr<Widget> getOpenMenu();
  [[nodiscard]] Menu getOpenMenuType() const noexcept;
  void loadCombatUI(Combat* combat, sdlwrapp::TTF* textBoxFont, sdlwrapp::TTF* healthBarFont, sdlwrapp::TTF* buttonFont,
                    const Point2D& windowSize, std::int32_t textBoxSize, std::int32_t statsBarSize);

private:
  std::vector<std::shared_ptr<Widget>> widgets{};
  std::shared_ptr<Widget> openedMenu{nullptr};
  Menu openedMenuType{Menu::none};

  Point2D windowSize{800, 600};
  sdlwrapp::SDLRect menuArea{0, 0, 800, 600};

  std::shared_ptr<Widget> characterMenu(Combat* combat) const;
  std::shared_ptr<Widget> equipmentMenu(Combat* combat) const;
  std::shared_ptr<Widget> inventoryMenu(Combat* combat) const;
  std::shared_ptr<Widget> teamMenu(Combat* combat) const;
  std::shared_ptr<Widget> skillsMenu(Combat* combat) const;
  std::shared_ptr<Widget> logBookMenu(Combat* combat) const;

  // Resizes and repositions a menu. Widget must contain a child of type Sheet
  static std::shared_ptr<Widget> updateMenu(const std::shared_ptr<Widget>& menu, const sdlwrapp::SDLRect& newSize);

  static std::shared_ptr<Widget> menuContainer();

  [[nodiscard]] sdlwrapp::SDLRect getCharacterMenuArea() const noexcept;
  [[nodiscard]] sdlwrapp::SDLRect getEquipmentMenuArea() const noexcept;
  [[nodiscard]] sdlwrapp::SDLRect getInventoryMenuArea() const noexcept;
  [[nodiscard]] sdlwrapp::SDLRect getTeamMenuArea() const noexcept;
  [[nodiscard]] sdlwrapp::SDLRect getSkillsMenuArea() const noexcept;
  [[nodiscard]] sdlwrapp::SDLRect getLogbookMenuArea() const noexcept;

  // Returns a new SDLRect with padding added to each side
  static constexpr sdlwrapp::SDLRect getAvailableMenuArea(const sdlwrapp::SDLRect& src, int pad);

  std::vector<std::shared_ptr<SpriteButton>> menuButtons{};

  static constexpr std::int32_t topButtonBarHeight = 32;
  static constexpr std::int32_t topButtonBarWidth = 300;
  static constexpr std::int32_t menuItemSize = 32;
  static constexpr std::int32_t minimapSize = 128;
  static constexpr std::uint16_t minimapEntitySize = 3;
  static constexpr std::uint16_t healthBarWidth = 128;
  static constexpr std::uint16_t healthBarSpacing = 16;
  static constexpr std::uint16_t healthBarBottomSpacing = 64;
  static constexpr std::uint16_t healthBarHeight = 16;
  static constexpr std::uint8_t menuElemPadding = 8;
  static constexpr sdlwrapp::SDLColor menuBackground = {128, 0, 128, 225};
};

template <WidgetType W> void TextUI::add(W widget) {
  this->widgets.push_back(std::make_shared<W>(widget));
}

#endif
