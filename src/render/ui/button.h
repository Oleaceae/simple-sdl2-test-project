#ifndef GAME_BUTTON_H
#define GAME_BUTTON_H

#include <functional>

#include "../../sdl2wrapp/sdl.h"
#include "../sprite.h"
#include "widget.h"

class Button : public Widget {
public:
  explicit Button(sdlwrapp::SDLRect area = {});
  void render(sdlwrapp::SDLRenderer& renderer) override;
  bool handleLeftClick(const Point2D& click) override;
  void addBorder(sdlwrapp::SDLColor color = {255, 255, 255}) noexcept;
  std::function<void()> onClick{};

  bool border{false};
  sdlwrapp::SDLColor borderColor{};

private:
};

class TextButton : public Button {
public:
  explicit TextButton(sdlwrapp::TTF* font, const std::u16string& innerText = u"", sdlwrapp::SDLRect area = {},
                      sdlwrapp::SDLColor textColor = {255, 255, 255});
  void render(sdlwrapp::SDLRenderer& renderer) override;

protected:
  void afterAreaChanged() override;

private:
  sdlwrapp::SDLRect renderArea{};
  sdlwrapp::TTF* font{};
  std::u16string innerText{};
  sdlwrapp::SDLColor textColor{};

  static sdlwrapp::SDLRect centerText(const sdlwrapp::TTF* f, const std::u16string& text,
                                      const sdlwrapp::SDLRect& area);
};

class SpriteButton : public Button {
public:
  explicit SpriteButton(sdlwrapp::SDLRect area, Sprites&& sprites);
  void render(sdlwrapp::SDLRenderer& renderer) override;
  void setSpriteState(std::uint8_t newState);

  std::uint8_t spriteState{};

private:
  Sprites sprites;
};

#endif
