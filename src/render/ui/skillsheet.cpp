#include "skillsheet.h"
#include "../../model/objectview.h"

SkillSheet::SkillSheet(const ObjectView* objV, const std::weak_ptr<Mob>& initialTarget,
                       const sdlwrapp::SDLRect& availableArea, std::shared_ptr<SpriteButton> button)
    : Sheet(button) {
}

void SkillSheet::update() {

}

void SkillSheet::updateSize(const sdlwrapp::SDLRect& availableArea) {

}
