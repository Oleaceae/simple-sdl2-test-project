#include "textbox.h"

using namespace sdlwrapp;

TextBox::TextBox(const TTF* font) {
  this->font = font;
}

void TextBox::render(SDLRenderer& renderer) {
  renderer.fillRectColor(this->area, this->backgroundColor);

  this->consume();

  auto renderArea = SDLRect(this->area);
  const int fontSize = static_cast<int>(this->font->getSize());
  renderArea.rect.h = fontSize;

  for (const auto& string : this->lineCache | std::views::reverse) {
    auto text = this->font->renderTextBlended(string);
    auto textTexture = SDLTexture(renderer, text);
    renderArea.rect.w = text.get()->w;
    textTexture.render(&renderer, &renderArea);
    renderArea.rect.y = renderArea.rect.y + fontSize;
  }

  this->renderChildren(renderer);
}

void TextBox::push_front(const std::string& line) {
  this->storedLines.push_front(line);
  if (this->storedLines.size() > this->maxLines) {
    this->storedLines.resize(this->maxLines);
  }
  this->updateCache();
}

void TextBox::consume() {
  while (!MessageQueue::instance().isEmpty()) {
    this->push_front(MessageQueue::instance().take());
  }
}

void TextBox::updateCache() {
  auto& [x, y, w, h] = this->area.rect;
  const auto fontHeight = font->getSize();
  const auto maxDisplayableLines = h / fontHeight;
  const auto fontWidth = font->getTextSize(".").x;
  const auto maxDisplayableCharacters = w / fontWidth;

  this->lineCache.clear();

  auto cachedLines = 0;
  for (const auto& line : this->storedLines) {
    if (line.size() > maxDisplayableCharacters) {
      std::string fullLine = line;
      std::vector<std::string> brokenLine = {};
      std::uint32_t pos = 0;

      while (pos < fullLine.size()) {
        brokenLine.emplace_back(fullLine.substr(pos, maxDisplayableCharacters));
        pos += maxDisplayableCharacters;
      }

      for (const auto& linePart : brokenLine | std::views::reverse) {
        lineCache.emplace_back(linePart);
        cachedLines++;
        if (cachedLines >= maxDisplayableLines) {
          break;
        }
      }
    } else {
      this->lineCache.emplace_back(line);
      cachedLines++;
    }
    if (cachedLines >= maxDisplayableLines) {
      break;
    }
  }
}
