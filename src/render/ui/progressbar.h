#ifndef GAME_PROGRESSBAR_H
#define GAME_PROGRESSBAR_H

#include "../../model/objectview.h"
#include "../../sdl2wrapp/sdl.h"
#include "widget.h"

class ProgressBar : public Widget {
public:
  ~ProgressBar() override = default;
protected:
  static void drawUnknownAmount(sdlwrapp::SDLRect area, const sdlwrapp::TTF* font, sdlwrapp::SDLRenderer& renderer);
  static void drawKnownAmount(sdlwrapp::SDLRect area, const sdlwrapp::TTF* font, sdlwrapp::SDLRenderer& renderer,
                              std::tuple<std::int32_t, std::int32_t> value, sdlwrapp::SDLColor& fillColor) noexcept;
  static void drawInnerText(sdlwrapp::SDLRect& area, sdlwrapp::SDLRenderer& renderer, const sdlwrapp::SDLSurface& text);

  sdlwrapp::SDLColor emptyColor{};
  sdlwrapp::SDLColor fillColor{};
};

class HealthBar final : public ProgressBar {
public:
  HealthBar(sdlwrapp::SDLRect area, const ObjectView* view, const sdlwrapp::TTF* font);
  void render(sdlwrapp::SDLRenderer& renderer) override;

private:
  const ObjectView* objectView{nullptr};
  const sdlwrapp::TTF* font{nullptr};
};

class ManaBar final : public ProgressBar {
public:
  ManaBar(sdlwrapp::SDLRect area, const ObjectView* view, const sdlwrapp::TTF* font);
  void render(sdlwrapp::SDLRenderer& renderer) override;

private:
  const ObjectView* objectView{nullptr};
  const sdlwrapp::TTF* font{nullptr};
};

#endif
