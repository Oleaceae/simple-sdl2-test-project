#ifndef GAME_CONTAINER_H
#define GAME_CONTAINER_H

#include "widget.h"

class Container : public Widget {
public:
  void render(sdlwrapp::SDLRenderer& renderer) override;

  [[nodiscard]] bool handleLeftClick(const Point2D& click) override;
  [[nodiscard]] bool handleKey(SDL_Keycode key) override;

  sdlwrapp::SDLColor backgroundColor{};
  bool background{false};

private:
};

#endif
