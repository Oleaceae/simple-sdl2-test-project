#include "image.h"

#include <utility>

using namespace sdlwrapp;

Image::Image(const SDLRect area, Sprite sprite) {
  this->area = area;
  this->imageCache = std::move(sprite);
}

Image::Image(Sprite sprite) : imageCache(std::move(sprite)) {
}

void Image::render(SDLRenderer& renderer) {
  this->imageCache.texture->render(&renderer, &this->area);
  this->renderChildren(renderer);
}
