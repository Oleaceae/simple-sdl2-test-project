#include "sheet.h"

Sheet::Sheet(const std::shared_ptr<SpriteButton>& button) : button(button) {
}

void Sheet::onOpen() const {
  this->button->setSpriteState(1);
}

void Sheet::onClose() const {
  this->button->setSpriteState(0);
}