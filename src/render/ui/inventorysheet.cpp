#include "inventorysheet.h"

InventorySheet::InventorySheet(const ObjectView* objV, const std::weak_ptr<Mob>& initialTarget,
                               const sdlwrapp::SDLRect& availableArea, std::shared_ptr<SpriteButton> button)
    : Sheet(button) {
}

void InventorySheet::update() {

}

void InventorySheet::updateSize(const sdlwrapp::SDLRect& availableArea) {

}
