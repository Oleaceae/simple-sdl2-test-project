#ifndef GAME_CHARACTERSHEET_H
#define GAME_CHARACTERSHEET_H

#include "image.h"

#include <memory>

#include "../../model/objectview.h"
#include "sheet.h"
#include "widget.h"

class Combat;

class CharacterSheet final : public Sheet {
public:
  CharacterSheet(const sdlwrapp::SDLRenderer& renderer, const sdlwrapp::TTF& font, Combat* combat,
                 const std::shared_ptr<Mob>& initialTarget, const sdlwrapp::SDLRect& availableArea,
                 const std::shared_ptr<SpriteButton>& button);

  void update() override;
  void updateSize(const sdlwrapp::SDLRect& availableArea) override;

  void render(sdlwrapp::SDLRenderer& renderer) override;
  bool handleKey(SDL_Keycode key) override;

private:
  const ObjectView* objectView{};
  Combat* combat{};
  std::shared_ptr<Mob> target{};

  std::shared_ptr<Widget> attributes(const sdlwrapp::SDLRect& size, const std::shared_ptr<Mob>& valueSource,
                                     const std::string& spritePath);
  std::shared_ptr<Widget> resistances(const sdlwrapp::SDLRect& size, const Resistances& values,
                                      const std::string& spritePath);
  [[nodiscard]] Image createResistanceHint(const Point2D& imgSize, const Point2D& loc, const std::string& attrPath,
                                           float value) const;
  [[nodiscard]] std::shared_ptr<Widget> createDamageWidget(Sprite&& icon, const Damage& damage,
                                                           const Point2D& availableArea) const;
  [[nodiscard]] std::shared_ptr<Widget> createCurMaxWidget(Sprite&& icon, const CurMax& curMax,
                                                           const Point2D& availableArea) const;
  [[nodiscard]] std::shared_ptr<Widget> createAttributeWidget(Sprite&& icon, const Attribute& attribute,
                                                              const Point2D& availableArea) const;
  [[nodiscard]] static int resistanceValueToSteps(float value) noexcept;
};

#endif
