#include "textui.h"
#include "../../combat.h"
#include "../../sound/playsound.h"
#include "button.h"
#include "charactersheet.h"
#include "container.h"
#include "equipmentsheet.h"
#include "inventorysheet.h"
#include "logsheet.h"
#include "minimap.h"
#include "progressbar.h"
#include "skillsheet.h"
#include "statsview.h"
#include "teamsheet.h"
#include "textbox.h"

#include <algorithm>

using namespace sdlwrapp;
using namespace sound;

SDLSurface TextUI::renderSurface() const {
  // TODO unimplemented
  return SDLSurface();
}

SDLTexture TextUI::renderTexture() const {
  // TODO unimplemented
  return SDLTexture();
}

void TextUI::renderToTarget(SDLRenderer& renderer) {
  for (auto& widget : this->widgets) {
    widget->render(renderer);
  }
}

bool TextUI::handleLeftClick(const Point2D& click) const {
  return std::ranges::any_of(this->widgets,
                             [&](const std::shared_ptr<Widget>& widget) { return widget->handleLeftClick(click); });
}

void TextUI::add(const std::shared_ptr<Widget>& child) {
  this->widgets.push_back(child);
}

void TextUI::openMenu(const Menu menuType, Combat* combat) noexcept {
  if (this->hasOpenMenu()) {
    for (auto it = this->widgets.begin(); it != this->widgets.end(); it++) {
      if (*it == this->openedMenu) {
        std::dynamic_pointer_cast<Sheet>(openedMenu->getChildren()[0])->onClose();
        this->widgets.erase(it);
        this->openedMenu.reset();
        if (this->openedMenuType != menuType) {
          break;
        }
        this->openedMenuType = Menu::none;
        return;
      }
    }
  }
  std::shared_ptr<Widget> menu;
  switch (menuType) {
  case Menu::character:
    playsound("assets/sound/effect/rustle5.ogg");
    menu = TextUI::characterMenu(combat);
    break;
  case Menu::equipment:
    playsound("assets/sound/effect/rustle3.ogg");
    menu = TextUI::equipmentMenu(combat);
    break;
  case Menu::inventory:
    playsound("assets/sound/effect/rustle5.ogg");
    menu = TextUI::inventoryMenu(combat);
    break;
  case Menu::team:
    playsound("assets/sound/effect/pageturn1.ogg");
    menu = TextUI::teamMenu(combat);
    break;
  case Menu::skills:
    playsound("assets/sound/effect/pageturn2.ogg");
    menu = TextUI::skillsMenu(combat);
    break;
  case Menu::logbook:
    playsound("assets/sound/effect/pageturn3.ogg");
    menu = TextUI::logBookMenu(combat);
    break;
  default:
    break;
  }
  this->openedMenu = menu;
  this->openedMenuType = menuType;
  std::dynamic_pointer_cast<Sheet>(openedMenu->getChildren()[0])->onOpen();
  this->widgets.push_back(menu);
}

void TextUI::reopenMenu(const std::shared_ptr<Widget>& menu, const Menu type) {
  this->openedMenuType = type;
  this->openedMenu = menu;

  switch (type) {
  case Menu::character:
    this->add(updateMenu(menu, getCharacterMenuArea()));
    break;
  case Menu::equipment:
    this->add(updateMenu(menu, getEquipmentMenuArea()));
    break;
  case Menu::inventory:
    this->add(updateMenu(menu, getInventoryMenuArea()));
    break;
  case Menu::team:
    this->add(updateMenu(menu, getTeamMenuArea()));
    break;
  case Menu::skills:
    this->add(updateMenu(menu, getSkillsMenuArea()));
    break;
  case Menu::logbook:
    this->add(updateMenu(menu, getLogbookMenuArea()));
    break;
  case Menu::none:
    break;
  }
}

void TextUI::loadCombatUI(Combat* combat, TTF* textBoxFont, TTF* healthBarFont, TTF* buttonFont, const Point2D& windowS,
                          const std::int32_t textBoxSize, const std::int32_t statsBarSize) {
  this->windowSize = windowS;
  SDLRenderer& renderer = *combat->getRenderer()->getRenderer();

  auto topButtonBar = Container();
  topButtonBar.area = {windowSize.x / 2 - topButtonBarWidth / 2, 0, topButtonBarWidth, topButtonBarHeight};
  topButtonBar.background = true;
  topButtonBar.backgroundColor = {30, 50, 0};

  this->menuButtons = {
      std::make_shared<SpriteButton>(
          SpriteButton({0, 0, menuItemSize, menuItemSize}, Sprites(renderer, "assets/icons/inventory.png", 32, 0, 2))),
      std::make_shared<SpriteButton>(SpriteButton({menuItemSize, 0, menuItemSize, menuItemSize},
                                                  Sprites(renderer, "assets/icons/inventory.png", 32, 1, 2))),
      std::make_shared<SpriteButton>(SpriteButton({menuItemSize * 2, 0, menuItemSize, menuItemSize},
                                                  Sprites(renderer, "assets/icons/inventory.png", 32, 2, 2))),
      std::make_shared<SpriteButton>(SpriteButton({topButtonBarWidth - menuItemSize * 3, 0, menuItemSize, menuItemSize},
                                                  Sprites(renderer, "assets/icons/inventory.png", 32, 3, 2))),
      std::make_shared<SpriteButton>(SpriteButton({topButtonBarWidth - menuItemSize * 2, 0, menuItemSize, menuItemSize},
                                                  Sprites(renderer, "assets/icons/inventory.png", 32, 4, 2))),
      std::make_shared<SpriteButton>(SpriteButton({topButtonBarWidth - menuItemSize, 0, menuItemSize, menuItemSize},
                                                  Sprites(renderer, "assets/icons/inventory.png", 32, 5, 2))),
  };

  menuButtons[0]->onClick = [this, combat]() { this->openMenu(Menu::character, combat); };
  menuButtons[1]->onClick = [this, combat]() { this->openMenu(Menu::equipment, combat); };
  menuButtons[2]->onClick = [this, combat]() { this->openMenu(Menu::inventory, combat); };
  menuButtons[3]->onClick = [this, combat]() { this->openMenu(Menu::team, combat); };
  menuButtons[4]->onClick = [this, combat]() { this->openMenu(Menu::skills, combat); };
  menuButtons[5]->onClick = [this, combat]() { this->openMenu(Menu::logbook, combat); };

  std::ranges::for_each(menuButtons,
                        [&topButtonBar](const std::shared_ptr<SpriteButton>& item) { topButtonBar.add(item); });

  TextBox textbox = TextBox(textBoxFont);
  textbox.area = {0, windowSize.y - textBoxSize - statsBarSize, windowSize.x - minimapSize, textBoxSize};
  textbox.backgroundColor = {100, 0, 50};

  Container statsBar = Container();
  statsBar.area = {0, windowSize.y - statsBarSize, windowSize.x, statsBarSize};
  statsBar.background = true;
  statsBar.backgroundColor = {100, 50, 0};

  auto statsView = StatsView({}, combat->getObjectView(), textBoxFont);
  statsBar.add(std::make_shared<StatsView>(std::move(statsView)));

  auto minimapContainer = Container();
  minimapContainer.background = true;
  minimapContainer.area = {windowSize.x - minimapSize, windowSize.y - minimapSize - statsBarSize, minimapSize,
                           minimapSize};

  const SDLRect area = {0, 0, minimapSize - minimapEntitySize, minimapSize - minimapEntitySize};
  auto minimap = Minimap(area, combat->getObjectView(), combat->getField().getSize(), minimapEntitySize);

  minimapContainer.add(minimap);

  auto healthManaContainer = Container();
  healthManaContainer.area = {windowSize.x / 2 - (healthBarWidth + healthBarSpacing / 2),
                              windowSize.y - (textBoxSize + statsBarSize + healthBarBottomSpacing + healthBarHeight),
                              healthBarWidth * 2 + healthBarSpacing, healthBarHeight};

  auto healthBar = HealthBar({0, 0, healthBarWidth, healthBarHeight}, combat->getObjectView(), healthBarFont);
  auto manaBar = ManaBar({healthBarWidth + healthBarSpacing, 0, healthBarWidth, healthBarHeight},
                         combat->getObjectView(), healthBarFont);

  healthManaContainer.add(healthBar);
  healthManaContainer.add(manaBar);

  this->add(topButtonBar);
  this->add(textbox);
  this->add(statsBar);
  this->add(minimapContainer);
  this->add(healthManaContainer);
}

std::shared_ptr<Widget> TextUI::characterMenu(Combat* combat) const {
  std::shared_ptr<Widget> menu = TextUI::menuContainer();
  menu->area = this->getCharacterMenuArea();

  menu->add(CharacterSheet(*combat->getRenderer()->getRenderer(), combat->getRenderer()->getTextBoxFont(), combat,
                           combat->getControlledMob(), getAvailableMenuArea(menu->area, menuElemPadding),
                           this->menuButtons[0]));

  return menu;
}

std::shared_ptr<Widget> TextUI::equipmentMenu(Combat* combat) const {
  auto menu = TextUI::menuContainer();
  menu->area = this->getEquipmentMenuArea();

  menu->add(EquipmentSheet(*combat->getRenderer()->getRenderer(), combat->getRenderer()->getTextBoxFont(), combat,
                           *combat->getObjectView(), combat->getControlledMob(),
                           getAvailableMenuArea(menu->area, menuElemPadding), this->menuButtons[1]));

  return menu;
}

std::shared_ptr<Widget> TextUI::inventoryMenu(Combat* combat) const {
  auto menu = TextUI::menuContainer();
  menu->area = this->getInventoryMenuArea();

  menu->add(InventorySheet(combat->getObjectView(), combat->getControlledMob(),
                           getAvailableMenuArea(menu->area, menuElemPadding), this->menuButtons[2]));

  return menu;
}

std::shared_ptr<Widget> TextUI::teamMenu(Combat* combat) const {
  auto menu = TextUI::menuContainer();
  menu->area = this->getTeamMenuArea();

  menu->add(TeamSheet(combat->getObjectView(), combat->getControlledMob(), getAvailableMenuArea(menu->area, menuElemPadding),
                      this->menuButtons[3]));
  return menu;
}

std::shared_ptr<Widget> TextUI::skillsMenu(Combat* combat) const {
  auto menu = TextUI::menuContainer();
  menu->area = this->getSkillsMenuArea();

  menu->add(SkillSheet(combat->getObjectView(), combat->getControlledMob(), getAvailableMenuArea(menu->area, menuElemPadding),
                       this->menuButtons[4]));
  return menu;
}

std::shared_ptr<Widget> TextUI::logBookMenu(Combat* combat) const {
  auto menu = TextUI::menuContainer();
  menu->area = this->getLogbookMenuArea();

  menu->add(LogSheet(combat->getObjectView(), combat->getControlledMob(), getAvailableMenuArea(menu->area, menuElemPadding),
                     this->menuButtons[5]));

  return menu;
}

std::shared_ptr<Widget> TextUI::updateMenu(const std::shared_ptr<Widget>& menu, const SDLRect& newSize) {
  menu->area = newSize;
  const SDLRect innerArea = {menuElemPadding, menuElemPadding, newSize.rect.w - menuElemPadding * 2,
                             newSize.rect.h - menuElemPadding * 2};
  std::dynamic_pointer_cast<Sheet>(menu->getChildren().at(0))->updateSize(innerArea);
  const auto childToReposition = menu->getChildren()[0];
  menu->getChildren().clear();
  menu->add(childToReposition);
  return menu;
}

std::shared_ptr<Widget> TextUI::menuContainer() {
  auto menu = std::make_shared<Container>();

  menu->background = true;
  menu->backgroundColor = menuBackground;

  return menu;
}

SDLRect TextUI::getCharacterMenuArea() const noexcept {
  return {static_cast<int>(windowSize.x * 0.3), static_cast<int>(windowSize.y * 0.05),
          static_cast<int>(windowSize.x * 0.4), static_cast<int>(windowSize.y * 0.68)};
}

SDLRect TextUI::getEquipmentMenuArea() const noexcept {
  return {static_cast<int>(windowSize.x * 0.1), static_cast<int>(windowSize.y * 0.05),
          static_cast<int>(windowSize.x * 0.8), static_cast<int>(windowSize.y * 0.68)};
}

SDLRect TextUI::getInventoryMenuArea() const noexcept {
  return {static_cast<int>(windowSize.x * 0.1), static_cast<int>(windowSize.y * 0.05),
          static_cast<int>(windowSize.x * 0.3), static_cast<int>(windowSize.y * 0.68)};
}

SDLRect TextUI::getTeamMenuArea() const noexcept {
  return {static_cast<int>(windowSize.x * 0.1), static_cast<int>(windowSize.y * 0.05),
          static_cast<int>(windowSize.x * 0.3), static_cast<int>(windowSize.y * 0.68)};
}

SDLRect TextUI::getSkillsMenuArea() const noexcept {
  return {static_cast<int>(windowSize.x * 0.1), static_cast<int>(windowSize.y * 0.05),
          static_cast<int>(windowSize.x * 0.3), static_cast<int>(windowSize.y * 0.68)};
}

SDLRect TextUI::getLogbookMenuArea() const noexcept {
  return {static_cast<int>(windowSize.x * 0.1), static_cast<int>(windowSize.y * 0.05),
          static_cast<int>(windowSize.x * 0.3), static_cast<int>(windowSize.y * 0.68)};
}

constexpr SDLRect TextUI::getAvailableMenuArea(const SDLRect& src, const int pad) {
  return {pad, pad, src.rect.w - pad * 2, src.rect.h - pad * 2};
}

bool TextUI::hasOpenMenu() const {
  return this->openedMenu != nullptr;
}

std::shared_ptr<Widget> TextUI::getOpenMenu() {
  return this->openedMenu;
}

TextUI::Menu TextUI::getOpenMenuType() const noexcept {
  return this->openedMenuType;
}
