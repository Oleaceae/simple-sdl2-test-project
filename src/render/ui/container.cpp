#include "container.h"

#include <algorithm>

using namespace sdlwrapp;

void Container::render(SDLRenderer& renderer) {
  // Render a background
  if (background) {
    renderer.fillRectColor(this->area, this->backgroundColor);
  }
  this->renderChildren(renderer);
}

bool Container::handleLeftClick(const Point2D& click) {
  return std::ranges::any_of(this->children,
                             [&](const std::shared_ptr<Widget>& widget) { return widget->handleLeftClick(click); });
}

bool Container::handleKey(const SDL_Keycode key) {
  return std::ranges::any_of(this->children,
                             [&](const std::shared_ptr<Widget>& widget) { return widget->handleKey(key); });
}
