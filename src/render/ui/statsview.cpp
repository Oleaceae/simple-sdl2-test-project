#include "statsview.h"

using namespace sdlwrapp;

StatsView::StatsView(SDLRect area, const ObjectView* view, const TTF* font) : objectView(view), font(font) {
  this->area = area;
}

void StatsView::render(SDLRenderer& renderer) {
  std::vector<std::int32_t> stats{};

  if (const auto playerStats = this->objectView->getPlayerStats(); playerStats.has_value()) {
    const auto s = playerStats.value();
    stats = {s->strength.getTotal(), s->dexterity.getTotal(), s->constitution.getTotal(), s->willpower.getTotal(),
             s->mind.getTotal()};
  } else {
    stats = {0, 0, 0, 0, 0};
  }

  auto renderArea = SDLRect(this->area);
  renderArea.rect.y = renderArea.rect.y + this->topSpacing;

  for (int i = 0; i < stats.size(); i++) {
    auto iconSurface = this->font->renderUnicodeBlended(this->glyphs.at(i), {255, 255, 255});
    auto statSurface = this->font->renderTextBlended(std::to_string(stats.at(i)), {255, 255, 255});

    auto iconTexture = SDLTexture(renderer, iconSurface);
    auto statTexture = SDLTexture(renderer, statSurface);

    renderArea.rect.w = iconSurface.get()->w;
    renderArea.rect.h = iconSurface.get()->h;
    iconTexture.render(&renderer, &renderArea);
    renderArea.rect.x = renderArea.rect.x + this->iconSpacing;

    renderArea.rect.w = statSurface.get()->w;
    renderArea.rect.h = statSurface.get()->h;
    statTexture.render(&renderer, &renderArea);
    renderArea.rect.x = renderArea.rect.x + this->statSpacing;
  }

  this->renderChildren(renderer);
}
