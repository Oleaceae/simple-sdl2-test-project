#include "logsheet.h"

#include "../../model/objectview.h"

LogSheet::LogSheet(const ObjectView* objV, const std::weak_ptr<Mob>& initialTarget,
                   const sdlwrapp::SDLRect& availableArea, std::shared_ptr<SpriteButton> button)
    : Sheet(button) {
}

void LogSheet::update() {

}

void LogSheet::updateSize(const sdlwrapp::SDLRect& availableArea) {

}
