#include "equipmentsheet.h"

#include <ranges>

#include "../../model/objectview.h"
#include "image.h"
#include "text.h"

using namespace sdlwrapp;

EquipmentSheet::EquipmentSheet(const SDLRenderer& renderer, const TTF& font, Combat* combat, const ObjectView& objV,
                               const std::shared_ptr<Mob>& initialTarget, const SDLRect& availableArea,
                               const std::shared_ptr<SpriteButton>& button)
    : Sheet(button), objectView(objV), combat(combat), target(initialTarget) {
  this->renderer = &renderer;
  this->font = &font;
  this->area = availableArea;

  this->update();
}

void EquipmentSheet::update() {
  if (target == nullptr) {
    return;
  }

  this->children.clear();

  const int width = area.rect.w / 3;
  const int height = area.rect.h * (static_cast<float>(3) / 4);
  const int bottomBarHeight = area.rect.h / 4;
  const int bottomBarY = height;

  const auto rotatingSelectorArea = SDLRect(0, 0, width, height);
  const auto equipmentViewArea = SDLRect(rotatingSelectorArea.rect.w, 0, width, height);
  const auto equipmentSelectorArea = SDLRect(rotatingSelectorArea.rect.w * 2, 0, width, height);
  const auto modifierComparatorArea = SDLRect(0, bottomBarY, area.rect.w, bottomBarHeight);

  this->add(createRotatingSelector(rotatingSelectorArea));
  // this->add(createEquipmentView(equipmentViewArea));
  // this->add(createEquipmentSelector(equipmentSelectorArea));
  // this->add(createModifierComparator(modifierComparatorArea));
}

void EquipmentSheet::updateSize(const SDLRect& availableArea) {
  this->area = availableArea;
  this->update();
}

std::shared_ptr<Widget> EquipmentSheet::createRotatingSelector(const SDLRect& area) {
  auto container = std::make_shared<RotatingUnitContainer>(RotatingUnitContainer());
  container->area = area;
  addUnitsToSelector(container, area);

  return container;
}

std::shared_ptr<Widget> EquipmentSheet::createEquipmentView(const SDLRect& area) {
}

std::shared_ptr<Widget> EquipmentSheet::createEquipmentSelector(const SDLRect& area) {
}

std::shared_ptr<Widget> EquipmentSheet::createModifierComparator(const SDLRect& area) {
}

void EquipmentSheet::addUnitsToSelector(std::shared_ptr<RotatingUnitContainer>& unitContainer, const SDLRect& area) {
  auto mobBeforeTarget = findMobBeforeTarget();
  auto mobAfterTarget = findMobAfterTarget();

  const int oneThirds{area.rect.h / 3};
  SDLRect first = {area.rect.x, area.rect.y, area.rect.w, oneThirds};
  SDLRect main = {area.rect.x, area.rect.y + oneThirds, area.rect.w, oneThirds};
  ;
  SDLRect third = {area.rect.x, area.rect.y + oneThirds * 2, area.rect.w, oneThirds};

  addPortraitAndName(unitContainer, mobBeforeTarget, first);
  addPortraitAndName(unitContainer, target, main);
  addPortraitAndName(unitContainer, mobAfterTarget, third);

  /*
   {
    auto elements = unitContainer.getChildren().size();
    bool foundTarget{};
    std::shared_ptr<const Mob> pre{};

    for (auto& mobView : std::ranges::reverse_view(objectView.getMobs())) {
      if (!foundTarget && !pre && mobView.getObject()->faction == target->faction) {
        pre = mobView.getObject();
      }
      if (mobView.getObject() == target) {
        foundTarget = true;
        continue;
      }
      if (foundTarget && mobView.getObject()->faction == target->faction) {
        addPortraitAndName(unitContainer, mobView.getObject());
        break;
      }
    }

    if (elements == unitContainer.getChildren().size() && pre) {
      addPortraitAndName(unitContainer, pre);
    }
    else if (elements == unitContainer.getChildren().size()) {
      addPortraitAndName(unitContainer, target);
    }
  }

  addPortraitAndName(unitContainer, target);

  bool foundTarget{};

  for (auto& mob : objectView.getMobs()) {
  }
  */
}

std::shared_ptr<const Mob> EquipmentSheet::findMobBeforeTarget() {
  std::reverse_iterator iter = objectView.getMobs().rbegin();
  for(; iter != objectView.getMobs().rend(); ++iter) {
    if (iter->getObject() == target) {
      break;
    }
  }
  for (auto iterCopy = iter; iterCopy != objectView.getMobs().rend(); ++iterCopy) {
    if (iterCopy->getObject() != target && iterCopy->getObject()->faction == target->faction) {
      return iterCopy->getObject();
    }
  }
  for (auto iterCopy = objectView.getMobs().rbegin(); iterCopy != iter; ++iterCopy) {
    if (iterCopy->getObject()->faction == target->faction) {
      return iterCopy->getObject();
    }
  }
  return target;
}

std::shared_ptr<const Mob> EquipmentSheet::findMobAfterTarget() {
  auto iter = objectView.getMobs().begin();
  for(; iter != objectView.getMobs().end(); ++iter) {
    if (iter->getObject() == target) {
      break;
    }
  }
  for (auto iterCopy = iter; iterCopy != objectView.getMobs().end(); ++iterCopy) {
    if (iterCopy->getObject() != target && iterCopy->getObject()->faction == target->faction) {
      return iterCopy->getObject();
    }
  }
  for (auto iterCopy = objectView.getMobs().begin(); iterCopy != iter; ++iterCopy) {
    if (iterCopy->getObject()->faction == target->faction) {
      return iterCopy->getObject();
    }
  }
  return target;
}

void EquipmentSheet::addPortraitAndName(std::shared_ptr<RotatingUnitContainer>& unitContainer,
                                        const std::shared_ptr<const Mob>& mob, const SDLRect& area) {
  const SDLRect halfTheArea = {area.rect.x, area.rect.y, area.rect.w / 2, area.rect.h};
  // TODO add actual portraits
  unitContainer->add(Image(halfTheArea, Sprite(*renderer, "assets/icons/inventory.png", 32, 0, 0)));

  auto name = Text(mob->name, *renderer, *font);
  name.area.rect.x = area.rect.w / 2;
  name.area.rect.y = area.rect.y;
  if (name.area.rect.w > area.rect.w / 2) {
    name.area.rect.w = area.rect.w / 2;
  }
  unitContainer->add(name);
}
