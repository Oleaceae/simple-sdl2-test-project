#include "teamsheet.h"
#include "../../model/objectview.h"

TeamSheet::TeamSheet(const ObjectView* objV, const std::weak_ptr<Mob>& initialTarget,
                     const sdlwrapp::SDLRect& availableArea, std::shared_ptr<SpriteButton> button)
    : Sheet(button) {
}

void TeamSheet::update() {

}

void TeamSheet::updateSize(const sdlwrapp::SDLRect& availableArea) {

}
