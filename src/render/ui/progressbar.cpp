#include "progressbar.h"

using namespace sdlwrapp;

HealthBar::HealthBar(SDLRect area, const ObjectView* view, const TTF* font) : objectView(view), font(font) {
  this->fillColor = {200, 0, 0, 128};
  this->emptyColor = {128, 128, 128, 128};
  this->area = area;
}

void HealthBar::render(SDLRenderer& renderer) {
  auto player = this->objectView->getPlayer();
  renderer.fillRectColor(this->area, this->emptyColor);

  if (!player.has_value()) {
    drawUnknownAmount(this->area, this->font, renderer);
  } else {
    const auto& [min, max] = player.value()->getHealth();
    drawKnownAmount(this->area, this->font, renderer,
                    {min, max}, this->fillColor);
  }

  renderChildren(renderer);
}

ManaBar::ManaBar(SDLRect area, const ObjectView* view, const TTF* font) : objectView(view), font(font) {
  this->fillColor = {0, 0, 200, 128};
  this->emptyColor = {128, 128, 128, 128};
  this->area = area;
}

void ManaBar::render(SDLRenderer& renderer) {
  auto player = this->objectView->getPlayer();
  renderer.fillRectColor(this->area, this->emptyColor);

  if (!player.has_value()) {
    drawUnknownAmount(this->area, this->font, renderer);
  } else {
    const auto& [min, max] = player.value()->getMana();
    drawKnownAmount(this->area, this->font, renderer, {min, max}, this->fillColor);
  }

  renderChildren(renderer);
}

void ProgressBar::drawUnknownAmount(SDLRect area, const TTF* font, SDLRenderer& renderer) {
  auto text = font->renderTextBlended("?", {255, 255, 255});
  auto textTexture = SDLTexture(renderer, text);
  drawInnerText(area, renderer, text);
}

void ProgressBar::drawKnownAmount(SDLRect area, const TTF* font, SDLRenderer& renderer,
                                  std::tuple<std::int32_t, std::int32_t> value, SDLColor& fillColor) noexcept {
  auto diff = static_cast<double>(std::get<0>(value)) / std::get<1>(value);
  auto fillArea = SDLRect(area);
  fillArea.rect.w = static_cast<int>(fillArea.rect.w * diff);
  renderer.fillRectColor(fillArea, fillColor);

  auto text = font->renderTextBlended(std::to_string(std::get<0>(value)) + " / " + std::to_string(std::get<1>(value)),
                                      {255, 255, 255});
  drawInnerText(area, renderer, text);
}

void ProgressBar::drawInnerText(SDLRect& area, SDLRenderer& renderer, const SDLSurface& text) {
  auto textTexture = SDLTexture(renderer, text);

  area.rect.x = (area.rect.x + area.rect.w / 2 - text.get()->w / 2);
  area.rect.y = (area.rect.y + area.rect.h / 2 - text.get()->h / 2);

  area.rect.h = (text.get()->h);
  area.rect.w = (text.get()->w);

  textTexture.render(&renderer, &area);
}
