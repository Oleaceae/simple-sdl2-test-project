#ifndef _GAME_TEXT_H
#define _GAME_TEXT_H

#include "../../sdl2wrapp/sdl.h"
#include "../../sdl2wrapp/sdlttf.h"
#include "widget.h"
#include <memory>
#include <string>

class Text : public Widget {
public:
  Text(const std::string& text, const sdlwrapp::SDLRenderer& renderer, const sdlwrapp::TTF& font);

  void render(sdlwrapp::SDLRenderer& renderer) override;
  void update(const std::string& text, const sdlwrapp::SDLRenderer& renderer, const sdlwrapp::TTF& font);

private:
  std::shared_ptr<sdlwrapp::SDLTexture> texture{};
};

#endif
