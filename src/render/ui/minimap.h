#ifndef GAME_MINIMAP_H
#define GAME_MINIMAP_H

#include "../../math/rectangle.h"
#include "../../model/battletile.h"
#include "../../model/mob.h"
#include "../../model/objectview.h"
#include "widget.h"

class Minimap : public Widget {
public:
  Minimap(sdlwrapp::SDLRect area, const ObjectView* objectView, const Point2D& fieldSize, std::uint16_t entitySize);
  void render(sdlwrapp::SDLRenderer& renderer) override;

private:
  const ObjectView* objectView{};
  // Unused background texture for the minimap. TODO add or remove whenever considered again
  // Using it requires a full constructor suite as SDLTexture can't be copied
  // sdlwrapp::SDLTexture minimapBase{};
  Point2D fieldSize{};
  std::uint16_t entitySize{};

  // sdlwrapp::SDLTexture generateMinimapBase();

  sdlwrapp::SDLColor friendlyColor = {0, 200, 0};
  sdlwrapp::SDLColor enemyColor = {200, 0, 0};
  sdlwrapp::SDLColor itemColor = {180, 125, 0};
  sdlwrapp::SDLColor blockerColor = {200, 200, 200};

  double fieldToAreaFactorX{};
  double fieldToAreaFactorY{};
};

#endif // GAME_MINIMAP_H
