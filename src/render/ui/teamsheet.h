#ifndef GAME_TEAMSHEET_H
#define GAME_TEAMSHEET_H

#include "sheet.h"

class ObjectView;
class Mob;

class TeamSheet final : public Sheet {
public:
  TeamSheet(const ObjectView* objV, const std::weak_ptr<Mob>& initialTarget, const sdlwrapp::SDLRect& availableArea,
            std::shared_ptr<SpriteButton> button);

  void update() override;
  void updateSize(const sdlwrapp::SDLRect& availableArea) override;

private:
};

#endif
