#ifndef GAME_TEXTBOX_H
#define GAME_TEXTBOX_H

#include "../../events/messagequeue.h"
#include "../../sdl2wrapp/sdl.h"
#include "widget.h"

#include <list>
#include <ranges>
#include <string>

class TextBox final : public Widget {
public:
  explicit TextBox(const sdlwrapp::TTF* font);
  void render(sdlwrapp::SDLRenderer& renderer) override;
  void push_front(const std::string& line);
  void consume();

  sdlwrapp::SDLColor backgroundColor{};

  std::uint32_t textSize{12};

private:
  void updateCache();

  const sdlwrapp::TTF* font{nullptr};
  const std::uint16_t maxLines{16};
  std::vector<std::string> lineCache{};
  std::list<std::string> storedLines{};
};

#endif
