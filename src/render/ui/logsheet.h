#ifndef GAME_LOGSHEET_H
#define GAME_LOGSHEET_H

#include "sheet.h"

class ObjectView;
class Mob;

class LogSheet final : public Sheet {
public:
  LogSheet(const ObjectView* objV, const std::weak_ptr<Mob>& initialTarget, const sdlwrapp::SDLRect& availableArea,
           std::shared_ptr<SpriteButton> button);

  void update() override;
  void updateSize(const sdlwrapp::SDLRect& availableArea) override;

private:
};

#endif
