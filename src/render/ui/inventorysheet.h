#ifndef GAME_INVENTORYSHEET_H
#define GAME_INVENTORYSHEET_H

#include "sheet.h"

class ObjectView;
class Mob;

class InventorySheet : public Sheet {
public:
  InventorySheet(const ObjectView* objV, const std::weak_ptr<Mob>& initialTarget,
                 const sdlwrapp::SDLRect& availableArea, std::shared_ptr<SpriteButton> button);

  void update() override;
  void updateSize(const sdlwrapp::SDLRect& availableArea) override;

private:
};

#endif
