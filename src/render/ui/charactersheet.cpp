#include "charactersheet.h"

#include "../../combat.h"
#include "container.h"
#include "image.h"
#include "text.h"

using namespace sdlwrapp;

CharacterSheet::CharacterSheet(const SDLRenderer& renderer, const TTF& font, Combat* combat,
                               const std::shared_ptr<Mob>& initialTarget, const SDLRect& availableArea,
                               const std::shared_ptr<SpriteButton>& button)
    : Sheet(button), objectView(combat->getObjectView()), combat(combat), target(initialTarget) {
  this->renderer = &renderer;
  this->font = &font;
  this->area = availableArea;

  this->update();
}

void CharacterSheet::update() {
  if (target == nullptr) {
    return;
  }

  const std::string spritePath = "assets/icons/attributes.png";
  this->children.clear();

  const auto& [x, y, w, h] = area.rect;

  const int elemSpacing = w * 0.01;
  const Point2D size = {w / 2 - elemSpacing, h / 3 - elemSpacing};

  auto portraitWidgetArea = SDLRect(elemSpacing, elemSpacing, w / 4 - elemSpacing * 2, size.y);

  auto detailsWidgetArea = SDLRect(portraitWidgetArea.rect.w + elemSpacing * 2, elemSpacing, portraitWidgetArea.rect.w,
                                   portraitWidgetArea.rect.h);

  auto attributeWidgetArea =
      SDLRect(elemSpacing, portraitWidgetArea.rect.h + elemSpacing * 2, w / 2 - elemSpacing * 3, size.y * 2);

  auto resistanceWidgetArea = SDLRect(w / 2 + elemSpacing, elemSpacing, size.x - elemSpacing, size.y);

  auto hintsWidgetArea =
      SDLRect(w / 2 + elemSpacing, resistanceWidgetArea.rect.h + elemSpacing * 2, size.x - elemSpacing, size.y);

  auto effectWidgetArea =
      SDLRect(w / 2 + elemSpacing, resistanceWidgetArea.rect.h + hintsWidgetArea.rect.h + elemSpacing * 3,
              size.x - elemSpacing, size.y - elemSpacing);

  auto portraitContainer = Container();
  portraitContainer.background = true;
  portraitContainer.area = portraitWidgetArea;
  // TODO add real portraits
  auto portrait = Image({0, 0, portraitWidgetArea.rect.w, portraitWidgetArea.rect.h},
                        Sprite(*renderer, "assets/icons/inventory.png", 32, 0, 0));

  portraitContainer.add(portrait);

  auto detailsContainer = Container();
  detailsContainer.background = true;
  detailsContainer.area = detailsWidgetArea;

  auto name = Text(target->name, *renderer, *font);
  auto race = Text(target->race.toString(), *renderer, *font);
  auto faction = Text(target->faction.toString(), *renderer, *font);

  race.area = SDLRect(0, name.area.rect.h + 5, race.area.rect.w, race.area.rect.h);
  faction.area = SDLRect(0, race.area.rect.y + race.area.rect.h + 5, faction.area.rect.w, faction.area.rect.h);

  detailsContainer.add(name);
  detailsContainer.add(race);
  detailsContainer.add(faction);

  auto effectsContainer = Container();
  effectsContainer.background = true;
  effectsContainer.area = effectWidgetArea;

  auto hintsContainer = Container();
  hintsContainer.background = true;
  hintsContainer.area = hintsWidgetArea;

  this->add(this->attributes(attributeWidgetArea, target, spritePath));
  this->add(this->resistances(resistanceWidgetArea, target->getResistances(), spritePath));
  this->add(effectsContainer);
  this->add(hintsContainer);
  this->add(detailsContainer);
  this->add(portraitContainer);
}

void CharacterSheet::updateSize(const SDLRect& availableArea) {
  this->area = availableArea;
  this->update();
}

void CharacterSheet::render(SDLRenderer& renderer) {
  renderChildren(renderer);
}

bool CharacterSheet::handleKey(const SDL_Keycode key) {
  const auto k = combat->getEventHandler().getMappedKey(key);
  if (!k.has_value()) {
    return true;
  }
  switch (k.value()) {
  case KeyPressConfig::escape:
  case KeyPressConfig::character:
    combat->getRenderer()->getUI()->openMenu(TextUI::Menu::character, combat);
    return true;
  case KeyPressConfig::left:
    combat->getRenderer()->getUI()->openMenu(TextUI::Menu::logbook, combat);
    return true;
  case KeyPressConfig::right:
    combat->getRenderer()->getUI()->openMenu(TextUI::Menu::equipment, combat);
  default:
    return true;
  }
}

std::shared_ptr<Widget> CharacterSheet::attributes(const SDLRect& size, const std::shared_ptr<Mob>& valueSource,
                                                   const std::string& spritePath) {
  auto attributesContainer = std::make_shared<Container>(Container());

  attributesContainer->area = size;

  const auto attrHeader = std::make_shared<Text>(Text("Statistics", *renderer, *font));
  attributesContainer->add(attrHeader);

  const auto& [x, y, w, h] = size.rect;
  const Point2D availableArea = {size.rect.w, (h - attrHeader->area.rect.h) / 10};

  std::array widgets = {
      createCurMaxWidget(Sprite(*renderer, spritePath, 32, 0, 2), target->getHealth(), availableArea),
      createCurMaxWidget(Sprite(*renderer, spritePath, 32, 1, 2), target->getMana(), availableArea),
      createDamageWidget(Sprite(*renderer, spritePath, 32, 3, 2), target->getAttackDamage(), availableArea),
      createAttributeWidget(Sprite(*renderer, spritePath, 32, 3, 1), target->getStats().strength, availableArea),
      createAttributeWidget(Sprite(*renderer, spritePath, 32, 4, 1), target->getStats().dexterity, availableArea),
      createAttributeWidget(Sprite(*renderer, spritePath, 32, 5, 1), target->getStats().constitution, availableArea),
      createAttributeWidget(Sprite(*renderer, spritePath, 32, 6, 1), target->getStats().willpower, availableArea),
      createAttributeWidget(Sprite(*renderer, spritePath, 32, 7, 1), target->getStats().mind, availableArea),
      createAttributeWidget(Sprite(*renderer, spritePath, 32, 2, 2), target->getStats().speed, availableArea)};

  int vertSpacing = attrHeader->area.rect.h;

  // Position every widget on Y-axis
  std::ranges::for_each(widgets, [&](auto& widget) {
    widget->move({0, vertSpacing});
    attributesContainer->add(widget);
    vertSpacing += availableArea.y;
  });

  return attributesContainer;
}

std::shared_ptr<Widget> CharacterSheet::resistances(const SDLRect& size, const Resistances& values,
                                                    const std::string& spritePath) {
  auto resistancesContainer = Container();
  resistancesContainer.background = false;
  resistancesContainer.area = size;

  const auto& [x, y, w, h] = size.rect;

  const auto resHeader = std::make_shared<Text>(Text("Elemental Resistance", *renderer, *font));
  resistancesContainer.add(resHeader);

  const int vertSpac = (h - 3) / 4;
  const int horSpac = w / 2;
  int vrt = resHeader->area.rect.h + 3;

  Point2D resImgSize = {h / 4 - vrt, h / 4 - vrt};

  if (resImgSize.x <= 0 || resImgSize.y <= 0) {
    resImgSize = {1, 1};
  }

  const auto res = target->getResistances().toArray();
  const std::array resArr = {res[0], res[2], res[4], res[6], res[1], res[3], res[5], res[7]};

  std::array<Image, 8> images{};
  std::list<Image> resistanceHints{};

  for (std::uint16_t i = 0; i < 4; i++) {
    images.at(i) = {Image({0, vrt, resImgSize.x, resImgSize.y}, {*renderer, spritePath, 32, i, 0})};
    if (resArr.at(i) != 0) {
      resistanceHints.emplace_back(createResistanceHint(resImgSize, {0, vrt}, spritePath, resArr[i]));
    }
    vrt += vertSpac;
  }

  vrt = resHeader->area.rect.h + 3;

  for (std::uint16_t i = 4; i < 8; i++) {
    images.at(i) = {Image({horSpac, vrt, resImgSize.x, resImgSize.y}, {*renderer, spritePath, 32, i, 0})};
    if (resArr.at(i) != 0) {
      resistanceHints.emplace_back(createResistanceHint(resImgSize, {horSpac, vrt}, spritePath, resArr.at(i)));
    }
    vrt += vertSpac;
  }

  for (const auto& image : images) {
    resistancesContainer.add(image);
  }

  if (!resistanceHints.empty()) {
    if (resistanceHints.begin()->area.rect.w > horSpac - resImgSize.x) {
      std::ranges::for_each(resistanceHints, [&](auto& hint) { hint.area.rect.w = horSpac - resImgSize.x; });
    }
    if (resistanceHints.begin()->area.rect.h > vertSpac) {
      std::ranges::for_each(resistanceHints, [&](auto& hint) { hint.area.rect.h = vertSpac; });
    }
  }

  for (const auto& image : resistanceHints) {
    resistancesContainer.add(image);
  }

  return std::make_shared<Container>(resistancesContainer);
}

Image CharacterSheet::createResistanceHint(const Point2D& imgSize, const Point2D& loc, const std::string& attrPath,
                                           const float value) const {
  constexpr int resHintSpriteSize = 32;
  const int stepSize = imgSize.x / 3;
  const int spriteToUse = value < 0 ? 0 : value < 1 ? 1 : 2;
  SDLRect dest = {0, 0, imgSize.x, imgSize.y};

  const auto source = Sprite::createSpriteSurface(attrPath, resHintSpriteSize, spriteToUse, 1);
  const SDLSurface targetSurface = SDLSurface::createTransparentRGBSurface(resHintSpriteSize * 3, resHintSpriteSize);

  for (int i = 0; i < resistanceValueToSteps(value); i++) {
    source.blit(targetSurface, dest);
    dest.rect.x = dest.rect.x + stepSize;
  }

  return Image({loc.x + imgSize.x, loc.y, imgSize.x * 3, imgSize.y},
               Sprite(std::make_shared<SDLTexture>(SDLTexture(*renderer, targetSurface))));
}

std::shared_ptr<Widget> CharacterSheet::createDamageWidget(Sprite&& icon, const Damage& damage,
                                                           const Point2D& availableArea) const {
  auto container = std::make_shared<Container>(Container());

  const auto& [w, h] = availableArea;

  auto img = Image(icon);
  img.area = SDLRect(w * 0.03, h * 0.25 / 2, h * 0.75, h * 0.75);

  auto text = Text(std::format("{} ~ {}, {:.2f}x", damage.min, damage.max, damage.multiplier), *renderer, *font);
  text.area.rect.x = img.area.rect.w + img.area.rect.x * 2;
  text.align(Alignment::vertical, {0, h});

  container->add(img);
  container->add(text);

  return container;
}

std::shared_ptr<Widget> CharacterSheet::createCurMaxWidget(Sprite&& icon, const CurMax& curMax,
                                                           const Point2D& availableArea) const {
  auto container = std::make_shared<Container>(Container());

  const auto& [w, h] = availableArea;

  auto img = Image(icon);
  img.area = SDLRect(w * 0.03, h * 0.25 / 2, h * 0.75, h * 0.75);

  auto text = Text(std::format("{} / {}", curMax.current, curMax.max), *renderer, *font);
  text.area.rect.x = img.area.rect.w + img.area.rect.x * 2;
  text.align(Alignment::vertical, {0, h});

  container->add(img);
  container->add(text);

  return container;
}

std::shared_ptr<Widget> CharacterSheet::createAttributeWidget(Sprite&& icon, const Attribute& attribute,
                                                              const Point2D& availableArea) const {
  auto container = std::make_shared<Container>(Container());

  const auto& [w, h] = availableArea;

  auto img = Image(icon);
  img.area = SDLRect(w * 0.03, h * 0.25 / 2, h * 0.75, h * 0.75);

  auto text = Text(std::format("{} ({})", attribute.base, attribute.getTotal()), *renderer, *font);
  text.area.rect.x = img.area.rect.w + img.area.rect.x * 2;
  text.align(Alignment::vertical, {0, h});

  container->add(img);
  container->add(text);

  return container;
}

int CharacterSheet::resistanceValueToSteps(float value) noexcept {
  value = std::abs(value);
  if (value >= 1) {
    value -= 1;
  }

  if (value == 0) {
    return 0;
  }
  if (value < 0.16) {
    return 1;
  }
  if (value >= 0.16) {
    value = static_cast<float>(std::trunc(value / 0.15));
    if (value > 6) {
      return 6;
    }
  }

  return static_cast<int>(value);
}
