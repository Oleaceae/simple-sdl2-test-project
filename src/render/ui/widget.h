#ifndef WIDGET_H
#define WIDGET_H

#include "../../math/point2d.h"
#include "../../sdl2wrapp/sdl.h"
#include "../../sdl2wrapp/sdlttf.h"
#include <memory>
#include <vector>

class Widget;

template <typename T>
concept WidgetType = std::is_base_of_v<Widget, T>;

class Widget {
public:
  enum class Alignment {
    vertical,
  };
  virtual ~Widget() = default;
  virtual void render(sdlwrapp::SDLRenderer& renderer);

  [[nodiscard]] virtual bool handleLeftClick(const Point2D& click);
  [[nodiscard]] virtual bool handleMidClick(const Point2D& click);
  [[nodiscard]] virtual bool handleRightClick(const Point2D& click);
  [[nodiscard]] virtual bool handleKey(SDL_Keycode key);
  [[nodiscard]] virtual bool handleMouseWheel(const Point2D& mousePos, std::int32_t wheelDir);

  void add(const std::shared_ptr<Widget>& child);
  template <WidgetType W> void add(const W& child);
  std::vector<std::shared_ptr<Widget>>& getChildren() noexcept;

  // Move widget including all of its children by an offset
  void move(const Point2D& direction) noexcept;

  void align(Alignment alignment, const Point2D& within) noexcept;

  sdlwrapp::SDLRect area{0, 0, 64, 64};
  bool interactive{true};

protected:
  void renderChildren(sdlwrapp::SDLRenderer& renderer);
  virtual void afterAreaChanged();
  std::vector<std::shared_ptr<Widget>> children{};

private:
  void positionAddedChildren(const Point2D& offset);
};

template <WidgetType W> void Widget::add(const W& child) {
  this->add(std::make_shared<W>(child));
}

#endif
