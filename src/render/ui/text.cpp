#include "text.h"

using namespace sdlwrapp;

Text::Text(const std::string& text, const SDLRenderer& renderer, const TTF& font) {
  this->update(text, renderer, font);
}

void Text::render(SDLRenderer& renderer) {
  this->texture->render(&renderer, &this->area);
}

void Text::update(const std::string& text, const SDLRenderer& renderer, const TTF& font) {
  const SDLSurface textSurface = font.renderTextBlended(text);
  this->area.rect.w = textSurface.get()->w;
  this->area.rect.h = textSurface.get()->h;
  this->texture = std::make_shared<SDLTexture>(SDLTexture(renderer, textSurface));
}
