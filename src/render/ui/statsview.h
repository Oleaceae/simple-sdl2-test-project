#ifndef GAME_STATSVIEW_H
#define GAME_STATSVIEW_H

#include "../../model/objectview.h"
#include "../../sdl2wrapp/sdl.h"
#include "widget.h"

enum class StatType { str, dex, con, mnd, wil };

class StatsView final : public Widget {
public:
  StatsView(sdlwrapp::SDLRect area, const ObjectView* view, const sdlwrapp::TTF* font);

  void render(sdlwrapp::SDLRenderer& renderer) override;

private:
  const ObjectView* objectView{nullptr};
  const sdlwrapp::TTF* font{nullptr};
  const std::int8_t topSpacing{2};
  const std::int16_t iconSpacing{12};
  const std::int16_t statSpacing{80};
  const std::vector<std::u16string> glyphs = {u"A", u"B", u"C", u"D", u"E"};
};

#endif
