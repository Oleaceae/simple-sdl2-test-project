#include "minimap.h"

using namespace sdlwrapp;

Minimap::Minimap(SDLRect area, const ObjectView* objectView, const Point2D& fieldSize, std::uint16_t entitySize)
    : objectView(objectView), fieldSize(fieldSize), entitySize(entitySize) {
  this->area = area;
  // this->minimapBase = this->generateMinimapBase();
  this->fieldToAreaFactorX = {static_cast<double>(area.rect.w) / this->fieldSize.x};
  this->fieldToAreaFactorY = {static_cast<double>(area.rect.h) / this->fieldSize.y};
}

void Minimap::render(SDLRenderer& renderer) {
  for (const auto& item : this->objectView->getItems()) {
    SDLRect targetArea = {
        this->area.rect.x + static_cast<int>(item.getObject()->location->location.x * fieldToAreaFactorX),
        this->area.rect.y + static_cast<int>(item.getObject()->location->location.y * fieldToAreaFactorY), entitySize,
        entitySize};
    renderer.fillRectColor(targetArea, itemColor);
  }

  for (const auto& blocker : this->objectView->getBlockers()) {
    SDLRect targetArea = {
        this->area.rect.x + static_cast<int>(blocker.getObject()->location->location.x * fieldToAreaFactorX),
        this->area.rect.y + static_cast<int>(blocker.getObject()->location->location.y * fieldToAreaFactorY),
        entitySize, entitySize};
    renderer.fillRectColor(targetArea, blockerColor);
  }

  for (const auto& mob : this->objectView->getMobs()) {
    SDLRect targetArea = {
        this->area.rect.x + static_cast<int>(mob.getObject()->location->location.x * fieldToAreaFactorX),
        this->area.rect.y + static_cast<int>(mob.getObject()->location->location.y * fieldToAreaFactorY), entitySize,
        entitySize};
    // TODO add proper faction to color method
    renderer.fillRectColor(targetArea, mob.getObject()->faction.type == Faction::Type::player ? friendlyColor : enemyColor);
  }

  this->renderChildren(renderer);
}

/*
SDLTexture Minimap::generateMinimapBase() {
  return SDLTexture();
}
*/
