#ifndef GAME_SHEET_H
#define GAME_SHEET_H

#include "button.h"
#include "widget.h"
#include "../../model/objectview.h"

class Sheet : public Widget {
public:
  explicit Sheet(const std::shared_ptr<SpriteButton>& button);

  void onOpen() const;
  void onClose() const;

  virtual void update() = 0;
  virtual void updateSize(const sdlwrapp::SDLRect& availableArea) = 0;

  std::shared_ptr<SpriteButton> button;

  const sdlwrapp::SDLRenderer* renderer{};
  const sdlwrapp::TTF* font{};
};

#endif
