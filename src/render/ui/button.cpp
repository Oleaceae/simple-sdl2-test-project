#include "button.h"

using namespace sdlwrapp;

Button::Button(const SDLRect area) {
  this->area = area;
}

void Button::render(SDLRenderer& renderer) {
  if (this->border) {
    renderer.drawRectColor(this->area, this->borderColor);
  }
  this->renderChildren(renderer);
}

bool Button::handleLeftClick(const Point2D& click) {
  if (click.inside(this->area)) {
    this->onClick();
    return true;
  }
  return false;
}

void Button::addBorder(const SDLColor color) noexcept {
  this->border = true;
  this->borderColor = color;
}

TextButton::TextButton(TTF* font, const std::u16string& innerText, const SDLRect area, const SDLColor textColor) {
  this->font = font;
  this->innerText = innerText;
  this->area = area;
  this->textColor = textColor;
  this->renderArea = centerText(font, innerText, area);
}

SDLRect TextButton::centerText(const TTF* f, const std::u16string& text, const SDLRect& area) {
  const auto [x, y] = f->getUnicodeSize(text);
  const auto ar = &area.rect;
  return {ar->x + ar->w / 2 - x / 2, ar->y + ar->h / 2 - y / 2, x, y};
}

void TextButton::render(SDLRenderer& renderer) {
  if (this->border) {
    renderer.drawRectColor(this->area, this->borderColor);
  }

  if (!this->innerText.empty()) {
    // TODO so creating  & destroying textures every frame is costly and most of the CPU time is wasted on this
    // Cache static textures, not only in button, eventually. Check profiler if this actually mattered
    const auto text = font->renderUnicodeBlended(this->innerText, this->textColor);
    const auto textTexture = SDLTexture(renderer, text);
    textTexture.render(&renderer, &this->renderArea);
  }

  this->renderChildren(renderer);
}

void TextButton::afterAreaChanged() {
  this->renderArea = centerText(this->font, this->innerText, this->area);
}

SpriteButton::SpriteButton(const SDLRect area, Sprites&& sprites) : sprites(std::move(sprites)) {
  this->area = area;
}

void SpriteButton::render(SDLRenderer& renderer) {
  this->sprites.sprites[spriteState].texture->render(&renderer, &this->area);
  this->renderChildren(renderer);
}

void SpriteButton::setSpriteState(const std::uint8_t newState) {
  this->spriteState = newState;
}
