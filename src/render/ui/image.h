#ifndef GAME_IMAGE_H
#define GAME_IMAGE_H

#include <utility>

#include "../sprite.h"
#include "widget.h"

class Image : public Widget {
public:
  Image() = default;
  explicit Image(sdlwrapp::SDLRect area, Sprite sprite);
  explicit Image(Sprite  sprite);

  void render(sdlwrapp::SDLRenderer& renderer) override;

private:
  Sprite imageCache{};
};

#endif