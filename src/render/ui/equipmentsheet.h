#ifndef GAME_EQUIPMENTSHEET_H
#define GAME_EQUIPMENTSHEET_H

#include "container.h"
#include "sheet.h"

class ObjectView;
class Mob;
class Combat;

class RotatingUnitContainer;

class EquipmentSheet final : public Sheet {
public:
  EquipmentSheet(const sdlwrapp::SDLRenderer& renderer, const sdlwrapp::TTF& font, Combat* combat,
                 const ObjectView& objV, const std::shared_ptr<Mob>& initialTarget,
                 const sdlwrapp::SDLRect& availableArea, const std::shared_ptr<SpriteButton>& button);
  void update() override;
  void updateSize(const sdlwrapp::SDLRect& availableArea) override;

private:
  const ObjectView& objectView{};
  Combat* combat{};
  std::shared_ptr<Mob> target{};

  [[nodiscard]] std::shared_ptr<Widget> createRotatingSelector(const sdlwrapp::SDLRect& area);
  [[nodiscard]] std::shared_ptr<Widget> createEquipmentView(const sdlwrapp::SDLRect& area);
  [[nodiscard]] std::shared_ptr<Widget> createEquipmentSelector(const sdlwrapp::SDLRect& area);
  [[nodiscard]] std::shared_ptr<Widget> createModifierComparator(const sdlwrapp::SDLRect& area);

  void addPortraitAndName(std::shared_ptr<RotatingUnitContainer>& unitContainer, const std::shared_ptr<const Mob>& mob,
                          const sdlwrapp::SDLRect& area);
  void addUnitsToSelector(std::shared_ptr<RotatingUnitContainer>& unitContainer, const sdlwrapp::SDLRect& area);
  std::shared_ptr<const Mob> findMobBeforeTarget();
  std::shared_ptr<const Mob> findMobAfterTarget();
};

class RotatingUnitContainer : public Container {
public:
};

#endif
