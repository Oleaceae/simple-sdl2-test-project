#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include "event.h"

#include <queue>

template <typename T> class EventQueue {
public:
  EventQueue() = default;
  EventQueue(const EventQueue<T>&& source) noexcept : events(source.events){};
  EventQueue(const EventQueue<T>& source) : events(source.events){};

  EventQueue<T>& operator=(EventQueue<T>&& other) noexcept;

  bool process(T receiver);
  void add(Event<T> event);

private:
  std::queue<Event<T>> events = {};
};

template <typename T> EventQueue<T>& EventQueue<T>::operator=(EventQueue&& other) noexcept {
  if (this != &other) {
    this->events = other.events;
  }
  return *this;
}

template <typename T> bool EventQueue<T>::process(T receiver) {
  while (!this->events.empty()) {
    events.front().run(receiver);
    events.pop();
  }
  return true;
}

template <typename T> void EventQueue<T>::add(Event<T> event) {
  events.push(event);
}

#endif
