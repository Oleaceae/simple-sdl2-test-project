#ifndef EVENT_H
#define EVENT_H

#include <functional>

template <typename T> class Event {
public:
  explicit Event(std::function<void(T)> event) : event(event){};
  Event(const Event<T>& source) : event(source.event){};
  Event(const Event<T>&& source) noexcept : event(source.event){};

  void run(T recipient);

private:
  const std::function<void(T)> event;
};

template <typename T> void Event<T>::run(T recipient) {
  this->event(recipient);
}

#endif
