#ifndef GAME_MESSAGEQUEUE_H
#define GAME_MESSAGEQUEUE_H

#include <queue>
#include <string>

class MessageQueue {
public:
  static MessageQueue& instance() {
    static MessageQueue q;
    return q;
  }

  void add(const std::string& message) {
    this->messages.emplace(message);
  }

  bool isEmpty() {
    return this->messages.empty();
  }

  std::string take() {
    std::string message = messages.front();
    messages.pop();
    return message;
  }

  MessageQueue(const MessageQueue&) = delete;
  MessageQueue& operator=(const MessageQueue&) = delete;

private:
  MessageQueue(){};
  ~MessageQueue(){};

  std::queue<std::string> messages{};
};

#endif
