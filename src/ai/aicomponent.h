#ifndef GAME_AICOMPONENT_H
#define GAME_AICOMPONENT_H

#include "../events/messagequeue.h"
#include "../math/point2d.h"
#include "condition.h"

#include <functional>
#include <memory>
#include <variant>

class Mob;
class BattleTile;

struct AIComponent {
  typedef std::variant<std::monostate, std::shared_ptr<const Mob>, const BattleTile*, const std::string*> OutcomeTarget;
  typedef std::function<void(BattleField&, std::shared_ptr<Mob>&, OutcomeTarget)> Outcome;

  bool process(BattleField& field, std::shared_ptr<Mob>& user) const;

  std::vector<Condition> conditions{};
  Outcome outcome{};
};

struct OutcomeFactory {
  static AIComponent::Outcome changeAI(const std::string& ai);
  static AIComponent::Outcome moveTowards();
};

#endif
