#ifndef GAME_ASTAR_H
#define GAME_ASTAR_H

#include <list>

#include "../math/point2d.h"
#include "../model/battlefield.h"

struct Node {
  Point2D location{};
  unsigned int cost{};
  unsigned int h{};
  std::shared_ptr<Node> parent{};

  bool operator==(const Node& other) {
    return this->location == other.location;
  }
};

class AStar {
public:
  static const Point2D nextStep(const Point2D& start, const Point2D& end, const BattleField& field);
	
private:
  std::vector<Point2D> calculatePath(const Point2D& start, const Point2D& end, const BattleField& field);
  unsigned int h(const Point2D& start, const Point2D& end) const noexcept;
  std::vector<std::shared_ptr<Node>> getNeighbors(const std::shared_ptr<Node>& tile, const Point2D& target, const BattleField& field) const noexcept;
  std::vector<Point2D> retracePath(std::shared_ptr<Node>& end, const Point2D& start) noexcept;
};

#endif