#ifndef GAME_CONDITION_H
#define GAME_CONDITION_H

#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <variant>

#include "../math/point2d.h"
#include "../math/random.h"

enum class Operator { NONE, AND, OR };

class Mob;
class BattleField;
class BattleTile;
class AI;

struct ConditionResult {
  typedef std::variant<std::monostate, std::shared_ptr<const Mob>, const BattleTile*, const std::string*> OutcomeTarget;

  bool result{false};
  OutcomeTarget target{std::monostate()};
};

class Condition {
  friend class ConditionFactory;

public:
  ConditionResult operator()(BattleField& field, const std::shared_ptr<Mob>& user, ConditionResult& pastResult) const;
  [[nodiscard]] Operator getOp() const noexcept;

  enum class TargetType { player, ally, enemy, tile };

protected:
  std::function<ConditionResult(BattleField&, const std::shared_ptr<Mob>&, ConditionResult&)> condition{};
  Operator op{};
  std::int32_t var{3};
};

class ConditionFactory {
public:
  static Condition inRange(Condition::TargetType target, std::int32_t range = 3);
  static Condition inRangeNearestMob(Condition::TargetType target, std::int32_t range = 3);
  static Condition inRangeFurthestMob(Condition::TargetType target, std::int32_t range = 3);
  static Condition always();
  static Condition random(std::int32_t percentChance);

private:
  static ConditionResult pickRandomTile(BattleField& field, const Point2D& center, std::int32_t range);
  static ConditionResult findMob(const BattleField& field, std::int32_t range, const std::shared_ptr<Mob>& user,
                                 Condition::TargetType targetType);
  static ConditionResult findNearestMob(const BattleField& field, std::int32_t range, const std::shared_ptr<Mob>& user,
                                 Condition::TargetType targetType);
  static ConditionResult findFurthestMob(const BattleField& field, std::int32_t range, const std::shared_ptr<Mob>& user,
                               Condition::TargetType targetType);
  static bool isEligibleTarget(std::int32_t range, const std::shared_ptr<Mob>& user,
                               Condition::TargetType targetType, const std::shared_ptr<const Mob>& targetMob);
};

#endif
