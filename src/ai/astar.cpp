#include "astar.h"
#include <unordered_set>

const Point2D AStar::nextStep(const Point2D& start, const Point2D& end, const BattleField& field) {
  AStar astar;
  auto path = astar.calculatePath(start, end, field);

  if (path.empty()) {
    return start;
  }
  return *path.begin();
}

struct p2dHash {
  std::size_t operator()(const Point2D p) const {
    return std::hash<int>()(p.x) ^ std::hash<int>()(p.y);
  }
};

std::vector<Point2D> AStar::calculatePath(const Point2D& start, const Point2D& end, const BattleField& field) {
  std::vector<std::shared_ptr<Node>> open{};
  std::unordered_set<Point2D, p2dHash> closed{};

  open.push_back(std::make_shared<Node>(Node(start, 0, this->h(start, end))));

  while (!open.empty()) {
    std::shared_ptr<Node> current = open.front();

    for (int i = 1; i < open.size(); i++) {
      if ((open[i]->cost + open[i]->h) < (current->cost + current->h)) {
        current = open[i];
      }
    }
    open.erase(std::find(open.begin(), open.end(), current));
    closed.insert(current->location);

    if (current->location == end) {
        return retracePath(current, start);
    }

    for (std::shared_ptr<Node> node : this->getNeighbors(current, end, field)) {
      if (node->location != end && field.getTile(node->location).blocksMovement() || closed.contains(node->location)) {
        continue;
      }
      if (std::find_if(open.begin(), open.end(), [&](std::shared_ptr<Node>& elem)  { return elem->location == node->location; }) ==
          open.end()) {
        node->parent = current;
        open.push_back(node);
      }
    }
  }
  return {};
}

unsigned int AStar::h(const Point2D& start, const Point2D& end) const noexcept {
  return start.distance(end);
}

std::vector<std::shared_ptr<Node>> AStar::getNeighbors(const std::shared_ptr<Node>& tile, const Point2D& target, const BattleField& field) const noexcept {
  std::vector<std::shared_ptr<Node>> out{};

  for (int x = -1; x <= 1; x++) {
    for (int y = -1; y <= 1; y++) {
      if (x == 0 && y == 0) {
        continue;
      }
      Point2D neighborLoc = {tile->location.x + x, tile->location.y + y};
      if (!neighborLoc.withinPositiveBounds(field.getSize())) {
        continue;
      }
      out.push_back(std::make_shared<Node>(neighborLoc, tile->cost + 1, neighborLoc.distance(target)));
    }
  }
  return out;
}

std::vector<Point2D> AStar::retracePath(std::shared_ptr<Node>& end, const Point2D& start) noexcept {
  std::vector<Point2D> path{};
  std::shared_ptr<Node> current = end;

  while (current->location != start) {
    path.push_back(current->location);
    current = current->parent;
  }
  std::reverse(path.begin(), path.end());
  return path;
}
