#include "ai.h"
#include "../model/battlefield.h"

void AI::process(BattleField& field, std::shared_ptr<Mob>& user) const {
  for (const auto& component : this->components) {
    if (component.process(field, user)) {
      break;
    }
  }
}

AI AIFactory::idleAI(const std::string& newAI) {
  AI ai{};

  AIComponent switchToCombat{};
  switchToCombat.conditions.push_back(ConditionFactory::inRange(Condition::TargetType::enemy, 10));
  switchToCombat.outcome = OutcomeFactory::changeAI(newAI);
  ai.components.push_back(switchToCombat);

  AIComponent idle{};
  idle.conditions.push_back(ConditionFactory::random(25));
  idle.conditions.push_back(ConditionFactory::inRange(Condition::TargetType::tile, 1));
  idle.outcome = OutcomeFactory::moveTowards();
  ai.components.push_back(idle);

  return ai;
}

AI AIFactory::simpleAI(const std::string& newAI) {
  AI ai{};

  AIComponent moveTowards{};
  moveTowards.conditions.push_back(ConditionFactory::inRangeNearestMob(Condition::TargetType::enemy, 12));
  moveTowards.outcome = OutcomeFactory::moveTowards();
  ai.components.push_back(moveTowards);

  AIComponent switchToIdle{};
  switchToIdle.conditions.push_back(ConditionFactory::always());
  switchToIdle.outcome = OutcomeFactory::changeAI(newAI);
  ai.components.push_back(switchToIdle);

  return ai;
}
