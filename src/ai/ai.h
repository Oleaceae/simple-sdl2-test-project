#ifndef GAME_AI_H
#define GAME_AI_H

#include "../math/random.h"
#include "aicomponent.h"
#include <vector>

class Mob;

class AI {
  friend class AIFactory;

public:
  void process(BattleField& field, std::shared_ptr<Mob>& user) const;

protected:
  std::vector<AIComponent> components{};
};

class AIFactory {
public:
  static AI idleAI(const std::string& newAI);
  static AI simpleAI(const std::string& newAI);

private:
};

#endif
