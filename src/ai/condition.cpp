#include "condition.h"
#include "../model/battlefield.h"
#include "ai.h"

ConditionResult Condition::operator()(BattleField& field, const std::shared_ptr<Mob>& user,
                                      ConditionResult& pastResult) const {
  return this->condition(field, user, pastResult);
}

Operator Condition::getOp() const noexcept {
  return this->op;
}

Condition ConditionFactory::inRange(Condition::TargetType target, std::int32_t range) {
  Condition condition{};
  condition.var = range;
  condition.condition = [range = condition.var, targetType = target](BattleField& field,
                                                                     const std::shared_ptr<Mob>& user,
                                                                     ConditionResult& pastResult) -> ConditionResult {
    if (targetType == Condition::TargetType::tile) {
      return ConditionFactory::pickRandomTile(field, user->location->location, range);
    } else {
      return ConditionFactory::findMob(field, range, user, targetType);
    }
  };

  return condition;
}

Condition ConditionFactory::inRangeNearestMob(Condition::TargetType target, std::int32_t range) {
  Condition condition{};
  condition.var = range;
  condition.condition = [range = condition.var, targetType = target](BattleField& field,
                                                                     const std::shared_ptr<Mob>& user,
                                                                     ConditionResult& pastResult) -> ConditionResult {
    return ConditionFactory::findNearestMob(field, range, user, targetType);
  };

  return condition;
}

Condition ConditionFactory::inRangeFurthestMob(Condition::TargetType target, std::int32_t range) {
  Condition condition{};
  condition.var = range;
  condition.condition = [range = condition.var, targetType = target](BattleField& field,
                                                                     const std::shared_ptr<Mob>& user,
                                                                     ConditionResult& pastResult) -> ConditionResult {
    return ConditionFactory::findFurthestMob(field, range, user, targetType);
  };

  return condition;
}

Condition ConditionFactory::always() {
  Condition condition{};
  condition.condition = [](BattleField& field, const std::shared_ptr<Mob>& user,
                           ConditionResult& pastResult) -> ConditionResult {
    return {true, std::monostate{}};
  };
  return condition;
}

Condition ConditionFactory::random(std::int32_t percentChance) {
  Condition condition{};
  condition.op = Operator::AND;
  condition.condition = [chance = percentChance](BattleField& field, const std::shared_ptr<Mob>& user,
                                                 ConditionResult& pastResult) -> ConditionResult {
    return {Random::instance().gen_uniform_distributed(1, 100) <= chance, std::monostate{}};
  };
  return condition;
}

ConditionResult ConditionFactory::pickRandomTile(BattleField& field, const Point2D& center, std::int32_t range) {
  if (range <= 0) {
    return {false, std::monostate{}};
  }
  for (int i = 0; i < 5; i++) {
    auto x = static_cast<std::int32_t>(Random::instance().gen_uniform_distributed(0, range * 2)) - range;
    auto y = static_cast<std::int32_t>(Random::instance().gen_uniform_distributed(0, range * 2)) - range;
    Point2D target = {center.x + x, center.y + y};
    if (target.withinPositiveBounds(field.getWidth(), field.getHeight())) {
      return {true, &field.getTile(target)};
    }
  }
  return {false, std::monostate{}};
}

ConditionResult ConditionFactory::findMob(const BattleField& field, const std::int32_t range,
                                          const std::shared_ptr<Mob>& user, const Condition::TargetType targetType) {

  for (const auto& mob : field.objectView.getMobs()) {
    const std::shared_ptr<const Mob>& targetMob = mob.getObject();
    if (isEligibleTarget(range, user, targetType, targetMob)) {
      return {true, targetMob};
    }
  }
  return {false, std::monostate{}};
}

ConditionResult ConditionFactory::findNearestMob(const BattleField& field, std::int32_t range,
                                                 const std::shared_ptr<Mob>& user, Condition::TargetType targetType) {
  std::shared_ptr<const Mob> nearest{};
  for (const auto& mob : field.objectView.getMobs()) {
    const std::shared_ptr<const Mob>& targetMob = mob.getObject();
    if (isEligibleTarget(range, user, targetType, targetMob)) {
      if (nearest == nullptr) {
        nearest = targetMob;
      } else if (targetMob->location->location.distance(user->location->location) <
                 nearest->location->location.distance(user->location->location)) {
        nearest = targetMob;
      }
    }
  }
  if (nearest == nullptr) {
    return {false, std::monostate{}};
  }
  return {true, nearest};
}

ConditionResult ConditionFactory::findFurthestMob(const BattleField& field, std::int32_t range,
                                                  const std::shared_ptr<Mob>& user, Condition::TargetType targetType) {
  std::shared_ptr<const Mob> furthest{};
  for (const auto& mob : field.objectView.getMobs()) {
    const std::shared_ptr<const Mob>& targetMob = mob.getObject();
    if (isEligibleTarget(range, user, targetType, targetMob)) {
      if (furthest == nullptr) {
        furthest = targetMob;
      } else if (targetMob->location->location.distance(user->location->location) >
                 furthest->location->location.distance(user->location->location)) {
        furthest = targetMob;
      }
    }
  }
  if (furthest == nullptr) {
    return {false, std::monostate{}};
  }
  return {true, furthest};
}

bool ConditionFactory::isEligibleTarget(const std::int32_t range, const std::shared_ptr<Mob>& user,
                                        const Condition::TargetType targetType,
                                        const std::shared_ptr<const Mob>& targetMob) {
  switch (targetType) {
  case Condition::TargetType::ally:
    if (user->faction.isFriend(targetMob->faction) &&
        targetMob->location->location.inRange(user->location->location, range)) {
      return true;
    }
    break;
  case Condition::TargetType::enemy:
    if (!user->faction.isFriend(targetMob->faction) &&
        targetMob->location->location.inRange(user->location->location, range)) {
      return true;
    }
    break;
  case Condition::TargetType::player:
    if (targetMob->isPlayerControlled() && targetMob->location->location.inRange(user->location->location, range)) {
      return true;
    }
    break;
  default:
    return false;
  }
  return false;
}
