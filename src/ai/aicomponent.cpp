#include "aicomponent.h"
#include "../model/battlefield.h"
#include "astar.h"

using Outcome = AIComponent::Outcome;
using OutcomeTarget = AIComponent::OutcomeTarget;

bool AIComponent::process(BattleField& field, std::shared_ptr<Mob>& user) const {
  auto res = ConditionResult{};
  for (const auto& cond : this->conditions) {
    res = cond(field, user, res);
    if (res.result) {
      if (cond.getOp() != Operator::AND) {
        this->outcome(field, user, res.target);
        return true;
      } else {
        continue;
      }
    } else if (cond.getOp() == Operator::OR) {
      continue;
    }
    return false;
  }
  return false;
}

Outcome OutcomeFactory::changeAI(const std::string& ai) {
  return [targetAI = ai](BattleField& field, const std::shared_ptr<Mob>& user, const OutcomeTarget& target) {
    try {
      user->activeAI = user->availableAIs.at(targetAI);
    } catch (std::out_of_range& ex) {
      MessageQueue::instance().add("ERROR: Tried switching to non-existent AI.");
    }
  };
}

Outcome OutcomeFactory::moveTowards() {
  return [](BattleField& field, const std::shared_ptr<Mob>& user, OutcomeTarget target) {
    Point2D targetLoc{};
    if (std::holds_alternative<std::shared_ptr<const Mob>>(target)) {
      targetLoc = std::get<std::shared_ptr<const Mob>>(target)->location->location;
    } else if (std::holds_alternative<const BattleTile*>(target)) {
      targetLoc = std::get<const BattleTile*>(target)->location;
    }

    Point2D userLoc = user->location->location;

    auto next = AStar::nextStep(userLoc, targetLoc, field);

    std::int32_t x = userLoc.x < next.x ? 1 : userLoc.x > next.x ? -1 : 0;
    std::int32_t y = userLoc.y < next.y ? 1 : userLoc.y > next.y ? -1 : 0;
    field.moveMob(user, {x, y});
  };
}
